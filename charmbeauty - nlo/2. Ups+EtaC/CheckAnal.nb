(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20943,        565]
NotebookOptionsPosition[     19448,        531]
NotebookOutlinePosition[     19782,        546]
CellTagsIndexPosition[     19739,        543]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"<<", "X`"}]], "Input",
 CellChangeTimes->{{3.821324521684812*^9, 3.8213245249991293`*^9}, {
  3.82316025414358*^9, 3.8231602552805*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"dc55a47d-d08d-45d1-b8ec-4e686cc93072"],

Cell[BoxData[
 FormBox["\<\"\\!\\(\\*TemplateBox[List[\\\"\\\\\\\"Package-X v2.1.1, by \
Hiren H. Patel\\\\\\\\nFor more information, see the \\\\\\\"\\\", \
TemplateBox[List[\\\"\\\\\\\"guide\\\\\\\"\\\", \\\"paclet:X/guide/PackageX\\\
\"], \\\"HyperlinkPaclet\\\"]], \\\"RowDefault\\\"]\\)\"\>", 
  TraditionalForm]], "Print",
 CellChangeTimes->{
  3.82132453911065*^9, 3.821324890068315*^9, 3.821325387219303*^9, 
   3.821334394581717*^9, 3.8213364590736217`*^9, 3.8213365021212587`*^9, 
   3.822383959759102*^9, 3.8223849673906307`*^9, 3.822385598289443*^9, 
   3.822385685464016*^9, 3.822474731293976*^9, {3.8231542739101133`*^9, 
   3.823154283357386*^9}, 3.823155516015375*^9, 3.823155849039309*^9, 
   3.823160256838966*^9, 3.823160392113358*^9, 3.823251976084959*^9, 
   3.8234255000073957`*^9, 3.82342759949942*^9, 3.823427876576537*^9, 
   3.824217130033276*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"6252fe09-3e62-4a2a-bd9b-c19a1fc09f9b"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Read amplitudes in ABC form ", "Section",
 CellChangeTimes->{{3.823161383898415*^9, 
  3.823161424282631*^9}},ExpressionUUID->"9fa8adf8-b3e1-466b-9171-\
4428dbf436b4"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"ToFileName", "[", 
    RowBox[{
     RowBox[{"NotebookDirectory", "[", "]"}], ",", "\"\<data\>\""}], "]"}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AmpUpsEC", "=", 
   RowBox[{"<<", "AmpABC`"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AmpUpsEC", "=", 
   RowBox[{
    RowBox[{"AmpUpsEC", "//", "Expand"}], "//", "Simplify"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AmpUpsECreversed", "=", 
   RowBox[{"AmpUpsEC", "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"ED", "\[Rule]", " ", "EU"}], ",", 
      RowBox[{"MB", "\[Rule]", " ", "MC"}], ",", 
      RowBox[{"MC", "\[Rule]", " ", "MB"}], ",", 
      RowBox[{"p", "\[Rule]", " ", "q"}], ",", 
      RowBox[{"q", "\[Rule]", " ", "p"}]}], "}"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.8213243607248383`*^9, 3.821324361770145*^9}, {
   3.821324407491097*^9, 3.821324408303217*^9}, {3.821324515940207*^9, 
   3.821324516914529*^9}, {3.822384914840311*^9, 3.822384918205621*^9}, {
   3.8231602100239067`*^9, 3.823160214448065*^9}, {3.823160366729108*^9, 
   3.82316037494421*^9}, {3.82316062832167*^9, 3.823160628843568*^9}, {
   3.823161231726523*^9, 3.823161238599306*^9}, {3.8234274396660833`*^9, 
   3.823427499663363*^9}, 3.823427815879026*^9, {3.823427857280528*^9, 
   3.8234278574888897`*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"2c189459-769d-4a24-bd40-b5d06373afa9"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"ToFileName", "[", 
    RowBox[{
     RowBox[{"NotebookDirectory", "[", "]"}], ",", 
     "\"\<../1. EtaB+Jpsi/data\>\""}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AmpEBJpsi", "=", 
   RowBox[{"<<", "AmpABC`"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AmpEBJpsi", "=", 
   RowBox[{
    RowBox[{"AmpEBJpsi", "//", "Expand"}], "//", "Simplify"}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.809443652367745*^9, 3.809443676899459*^9}, {
  3.8094441467748013`*^9, 3.809444150613722*^9}, {3.809445264532439*^9, 
  3.8094452675875063`*^9}, {3.8134083033021584`*^9, 3.813408308286025*^9}, {
  3.813492464228045*^9, 3.8134924675793953`*^9}, {3.815124695481485*^9, 
  3.8151247331096992`*^9}, {3.820557650936648*^9, 3.820557657374689*^9}, {
  3.821324278256679*^9, 3.821324412407524*^9}, {3.8223848962707233`*^9, 
  3.822384907953599*^9}, {3.823160219776136*^9, 3.823160226711475*^9}, {
  3.823160380905251*^9, 3.8231603820477343`*^9}, {3.8231604257614927`*^9, 
  3.823160427555726*^9}, {3.8231612455417147`*^9, 3.8231612509504757`*^9}, {
  3.8234275059610023`*^9, 3.8234275329693327`*^9}, {3.8234278190876226`*^9, 
  3.823427819924341*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"a66a3dfd-91d8-4cc6-aa86-1edb51e718c3"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"upsEtac", "=", 
  RowBox[{
   RowBox[{"AmpUpsECreversed", "/.", 
    RowBox[{"D", "\[Rule]", " ", 
     RowBox[{"4", "-", 
      RowBox[{"2", "\[Epsilon]"}]}]}]}], "//", 
   "LoopRefine"}]}], "\[IndentingNewLine]", 
 RowBox[{"jpsiEtab", "=", 
  RowBox[{
   RowBox[{"AmpEBJpsi", "/.", 
    RowBox[{"D", "\[Rule]", " ", 
     RowBox[{"4", "-", 
      RowBox[{"2", "\[Epsilon]"}]}]}]}], "//", "LoopRefine"}]}]}], "Input",
 CellChangeTimes->{{3.8231613039986563`*^9, 3.8231613327744837`*^9}, {
  3.823427543281733*^9, 3.823427567519289*^9}, {3.823427853389387*^9, 
  3.823427855420519*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"ab3e1b2b-a873-4808-9bbc-b66846d25673"],

Cell[BoxData[
 FormBox[
  RowBox[{
   SuperscriptBox["\[CurlyEpsilon]", 
    RowBox[{
    "li1", "\[InvisibleComma]", "p", "\[InvisibleComma]", "Psi1", 
     "\[InvisibleComma]", "q"}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{
       "128", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU",
         " ", 
        SuperscriptBox["MB", "2"], " ", "MC", " ", 
        SuperscriptBox[
         RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
        TooltipBox[
         RowBox[{
          SubscriptBox["C", "0"], "(", 
          RowBox[{
           RowBox[{"4", " ", 
            SuperscriptBox["MB", "2"]}], ",", 
           SuperscriptBox["MC", "2"], ",", 
           RowBox[{
            RowBox[{"2", " ", 
             SuperscriptBox["MB", "2"]}], "-", 
            SuperscriptBox["MC", "2"], "+", 
            FractionBox["s", "2"]}]}], ";", 
          RowBox[{"0", ",", "0", ",", "MC"}], ")"}],
         "ScalarC0"]}], 
       RowBox[{
        RowBox[{"16", " ", 
         SuperscriptBox["MB", "4"]}], "-", 
        RowBox[{"16", " ", 
         SuperscriptBox["MC", "4"]}], "+", 
        RowBox[{"8", " ", 
         SuperscriptBox["MC", "2"], " ", "s"}], "-", 
        SuperscriptBox["s", "2"]}]]}], "-", 
     FractionBox[
      RowBox[{
      "128", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU", 
       " ", 
       SuperscriptBox["MC", "3"], " ", 
       SuperscriptBox[
        RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"4", " ", 
          SuperscriptBox["MB", "2"]}], "-", 
         RowBox[{"4", " ", 
          SuperscriptBox["MC", "2"]}], "+", "s"}], ")"}], " ", 
       TooltipBox[
        RowBox[{
         SubscriptBox["C", "0"], "(", 
         RowBox[{
          SuperscriptBox["MC", "2"], ",", 
          RowBox[{
           RowBox[{"2", " ", 
            SuperscriptBox["MB", "2"]}], "-", 
           SuperscriptBox["MC", "2"], "+", 
           FractionBox["s", "2"]}], ",", "s"}], ";", 
         RowBox[{"MC", ",", "0", ",", "MC"}], ")"}],
        "ScalarC0"]}], 
      RowBox[{
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "6"]}], "-", 
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "4"], " ", 
        SuperscriptBox["MC", "2"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MB", "4"], " ", "s"}], "-", 
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "4"]}], "-", 
       RowBox[{"32", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "2"], " ", "s"}], "+", 
       RowBox[{"12", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["s", "2"]}], "+", 
       RowBox[{"64", " ", 
        SuperscriptBox["MC", "6"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MC", "4"], " ", "s"}], "+", 
       RowBox[{"12", " ", 
        SuperscriptBox["MC", "2"], " ", 
        SuperscriptBox["s", "2"]}], "-", 
       SuperscriptBox["s", "3"]}]], "-", 
     FractionBox[
      RowBox[{
      "128", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU", 
       " ", "MC", " ", "s", " ", 
       SuperscriptBox[
        RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
       TooltipBox[
        RowBox[{"\[CapitalLambda]", "(", 
         RowBox[{"s", ",", "MC", ",", "MC"}], ")"}],
        "DiscB"]}], 
      RowBox[{
       RowBox[{"-", 
        RowBox[{"64", " ", 
         SuperscriptBox["MB", "6"]}]}], "+", 
       RowBox[{"192", " ", 
        SuperscriptBox["MB", "4"], " ", 
        SuperscriptBox["MC", "2"]}], "+", 
       RowBox[{"16", " ", 
        SuperscriptBox["MB", "4"], " ", "s"}], "-", 
       RowBox[{"192", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "4"]}], "+", 
       RowBox[{"32", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "2"], " ", "s"}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["s", "2"]}], "+", 
       RowBox[{"64", " ", 
        SuperscriptBox["MC", "6"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MC", "4"], " ", "s"}], "+", 
       RowBox[{"12", " ", 
        SuperscriptBox["MC", "2"], " ", 
        SuperscriptBox["s", "2"]}], "-", 
       SuperscriptBox["s", "3"]}]], "-", 
     FractionBox[
      RowBox[{
      "32", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU", 
       " ", "MC", " ", 
       SuperscriptBox[
        RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"4", " ", 
          SuperscriptBox["MB", "2"]}], "-", 
         RowBox[{"4", " ", 
          SuperscriptBox["MC", "2"]}], "+", 
         RowBox[{"3", " ", "s"}]}], ")"}], " ", 
       RowBox[{"log", "(", 
        FractionBox[
         RowBox[{"2", " ", 
          SuperscriptBox["MC", "2"]}], 
         RowBox[{
          RowBox[{"-", 
           RowBox[{"4", " ", 
            SuperscriptBox["MB", "2"]}]}], "+", 
          RowBox[{"4", " ", 
           SuperscriptBox["MC", "2"]}], "-", "s"}]], ")"}]}], 
      RowBox[{
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "6"]}], "-", 
       RowBox[{"160", " ", 
        SuperscriptBox["MB", "4"], " ", 
        SuperscriptBox["MC", "2"]}], "-", 
       RowBox[{"16", " ", 
        SuperscriptBox["MB", "4"], " ", "s"}], "+", 
       RowBox[{"128", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "4"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "2"], " ", "s"}], "-", 
       RowBox[{"4", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["s", "2"]}], "-", 
       RowBox[{"32", " ", 
        SuperscriptBox["MC", "6"]}], "+", 
       RowBox[{"32", " ", 
        SuperscriptBox["MC", "4"], " ", "s"}], "-", 
       RowBox[{"10", " ", 
        SuperscriptBox["MC", "2"], " ", 
        SuperscriptBox["s", "2"]}], "+", 
       SuperscriptBox["s", "3"]}]]}], ")"}]}], TraditionalForm]], "Output",
 CellChangeTimes->{3.823161344194634*^9, 3.823251977206678*^9, 
  3.823425501166541*^9, 3.8234276005075207`*^9, 3.823427877640324*^9, 
  3.8242171311122932`*^9},
 CellLabel->"Out[9]=",ExpressionUUID->"c1ffe1ba-09c9-41e6-9121-f999befc66b8"],

Cell[BoxData[
 FormBox[
  RowBox[{
   SuperscriptBox["\[CurlyEpsilon]", 
    RowBox[{
    "li1", "\[InvisibleComma]", "p", "\[InvisibleComma]", "Psi1", 
     "\[InvisibleComma]", "q"}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{
       "128", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU",
         " ", 
        SuperscriptBox["MB", "2"], " ", "MC", " ", 
        SuperscriptBox[
         RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
        TooltipBox[
         RowBox[{
          SubscriptBox["C", "0"], "(", 
          RowBox[{
           RowBox[{"4", " ", 
            SuperscriptBox["MB", "2"]}], ",", 
           SuperscriptBox["MC", "2"], ",", 
           RowBox[{
            RowBox[{"2", " ", 
             SuperscriptBox["MB", "2"]}], "-", 
            SuperscriptBox["MC", "2"], "+", 
            FractionBox["s", "2"]}]}], ";", 
          RowBox[{"0", ",", "0", ",", "MC"}], ")"}],
         "ScalarC0"]}], 
       RowBox[{
        RowBox[{"16", " ", 
         SuperscriptBox["MB", "4"]}], "-", 
        RowBox[{"16", " ", 
         SuperscriptBox["MC", "4"]}], "+", 
        RowBox[{"8", " ", 
         SuperscriptBox["MC", "2"], " ", "s"}], "-", 
        SuperscriptBox["s", "2"]}]]}], "-", 
     FractionBox[
      RowBox[{
      "128", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU", 
       " ", 
       SuperscriptBox["MC", "3"], " ", 
       SuperscriptBox[
        RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"4", " ", 
          SuperscriptBox["MB", "2"]}], "-", 
         RowBox[{"4", " ", 
          SuperscriptBox["MC", "2"]}], "+", "s"}], ")"}], " ", 
       TooltipBox[
        RowBox[{
         SubscriptBox["C", "0"], "(", 
         RowBox[{
          SuperscriptBox["MC", "2"], ",", 
          RowBox[{
           RowBox[{"2", " ", 
            SuperscriptBox["MB", "2"]}], "-", 
           SuperscriptBox["MC", "2"], "+", 
           FractionBox["s", "2"]}], ",", "s"}], ";", 
         RowBox[{"MC", ",", "0", ",", "MC"}], ")"}],
        "ScalarC0"]}], 
      RowBox[{
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "6"]}], "-", 
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "4"], " ", 
        SuperscriptBox["MC", "2"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MB", "4"], " ", "s"}], "-", 
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "4"]}], "-", 
       RowBox[{"32", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "2"], " ", "s"}], "+", 
       RowBox[{"12", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["s", "2"]}], "+", 
       RowBox[{"64", " ", 
        SuperscriptBox["MC", "6"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MC", "4"], " ", "s"}], "+", 
       RowBox[{"12", " ", 
        SuperscriptBox["MC", "2"], " ", 
        SuperscriptBox["s", "2"]}], "-", 
       SuperscriptBox["s", "3"]}]], "-", 
     FractionBox[
      RowBox[{
      "128", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU", 
       " ", "MC", " ", "s", " ", 
       SuperscriptBox[
        RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
       TooltipBox[
        RowBox[{"\[CapitalLambda]", "(", 
         RowBox[{"s", ",", "MC", ",", "MC"}], ")"}],
        "DiscB"]}], 
      RowBox[{
       RowBox[{"-", 
        RowBox[{"64", " ", 
         SuperscriptBox["MB", "6"]}]}], "+", 
       RowBox[{"192", " ", 
        SuperscriptBox["MB", "4"], " ", 
        SuperscriptBox["MC", "2"]}], "+", 
       RowBox[{"16", " ", 
        SuperscriptBox["MB", "4"], " ", "s"}], "-", 
       RowBox[{"192", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "4"]}], "+", 
       RowBox[{"32", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "2"], " ", "s"}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["s", "2"]}], "+", 
       RowBox[{"64", " ", 
        SuperscriptBox["MC", "6"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MC", "4"], " ", "s"}], "+", 
       RowBox[{"12", " ", 
        SuperscriptBox["MC", "2"], " ", 
        SuperscriptBox["s", "2"]}], "-", 
       SuperscriptBox["s", "3"]}]], "-", 
     FractionBox[
      RowBox[{
      "32", " ", "\[ImaginaryI]", " ", "CA", " ", "CF", " ", "e", " ", "EU", 
       " ", "MC", " ", 
       SuperscriptBox[
        RowBox[{"SMP", "(", "\<\"g_s\"\>", ")"}], "4"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"4", " ", 
          SuperscriptBox["MB", "2"]}], "-", 
         RowBox[{"4", " ", 
          SuperscriptBox["MC", "2"]}], "+", 
         RowBox[{"3", " ", "s"}]}], ")"}], " ", 
       RowBox[{"log", "(", 
        FractionBox[
         RowBox[{"2", " ", 
          SuperscriptBox["MC", "2"]}], 
         RowBox[{
          RowBox[{"-", 
           RowBox[{"4", " ", 
            SuperscriptBox["MB", "2"]}]}], "+", 
          RowBox[{"4", " ", 
           SuperscriptBox["MC", "2"]}], "-", "s"}]], ")"}]}], 
      RowBox[{
       RowBox[{"64", " ", 
        SuperscriptBox["MB", "6"]}], "-", 
       RowBox[{"160", " ", 
        SuperscriptBox["MB", "4"], " ", 
        SuperscriptBox["MC", "2"]}], "-", 
       RowBox[{"16", " ", 
        SuperscriptBox["MB", "4"], " ", "s"}], "+", 
       RowBox[{"128", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "4"]}], "-", 
       RowBox[{"48", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["MC", "2"], " ", "s"}], "-", 
       RowBox[{"4", " ", 
        SuperscriptBox["MB", "2"], " ", 
        SuperscriptBox["s", "2"]}], "-", 
       RowBox[{"32", " ", 
        SuperscriptBox["MC", "6"]}], "+", 
       RowBox[{"32", " ", 
        SuperscriptBox["MC", "4"], " ", "s"}], "-", 
       RowBox[{"10", " ", 
        SuperscriptBox["MC", "2"], " ", 
        SuperscriptBox["s", "2"]}], "+", 
       SuperscriptBox["s", "3"]}]]}], ")"}]}], TraditionalForm]], "Output",
 CellChangeTimes->{3.823161344194634*^9, 3.823251977206678*^9, 
  3.823425501166541*^9, 3.8234276005075207`*^9, 3.823427877640324*^9, 
  3.824217131225541*^9},
 CellLabel->"Out[10]=",ExpressionUUID->"ad0e6d40-4473-4435-9c79-26c6ebd090b6"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Check", "Section",
 CellChangeTimes->{{3.823161446405167*^9, 
  3.8231614547094793`*^9}},ExpressionUUID->"1cd8847b-9be4-45ab-a8bf-\
c0532b9aa0e2"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"ratio", "=", 
  RowBox[{"upsEtac", "/", "jpsiEtab"}]}], "\[IndentingNewLine]", 
 RowBox[{"diff", "=", 
  RowBox[{"upsEtac", "-", "jpsiEtab"}]}]}], "Input",
 CellChangeTimes->{{3.823161352784732*^9, 3.823161370906041*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"3028675f-f0b7-4f1f-acd6-6da49c533f7c"],

Cell[BoxData[
 FormBox["1", TraditionalForm]], "Output",
 CellChangeTimes->{{3.8231613621064863`*^9, 3.823161371223328*^9}, 
   3.823251977364374*^9, 3.823425501288931*^9, 3.8234276008029222`*^9, 
   3.823427877800206*^9, 3.824217131271161*^9},
 CellLabel->"Out[11]=",ExpressionUUID->"6c6929d9-6a80-40f1-9bbe-3ff83c1e04c4"],

Cell[BoxData[
 FormBox["0", TraditionalForm]], "Output",
 CellChangeTimes->{{3.8231613621064863`*^9, 3.823161371223328*^9}, 
   3.823251977364374*^9, 3.823425501288931*^9, 3.8234276008029222`*^9, 
   3.823427877800206*^9, 3.8242171312718353`*^9},
 CellLabel->"Out[12]=",ExpressionUUID->"c8b6a3b8-ffe2-4665-8bc3-7f30f5bb297e"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1248, 1376},
WindowMargins->{{Automatic, 0}, {0, Automatic}},
FrontEndVersion->"12.0 for Linux x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 238, 4, 31, "Input",ExpressionUUID->"dc55a47d-d08d-45d1-b8ec-4e686cc93072"],
Cell[821, 28, 977, 17, 45, "Print",ExpressionUUID->"6252fe09-3e62-4a2a-bd9b-c19a1fc09f9b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1835, 50, 173, 3, 68, "Section",ExpressionUUID->"9fa8adf8-b3e1-466b-9171-4428dbf436b4"],
Cell[2011, 55, 1460, 33, 101, "Input",ExpressionUUID->"2c189459-769d-4a24-bd40-b5d06373afa9"],
Cell[3474, 90, 1322, 27, 78, "Input",ExpressionUUID->"a66a3dfd-91d8-4cc6-aa86-1edb51e718c3"],
Cell[CellGroupData[{
Cell[4821, 121, 689, 17, 55, "Input",ExpressionUUID->"ab3e1b2b-a873-4808-9bbc-b66846d25673"],
Cell[5513, 140, 6350, 176, 266, "Output",ExpressionUUID->"c1ffe1ba-09c9-41e6-9121-f999befc66b8"],
Cell[11866, 318, 6349, 176, 266, "Output",ExpressionUUID->"ad0e6d40-4473-4435-9c79-26c6ebd090b6"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[18264, 500, 152, 3, 68, "Section",ExpressionUUID->"1cd8847b-9be4-45ab-a8bf-c0532b9aa0e2"],
Cell[CellGroupData[{
Cell[18441, 507, 325, 6, 55, "Input",ExpressionUUID->"3028675f-f0b7-4f1f-acd6-6da49c533f7c"],
Cell[18769, 515, 323, 5, 34, "Output",ExpressionUUID->"6c6929d9-6a80-40f1-9bbe-3ff83c1e04c4"],
Cell[19095, 522, 325, 5, 67, "Output",ExpressionUUID->"c8b6a3b8-ffe2-4665-8bc3-7f30f5bb297e"]
}, Open  ]]
}, Open  ]]
}
]
*)

