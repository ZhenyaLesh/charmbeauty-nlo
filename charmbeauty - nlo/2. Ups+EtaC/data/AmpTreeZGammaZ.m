(* Created with the Wolfram Language : www.wolfram.com *)
(e^3*Rb*Rc*(-16*MB^6 + 48*MB^4*MC^2 - 32*MB^2*MC^4 - 4*MB^4*MZ^2 + 
   4*MB^2*MC^2*MZ^2 - 4*MB^4*s - 8*MB^2*MC^2*s + 3*MB^2*MZ^2*s - 
   64*ED*MB^6*SW^2 + 192*ED*MB^4*MC^2*SW^2 - 128*EU*MB^4*MC^2*SW^2 - 
   256*CW^2*ED*EU*MB^4*MC^2*SW^2 - 128*ED*MB^2*MC^4*SW^2 + 
   128*EU*MB^2*MC^4*SW^2 + 256*CW^2*ED*EU*MB^2*MC^4*SW^2 - 
   16*ED*MB^4*MZ^2*SW^2 + 32*EU*MB^4*MZ^2*SW^2 + 
   64*CW^2*ED*EU*MB^4*MZ^2*SW^2 + 16*ED*MB^2*MC^2*MZ^2*SW^2 - 
   32*EU*MB^2*MC^2*MZ^2*SW^2 - 64*CW^2*ED*EU*MC^4*MZ^2*SW^2 - 
   16*CW^2*ED*EU*MB^2*MZ^4*SW^2 + 16*CW^2*ED*EU*MC^2*MZ^4*SW^2 - 
   16*ED*MB^4*s*SW^2 - 32*ED*MB^2*MC^2*s*SW^2 + 32*EU*MB^2*MC^2*s*SW^2 + 
   64*CW^2*ED*EU*MB^2*MC^2*s*SW^2 + 12*ED*MB^2*MZ^2*s*SW^2 - 
   8*EU*MB^2*MZ^2*s*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^2*s*SW^2 - 
   16*CW^2*ED*EU*MC^2*MZ^2*s*SW^2 + 4*CW^2*ED*EU*MZ^4*s*SW^2 - 
   512*ED*EU*MB^4*MC^2*SW^4 + 256*EU^2*MB^4*MC^2*SW^4 + 
   1024*CW^2*ED*EU^2*MB^4*MC^2*SW^4 + 512*ED*EU*MB^2*MC^4*SW^4 - 
   256*EU^2*MB^2*MC^4*SW^4 - 1024*CW^2*ED*EU^2*MB^2*MC^4*SW^4 + 
   128*ED*EU*MB^4*MZ^2*SW^4 - 64*EU^2*MB^4*MZ^2*SW^4 - 
   256*CW^2*ED*EU^2*MB^4*MZ^2*SW^4 - 128*ED*EU*MB^2*MC^2*MZ^2*SW^4 + 
   64*EU^2*MB^2*MC^2*MZ^2*SW^4 + 256*CW^2*ED*EU^2*MC^4*MZ^2*SW^4 + 
   64*CW^2*ED*EU^2*MB^2*MZ^4*SW^4 - 64*CW^2*ED*EU^2*MC^2*MZ^4*SW^4 + 
   128*ED*EU*MB^2*MC^2*s*SW^4 - 64*EU^2*MB^2*MC^2*s*SW^4 - 
   256*CW^2*ED*EU^2*MB^2*MC^2*s*SW^4 - 32*ED*EU*MB^2*MZ^2*s*SW^4 + 
   16*EU^2*MB^2*MZ^2*s*SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^2*s*SW^4 + 
   64*CW^2*ED*EU^2*MC^2*MZ^2*s*SW^4 - 16*CW^2*ED*EU^2*MZ^4*s*SW^4 + 
   1024*ED*EU^2*MB^4*MC^2*SW^6 - 1024*ED*EU^2*MB^2*MC^4*SW^6 - 
   256*ED*EU^2*MB^4*MZ^2*SW^6 + 256*ED*EU^2*MB^2*MC^2*MZ^2*SW^6 - 
   256*ED*EU^2*MB^2*MC^2*s*SW^6 + 64*ED*EU^2*MB^2*MZ^2*s*SW^6)*
  Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
   Momentum[q]])/(16*CW^3*MB*Sqrt[MB*MC]*(2*MB - MZ)*(2*MC - MZ)*(2*MB + MZ)*
  (2*MC + MZ)*Pi*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SUNN*SW^3)
