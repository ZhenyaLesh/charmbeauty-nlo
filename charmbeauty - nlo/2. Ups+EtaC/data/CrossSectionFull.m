(* Created with the Wolfram Language : www.wolfram.com *)
{(Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*(PreContract[(Sqrt[Pi]*Rb*Rc*(-16*MB^6 + 48*MB^4*MC^2 - 
         32*MB^2*MC^4 - 4*MB^4*MZ^2 + 4*MB^2*MC^2*MZ^2 - 4*MB^4*s - 
         8*MB^2*MC^2*s + 3*MB^2*MZ^2*s - 64*ED*MB^6*SW^2 + 
         192*ED*MB^4*MC^2*SW^2 - 128*EU*MB^4*MC^2*SW^2 - 
         256*CW^2*ED*EU*MB^4*MC^2*SW^2 - 128*ED*MB^2*MC^4*SW^2 + 
         128*EU*MB^2*MC^4*SW^2 + 256*CW^2*ED*EU*MB^2*MC^4*SW^2 - 
         16*ED*MB^4*MZ^2*SW^2 + 32*EU*MB^4*MZ^2*SW^2 + 64*CW^2*ED*EU*MB^4*
          MZ^2*SW^2 + 16*ED*MB^2*MC^2*MZ^2*SW^2 - 32*EU*MB^2*MC^2*MZ^2*SW^2 - 
         64*CW^2*ED*EU*MC^4*MZ^2*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^4*SW^2 + 
         16*CW^2*ED*EU*MC^2*MZ^4*SW^2 - 16*ED*MB^4*s*SW^2 - 
         32*ED*MB^2*MC^2*s*SW^2 + 32*EU*MB^2*MC^2*s*SW^2 + 
         64*CW^2*ED*EU*MB^2*MC^2*s*SW^2 + 12*ED*MB^2*MZ^2*s*SW^2 - 
         8*EU*MB^2*MZ^2*s*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^2*s*SW^2 - 
         16*CW^2*ED*EU*MC^2*MZ^2*s*SW^2 + 4*CW^2*ED*EU*MZ^4*s*SW^2 - 
         512*ED*EU*MB^4*MC^2*SW^4 + 256*EU^2*MB^4*MC^2*SW^4 + 
         1024*CW^2*ED*EU^2*MB^4*MC^2*SW^4 + 512*ED*EU*MB^2*MC^4*SW^4 - 
         256*EU^2*MB^2*MC^4*SW^4 - 1024*CW^2*ED*EU^2*MB^2*MC^4*SW^4 + 
         128*ED*EU*MB^4*MZ^2*SW^4 - 64*EU^2*MB^4*MZ^2*SW^4 - 
         256*CW^2*ED*EU^2*MB^4*MZ^2*SW^4 - 128*ED*EU*MB^2*MC^2*MZ^2*SW^4 + 
         64*EU^2*MB^2*MC^2*MZ^2*SW^4 + 256*CW^2*ED*EU^2*MC^4*MZ^2*SW^4 + 
         64*CW^2*ED*EU^2*MB^2*MZ^4*SW^4 - 64*CW^2*ED*EU^2*MC^2*MZ^4*SW^4 + 
         128*ED*EU*MB^2*MC^2*s*SW^4 - 64*EU^2*MB^2*MC^2*s*SW^4 - 
         256*CW^2*ED*EU^2*MB^2*MC^2*s*SW^4 - 32*ED*EU*MB^2*MZ^2*s*SW^4 + 
         16*EU^2*MB^2*MZ^2*s*SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^2*s*SW^4 + 
         64*CW^2*ED*EU^2*MC^2*MZ^2*s*SW^4 - 16*CW^2*ED*EU^2*MZ^4*s*SW^4 + 
         1024*ED*EU^2*MB^4*MC^2*SW^6 - 1024*ED*EU^2*MB^2*MC^4*SW^6 - 
         256*ED*EU^2*MB^4*MZ^2*SW^6 + 256*ED*EU^2*MB^2*MC^2*MZ^2*SW^6 - 
         256*ED*EU^2*MB^2*MC^2*s*SW^6 + 64*ED*EU^2*MB^2*MZ^2*s*SW^6)*
        \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
         Momentum[p], Momentum[q]]*((-2*I)*(-1 + 4*SW^2)*
          Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Gamma]1], Momentum[p1], 
           Momentum[p2]] + (1 + (-1 + 4*SW^2)^2)*
          (-(s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Gamma]1]])/2 + 
           Pair[LorentzIndex[\[Gamma]], Momentum[p2]]*
            Pair[LorentzIndex[\[Gamma]1], Momentum[p1]] + 
           Pair[LorentzIndex[\[Gamma]], Momentum[p1]]*
            Pair[LorentzIndex[\[Gamma]1], Momentum[p2]]))*
        (-Pair[LorentzIndex[\[Psi]], LorentzIndex[\[Psi]1]] + 
         (Pair[LorentzIndex[\[Psi]], Momentum[p]]*Pair[LorentzIndex[\[Psi]1], 
            Momentum[p]])/(4*MC^2))*PreConjugate[
         (Sqrt[Pi]*Rb*Rc*(-16*MB^6 + 48*MB^4*MC^2 - 32*MB^2*MC^4 - 
            4*MB^4*MZ^2 + 4*MB^2*MC^2*MZ^2 - 4*MB^4*s - 8*MB^2*MC^2*s + 
            3*MB^2*MZ^2*s - 64*ED*MB^6*SW^2 + 192*ED*MB^4*MC^2*SW^2 - 
            128*EU*MB^4*MC^2*SW^2 - 256*CW^2*ED*EU*MB^4*MC^2*SW^2 - 
            128*ED*MB^2*MC^4*SW^2 + 128*EU*MB^2*MC^4*SW^2 + 
            256*CW^2*ED*EU*MB^2*MC^4*SW^2 - 16*ED*MB^4*MZ^2*SW^2 + 
            32*EU*MB^4*MZ^2*SW^2 + 64*CW^2*ED*EU*MB^4*MZ^2*SW^2 + 
            16*ED*MB^2*MC^2*MZ^2*SW^2 - 32*EU*MB^2*MC^2*MZ^2*SW^2 - 
            64*CW^2*ED*EU*MC^4*MZ^2*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^4*SW^2 + 
            16*CW^2*ED*EU*MC^2*MZ^4*SW^2 - 16*ED*MB^4*s*SW^2 - 
            32*ED*MB^2*MC^2*s*SW^2 + 32*EU*MB^2*MC^2*s*SW^2 + 
            64*CW^2*ED*EU*MB^2*MC^2*s*SW^2 + 12*ED*MB^2*MZ^2*s*SW^2 - 
            8*EU*MB^2*MZ^2*s*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^2*s*SW^2 - 
            16*CW^2*ED*EU*MC^2*MZ^2*s*SW^2 + 4*CW^2*ED*EU*MZ^4*s*SW^2 - 
            512*ED*EU*MB^4*MC^2*SW^4 + 256*EU^2*MB^4*MC^2*SW^4 + 
            1024*CW^2*ED*EU^2*MB^4*MC^2*SW^4 + 512*ED*EU*MB^2*MC^4*SW^4 - 
            256*EU^2*MB^2*MC^4*SW^4 - 1024*CW^2*ED*EU^2*MB^2*MC^4*SW^4 + 
            128*ED*EU*MB^4*MZ^2*SW^4 - 64*EU^2*MB^4*MZ^2*SW^4 - 
            256*CW^2*ED*EU^2*MB^4*MZ^2*SW^4 - 128*ED*EU*MB^2*MC^2*MZ^2*SW^4 + 
            64*EU^2*MB^2*MC^2*MZ^2*SW^4 + 256*CW^2*ED*EU^2*MC^4*MZ^2*SW^4 + 
            64*CW^2*ED*EU^2*MB^2*MZ^4*SW^4 - 64*CW^2*ED*EU^2*MC^2*MZ^4*SW^4 + 
            128*ED*EU*MB^2*MC^2*s*SW^4 - 64*EU^2*MB^2*MC^2*s*SW^4 - 
            256*CW^2*ED*EU^2*MB^2*MC^2*s*SW^4 - 32*ED*EU*MB^2*MZ^2*s*SW^4 + 
            16*EU^2*MB^2*MZ^2*s*SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^2*s*SW^4 + 
            64*CW^2*ED*EU^2*MC^2*MZ^2*s*SW^4 - 16*CW^2*ED*EU^2*MZ^4*s*SW^4 + 
            1024*ED*EU^2*MB^4*MC^2*SW^6 - 1024*ED*EU^2*MB^2*MC^4*SW^6 - 
            256*ED*EU^2*MB^4*MZ^2*SW^6 + 256*ED*EU^2*MB^2*MC^2*MZ^2*SW^6 - 
            256*ED*EU^2*MB^2*MC^2*s*SW^6 + 64*ED*EU^2*MB^2*MZ^2*s*SW^6)*
           \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]1], 
            Momentum[p], Momentum[q]])/(6*CW^3*MB*Sqrt[MB*MC]*(2*MB - MZ)*
           (2*MC - MZ)*(2*MB + MZ)*(2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*
           (4*MB^2 - 4*MC^2 + s)*SW^3)])/(6*CW^3*MB*Sqrt[MB*MC]*(2*MB - MZ)*
        (2*MC - MZ)*(2*MB + MZ)*(2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*
        (4*MB^2 - 4*MC^2 + s)*SW^3)]/(16*CW^2*SW^2*((-MZ^2 + s)^2 + 
       MZ^2*\[CapitalGamma]^2)) + 
    PreContract[(2*EU*Sqrt[Pi]*Rb*Rc*(-4*CW^2*ED*EU*MZ^2*SW^2 + 
         MB^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
        \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
         Momentum[p], Momentum[q]]*
        (-(s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Gamma]1]])/2 + 
         Pair[LorentzIndex[\[Gamma]], Momentum[p2]]*
          Pair[LorentzIndex[\[Gamma]1], Momentum[p1]] + 
         Pair[LorentzIndex[\[Gamma]], Momentum[p1]]*
          Pair[LorentzIndex[\[Gamma]1], Momentum[p2]])*
        (-Pair[LorentzIndex[\[Psi]], LorentzIndex[\[Psi]1]] + 
         (Pair[LorentzIndex[\[Psi]], Momentum[p]]*Pair[LorentzIndex[\[Psi]1], 
            Momentum[p]])/(4*MC^2))*PreConjugate[
         (2*EU*Sqrt[Pi]*Rb*Rc*(-4*CW^2*ED*EU*MZ^2*SW^2 + 
            MB^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
           \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]1], 
            Momentum[p], Momentum[q]])/(3*CW^2*MB*Sqrt[MB*MC]*(2*MB - MZ)*
           (2*MB + MZ)*(4*MB^2 - 4*MC^2 + s)*SW^2)])/(3*CW^2*MB*Sqrt[MB*MC]*
        (2*MB - MZ)*(2*MB + MZ)*(4*MB^2 - 4*MC^2 + s)*SW^2)]/s^2 + 
    ((-MZ^2 + s)*Re[PreContract[(Sqrt[Pi]*Rb*Rc*(-16*MB^6 + 48*MB^4*MC^2 - 
           32*MB^2*MC^4 - 4*MB^4*MZ^2 + 4*MB^2*MC^2*MZ^2 - 4*MB^4*s - 
           8*MB^2*MC^2*s + 3*MB^2*MZ^2*s - 64*ED*MB^6*SW^2 + 
           192*ED*MB^4*MC^2*SW^2 - 128*EU*MB^4*MC^2*SW^2 - 
           256*CW^2*ED*EU*MB^4*MC^2*SW^2 - 128*ED*MB^2*MC^4*SW^2 + 
           128*EU*MB^2*MC^4*SW^2 + 256*CW^2*ED*EU*MB^2*MC^4*SW^2 - 
           16*ED*MB^4*MZ^2*SW^2 + 32*EU*MB^4*MZ^2*SW^2 + 64*CW^2*ED*EU*MB^4*
            MZ^2*SW^2 + 16*ED*MB^2*MC^2*MZ^2*SW^2 - 32*EU*MB^2*MC^2*MZ^2*
            SW^2 - 64*CW^2*ED*EU*MC^4*MZ^2*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^4*
            SW^2 + 16*CW^2*ED*EU*MC^2*MZ^4*SW^2 - 16*ED*MB^4*s*SW^2 - 
           32*ED*MB^2*MC^2*s*SW^2 + 32*EU*MB^2*MC^2*s*SW^2 + 
           64*CW^2*ED*EU*MB^2*MC^2*s*SW^2 + 12*ED*MB^2*MZ^2*s*SW^2 - 
           8*EU*MB^2*MZ^2*s*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^2*s*SW^2 - 
           16*CW^2*ED*EU*MC^2*MZ^2*s*SW^2 + 4*CW^2*ED*EU*MZ^4*s*SW^2 - 
           512*ED*EU*MB^4*MC^2*SW^4 + 256*EU^2*MB^4*MC^2*SW^4 + 
           1024*CW^2*ED*EU^2*MB^4*MC^2*SW^4 + 512*ED*EU*MB^2*MC^4*SW^4 - 
           256*EU^2*MB^2*MC^4*SW^4 - 1024*CW^2*ED*EU^2*MB^2*MC^4*SW^4 + 
           128*ED*EU*MB^4*MZ^2*SW^4 - 64*EU^2*MB^4*MZ^2*SW^4 - 
           256*CW^2*ED*EU^2*MB^4*MZ^2*SW^4 - 128*ED*EU*MB^2*MC^2*MZ^2*SW^4 + 
           64*EU^2*MB^2*MC^2*MZ^2*SW^4 + 256*CW^2*ED*EU^2*MC^4*MZ^2*SW^4 + 
           64*CW^2*ED*EU^2*MB^2*MZ^4*SW^4 - 64*CW^2*ED*EU^2*MC^2*MZ^4*SW^4 + 
           128*ED*EU*MB^2*MC^2*s*SW^4 - 64*EU^2*MB^2*MC^2*s*SW^4 - 
           256*CW^2*ED*EU^2*MB^2*MC^2*s*SW^4 - 32*ED*EU*MB^2*MZ^2*s*SW^4 + 
           16*EU^2*MB^2*MZ^2*s*SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^2*s*SW^4 + 
           64*CW^2*ED*EU^2*MC^2*MZ^2*s*SW^4 - 16*CW^2*ED*EU^2*MZ^4*s*SW^4 + 
           1024*ED*EU^2*MB^4*MC^2*SW^6 - 1024*ED*EU^2*MB^2*MC^4*SW^6 - 
           256*ED*EU^2*MB^4*MZ^2*SW^6 + 256*ED*EU^2*MB^2*MC^2*MZ^2*SW^6 - 
           256*ED*EU^2*MB^2*MC^2*s*SW^6 + 64*ED*EU^2*MB^2*MZ^2*s*SW^6)*
          \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
           Momentum[p], Momentum[q]]*((-I)*Eps[LorentzIndex[\[Gamma]], 
             LorentzIndex[\[Gamma]1], Momentum[p1], Momentum[p2]] + 
           (-1 + 4*SW^2)*(-(s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[
                  \[Gamma]1]])/2 + Pair[LorentzIndex[\[Gamma]], Momentum[p2]]*
              Pair[LorentzIndex[\[Gamma]1], Momentum[p1]] + 
             Pair[LorentzIndex[\[Gamma]], Momentum[p1]]*Pair[LorentzIndex[
                \[Gamma]1], Momentum[p2]]))*
          (-Pair[LorentzIndex[\[Psi]], LorentzIndex[\[Psi]1]] + 
           (Pair[LorentzIndex[\[Psi]], Momentum[p]]*Pair[LorentzIndex[
               \[Psi]1], Momentum[p]])/(4*MC^2))*PreConjugate[
           (2*EU*Sqrt[Pi]*Rb*Rc*(-4*CW^2*ED*EU*MZ^2*SW^2 + MB^2*(-1 + 
                4*EU*SW^2 + 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
             \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]1], LorentzIndex[
               \[Psi]1], Momentum[p], Momentum[q]])/(3*CW^2*MB*Sqrt[MB*MC]*
             (2*MB - MZ)*(2*MB + MZ)*(4*MB^2 - 4*MC^2 + s)*SW^2)])/
         (6*CW^3*MB*Sqrt[MB*MC]*(2*MB - MZ)*(2*MC - MZ)*(2*MB + MZ)*
          (2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3)]])/
     (2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2))))/(8*s), 
 (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*(Re[PreContract[ConditionalExpression[
        ((-64*MB*MC^2*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                   8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - 16*MC^4 - 8*MB^2*s + s^2)*SW) - 
          (64*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*
            \[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
             DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MB^2*(4*MB^2 - 4*MC^2 - s))), -4*MB^2 + 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
                  2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 
               4*MB^2 - 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                   (4*MC^2 - s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
               (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s) + 2*(4*MC^2 + s)*
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                    s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*
                      MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 2*MC^2 - s/2 + 
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                    s))/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MC^4 - s^2/4 + 
                MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
          (((16*I)/3)*MB*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 3*s)*
               Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + ((4*I)*Sqrt[s*(-4*MB^2 + s)]*
               (I*Pi + Log[-(2*MB^2 - s + Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/
              (64*MB^6 - 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + 
                 s) + 4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/
           (CW*Sqrt[MB*MC]*Sqrt[Pi]*SW))*((-2*I)*(-1 + 4*SW^2)*
           Eps[LorentzIndex[\[Gamma]], LorentzIndex[li1], Momentum[p1], 
            Momentum[p2]] + (1 + (-1 + 4*SW^2)^2)*
           (-(s*Pair[LorentzIndex[li1], LorentzIndex[\[Gamma]]])/2 + 
            Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[
               \[Gamma]], Momentum[p1]] + Pair[LorentzIndex[li1], 
              Momentum[p1]]*Pair[LorentzIndex[\[Gamma]], Momentum[p2]]))*
         (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
          (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
             Momentum[p]])/(4*MC^2))*PreConjugate[
          (Sqrt[Pi]*Rb*Rc*(-16*MB^6 + 48*MB^4*MC^2 - 32*MB^2*MC^4 - 
             4*MB^4*MZ^2 + 4*MB^2*MC^2*MZ^2 - 4*MB^4*s - 8*MB^2*MC^2*s + 
             3*MB^2*MZ^2*s - 64*ED*MB^6*SW^2 + 192*ED*MB^4*MC^2*SW^2 - 
             128*EU*MB^4*MC^2*SW^2 - 256*CW^2*ED*EU*MB^4*MC^2*SW^2 - 
             128*ED*MB^2*MC^4*SW^2 + 128*EU*MB^2*MC^4*SW^2 + 
             256*CW^2*ED*EU*MB^2*MC^4*SW^2 - 16*ED*MB^4*MZ^2*SW^2 + 
             32*EU*MB^4*MZ^2*SW^2 + 64*CW^2*ED*EU*MB^4*MZ^2*SW^2 + 
             16*ED*MB^2*MC^2*MZ^2*SW^2 - 32*EU*MB^2*MC^2*MZ^2*SW^2 - 
             64*CW^2*ED*EU*MC^4*MZ^2*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^4*SW^2 + 
             16*CW^2*ED*EU*MC^2*MZ^4*SW^2 - 16*ED*MB^4*s*SW^2 - 
             32*ED*MB^2*MC^2*s*SW^2 + 32*EU*MB^2*MC^2*s*SW^2 + 
             64*CW^2*ED*EU*MB^2*MC^2*s*SW^2 + 12*ED*MB^2*MZ^2*s*SW^2 - 
             8*EU*MB^2*MZ^2*s*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^2*s*SW^2 - 
             16*CW^2*ED*EU*MC^2*MZ^2*s*SW^2 + 4*CW^2*ED*EU*MZ^4*s*SW^2 - 
             512*ED*EU*MB^4*MC^2*SW^4 + 256*EU^2*MB^4*MC^2*SW^4 + 
             1024*CW^2*ED*EU^2*MB^4*MC^2*SW^4 + 512*ED*EU*MB^2*MC^4*SW^4 - 
             256*EU^2*MB^2*MC^4*SW^4 - 1024*CW^2*ED*EU^2*MB^2*MC^4*SW^4 + 
             128*ED*EU*MB^4*MZ^2*SW^4 - 64*EU^2*MB^4*MZ^2*SW^4 - 
             256*CW^2*ED*EU^2*MB^4*MZ^2*SW^4 - 128*ED*EU*MB^2*MC^2*MZ^2*
              SW^4 + 64*EU^2*MB^2*MC^2*MZ^2*SW^4 + 256*CW^2*ED*EU^2*MC^4*MZ^2*
              SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^4*SW^4 - 64*CW^2*ED*EU^2*MC^2*
              MZ^4*SW^4 + 128*ED*EU*MB^2*MC^2*s*SW^4 - 64*EU^2*MB^2*MC^2*s*
              SW^4 - 256*CW^2*ED*EU^2*MB^2*MC^2*s*SW^4 - 32*ED*EU*MB^2*MZ^2*s*
              SW^4 + 16*EU^2*MB^2*MZ^2*s*SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^2*s*
              SW^4 + 64*CW^2*ED*EU^2*MC^2*MZ^2*s*SW^4 - 16*CW^2*ED*EU^2*MZ^4*
              s*SW^4 + 1024*ED*EU^2*MB^4*MC^2*SW^6 - 1024*ED*EU^2*MB^2*MC^4*
              SW^6 - 256*ED*EU^2*MB^4*MZ^2*SW^6 + 256*ED*EU^2*MB^2*MC^2*MZ^2*
              SW^6 - 256*ED*EU^2*MB^2*MC^2*s*SW^6 + 64*ED*EU^2*MB^2*MZ^2*s*
              SW^6)*\[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], 
             LorentzIndex[\[Psi]], Momentum[p], Momentum[q]])/
           (6*CW^3*MB*Sqrt[MB*MC]*(2*MB - MZ)*(2*MC - MZ)*(2*MB + MZ)*
            (2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3)], 
        16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2 > 0]]]/
     (8*CW^2*SW^2*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    ((-MZ^2 + s)*Re[PreContract[ConditionalExpression[
         ((-64*MB*MC^2*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((-16*MB^4 + 
                    32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                 s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*
                  (4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*(4*MC^2 + s) - 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 16*MC^4 - 
              8*MB^2*s + s^2)*SW) - (64*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*
             (1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - 
                      s)) - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - 
                    s))), -4*MB^2 + 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
                   2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 
                4*MB^2 - 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - 4*MC^2 - s)*
                  (4*MC^2 - s) + 2*(4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                (-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 8*MB^2*
                      MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                    (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[
               Psi1], Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
             (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
              4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
           (((16*I)/3)*MB*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 
                32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 
                 3*s)*Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 
                32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
              ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + Log[-(2*MB^2 - s + 
                     Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/(64*MB^6 - 
                48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/(CW*Sqrt[MB*MC]*
             Sqrt[Pi]*SW))*((-I)*Eps[LorentzIndex[\[Gamma]], 
             LorentzIndex[li1], Momentum[p1], Momentum[p2]] + 
           (-1 + 4*SW^2)*(-(s*Pair[LorentzIndex[li1], LorentzIndex[
                  \[Gamma]]])/2 + Pair[LorentzIndex[li1], Momentum[p2]]*
              Pair[LorentzIndex[\[Gamma]], Momentum[p1]] + 
             Pair[LorentzIndex[li1], Momentum[p1]]*Pair[LorentzIndex[
                \[Gamma]], Momentum[p2]]))*
          (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
           (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
              Momentum[p]])/(4*MC^2))*PreConjugate[
           (2*EU*Sqrt[Pi]*Rb*Rc*(-4*CW^2*ED*EU*MZ^2*SW^2 + MB^2*(-1 + 
                4*EU*SW^2 + 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
             \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
              Momentum[p], Momentum[q]])/(3*CW^2*MB*Sqrt[MB*MC]*(2*MB - MZ)*
             (2*MB + MZ)*(4*MB^2 - 4*MC^2 + s)*SW^2)], 
         16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2 > 0]]])/
     (2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    (2*Re[PreContract[((-256*ED*MB*MC^2*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                   8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - 16*MC^4 - 8*MB^2*s + s^2)) - 
          (256*ED*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - 
                     s)) - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - 
                   s))), -4*MB^2 + 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MB^2*(4*MB^2 - 4*MC^2 - s))), 4*MB^2 - 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                    s))/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - 4*MC^2 - s)*
                 (4*MC^2 - s) + 2*(4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                   (4*MC^2 - s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
               2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                   (4*MC^2 - s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
               4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                   (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
          (((64*I)/3)*ED*MB*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 3*s)*
               Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + ((4*I)*Sqrt[s*(-4*MB^2 + s)]*
               (I*Pi + Log[-(2*MB^2 - s + Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/
              (64*MB^6 - 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + 
                 s) + 4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/
           (Sqrt[MB*MC]*Sqrt[Pi]))*
         (-(s*Pair[LorentzIndex[li1], LorentzIndex[\[Gamma]]])/2 + 
          Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[\[Gamma]], 
            Momentum[p1]] + Pair[LorentzIndex[li1], Momentum[p1]]*
           Pair[LorentzIndex[\[Gamma]], Momentum[p2]])*
         (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
          (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
             Momentum[p]])/(4*MC^2))*PreConjugate[
          (2*EU*Sqrt[Pi]*Rb*Rc*(-4*CW^2*ED*EU*MZ^2*SW^2 + 
             MB^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
            \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
             Momentum[p], Momentum[q]])/(3*CW^2*MB*Sqrt[MB*MC]*(2*MB - MZ)*
            (2*MB + MZ)*(4*MB^2 - 4*MC^2 + s)*SW^2)]]])/s^2 + 
    ((-MZ^2 + s)*Re[PreContract[(Sqrt[Pi]*Rb*Rc*(-16*MB^6 + 48*MB^4*MC^2 - 
           32*MB^2*MC^4 - 4*MB^4*MZ^2 + 4*MB^2*MC^2*MZ^2 - 4*MB^4*s - 
           8*MB^2*MC^2*s + 3*MB^2*MZ^2*s - 64*ED*MB^6*SW^2 + 
           192*ED*MB^4*MC^2*SW^2 - 128*EU*MB^4*MC^2*SW^2 - 
           256*CW^2*ED*EU*MB^4*MC^2*SW^2 - 128*ED*MB^2*MC^4*SW^2 + 
           128*EU*MB^2*MC^4*SW^2 + 256*CW^2*ED*EU*MB^2*MC^4*SW^2 - 
           16*ED*MB^4*MZ^2*SW^2 + 32*EU*MB^4*MZ^2*SW^2 + 64*CW^2*ED*EU*MB^4*
            MZ^2*SW^2 + 16*ED*MB^2*MC^2*MZ^2*SW^2 - 32*EU*MB^2*MC^2*MZ^2*
            SW^2 - 64*CW^2*ED*EU*MC^4*MZ^2*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^4*
            SW^2 + 16*CW^2*ED*EU*MC^2*MZ^4*SW^2 - 16*ED*MB^4*s*SW^2 - 
           32*ED*MB^2*MC^2*s*SW^2 + 32*EU*MB^2*MC^2*s*SW^2 + 
           64*CW^2*ED*EU*MB^2*MC^2*s*SW^2 + 12*ED*MB^2*MZ^2*s*SW^2 - 
           8*EU*MB^2*MZ^2*s*SW^2 - 16*CW^2*ED*EU*MB^2*MZ^2*s*SW^2 - 
           16*CW^2*ED*EU*MC^2*MZ^2*s*SW^2 + 4*CW^2*ED*EU*MZ^4*s*SW^2 - 
           512*ED*EU*MB^4*MC^2*SW^4 + 256*EU^2*MB^4*MC^2*SW^4 + 
           1024*CW^2*ED*EU^2*MB^4*MC^2*SW^4 + 512*ED*EU*MB^2*MC^4*SW^4 - 
           256*EU^2*MB^2*MC^4*SW^4 - 1024*CW^2*ED*EU^2*MB^2*MC^4*SW^4 + 
           128*ED*EU*MB^4*MZ^2*SW^4 - 64*EU^2*MB^4*MZ^2*SW^4 - 
           256*CW^2*ED*EU^2*MB^4*MZ^2*SW^4 - 128*ED*EU*MB^2*MC^2*MZ^2*SW^4 + 
           64*EU^2*MB^2*MC^2*MZ^2*SW^4 + 256*CW^2*ED*EU^2*MC^4*MZ^2*SW^4 + 
           64*CW^2*ED*EU^2*MB^2*MZ^4*SW^4 - 64*CW^2*ED*EU^2*MC^2*MZ^4*SW^4 + 
           128*ED*EU*MB^2*MC^2*s*SW^4 - 64*EU^2*MB^2*MC^2*s*SW^4 - 
           256*CW^2*ED*EU^2*MB^2*MC^2*s*SW^4 - 32*ED*EU*MB^2*MZ^2*s*SW^4 + 
           16*EU^2*MB^2*MZ^2*s*SW^4 + 64*CW^2*ED*EU^2*MB^2*MZ^2*s*SW^4 + 
           64*CW^2*ED*EU^2*MC^2*MZ^2*s*SW^4 - 16*CW^2*ED*EU^2*MZ^4*s*SW^4 + 
           1024*ED*EU^2*MB^4*MC^2*SW^6 - 1024*ED*EU^2*MB^2*MC^4*SW^6 - 
           256*ED*EU^2*MB^4*MZ^2*SW^6 + 256*ED*EU^2*MB^2*MC^2*MZ^2*SW^6 - 
           256*ED*EU^2*MB^2*MC^2*s*SW^6 + 64*ED*EU^2*MB^2*MZ^2*s*SW^6)*
          \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
           Momentum[p], Momentum[q]]*((-I)*Eps[LorentzIndex[\[Gamma]], 
             LorentzIndex[li1], Momentum[p1], Momentum[p2]] + 
           (-1 + 4*SW^2)*(-(s*Pair[LorentzIndex[li1], LorentzIndex[
                  \[Gamma]]])/2 + Pair[LorentzIndex[li1], Momentum[p2]]*
              Pair[LorentzIndex[\[Gamma]], Momentum[p1]] + 
             Pair[LorentzIndex[li1], Momentum[p1]]*Pair[LorentzIndex[
                \[Gamma]], Momentum[p2]]))*
          (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
           (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
              Momentum[p]])/(4*MC^2))*PreConjugate[
           (-256*ED*MB*MC^2*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
              (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(8*MB^2*MC^2), -4*MC^2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(8*MB^2*MC^2), 4*MC^2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]), 2*MB^2 + 2*MC^2 - s/2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] + DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]), -2*MB^2 - 2*MC^2 + s/2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 
                     16*MC^4 + 8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2), -4*MB^4 + 4*MC^4 - s^2/4 + 2*MB^2*
                   (4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*
                   (4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/
                    4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*(4*MC^2 + s) - 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*
              Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
               Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 16*MC^4 - 8*
                MB^2*s + s^2)) - (256*ED*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*
              Sqrt[\[Alpha]]*\[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - DiLog[
                 -((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (MB^2*(4*MB^2 - 4*MC^2 - s))), -4*MB^2 + 4*MC^2 + s - 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                 -((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (MB^2*(4*MB^2 - 4*MC^2 - s))), 4*MB^2 - 4*MC^2 - s + 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + ((4*MC^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                     (4*MC^2 - s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2), (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s) + 2*(4*MC^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*
                     (4*MC^2 - s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2)/(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                   ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + ((4*MC^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                     (4*MC^2 - s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2), 4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
               DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                     (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                   Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                 (-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 8*MB^2*
                       MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MB^2 + s)*
                     (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4)]), -4*MB^2 - 4*MC^2 + s + 2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], 
               LorentzIndex[Psi1], Momentum[p], Momentum[q]])/
             (3*Sqrt[MB*MC]*Sqrt[Pi]*(64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*
                (4*MC^2 + 3*s) - 4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
            (((64*I)/3)*ED*MB*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
              Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
               Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 
                 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                 2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 
                  4*MC^2 - 3*s)*Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/
                (32*MB^6 - 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + 
                   s) + 2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
               ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + Log[-(2*MB^2 - s + 
                      Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/(64*MB^6 - 
                 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                 4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/(Sqrt[MB*MC]*
              Sqrt[Pi])])/(6*CW^3*MB*Sqrt[MB*MC]*(2*MB - MZ)*(2*MC - MZ)*
          (2*MB + MZ)*(2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*
          SW^3)]])/(2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2))))/
  (8*s), (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/
     s^2]*\[Alpha]*
   (((-MZ^2 + s)*Re[PreContract[ConditionalExpression[
         ((-64*MB*MC^2*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((-16*MB^4 + 
                    32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                 s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*
                  (4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*(4*MC^2 + s) - 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 16*MC^4 - 
              8*MB^2*s + s^2)*SW) - (64*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*
             (1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - 
                      s)) - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - 
                    s))), -4*MB^2 + 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
                   2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 
                4*MB^2 - 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - 4*MC^2 - s)*
                  (4*MC^2 - s) + 2*(4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                (-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 8*MB^2*
                      MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                    (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[
               Psi1], Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
             (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
              4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
           (((16*I)/3)*MB*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 
                32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 
                 3*s)*Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 
                32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
              ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + Log[-(2*MB^2 - s + 
                     Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/(64*MB^6 - 
                48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/(CW*Sqrt[MB*MC]*
             Sqrt[Pi]*SW))*((-I)*Eps[LorentzIndex[li1], LorentzIndex[li2], 
             Momentum[p1], Momentum[p2]] + (-1 + 4*SW^2)*
            (-(s*Pair[LorentzIndex[li1], LorentzIndex[li2]])/2 + 
             Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[li2], 
               Momentum[p1]] + Pair[LorentzIndex[li1], Momentum[p1]]*
              Pair[LorentzIndex[li2], Momentum[p2]]))*
          (-Pair[LorentzIndex[Psi1], LorentzIndex[Psi2]] + 
           (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[Psi2], 
              Momentum[p]])/(4*MC^2))*PreConjugate[
           (-256*ED*MB*MC^2*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
              (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(8*MB^2*MC^2), -4*MC^2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(8*MB^2*MC^2), 4*MC^2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]), 2*MB^2 + 2*MC^2 - s/2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] + DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 
                   4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]), -2*MB^2 - 2*MC^2 + s/2 + 
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 
                     16*MC^4 + 8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2), -4*MB^4 + 4*MC^4 - s^2/4 + 2*MB^2*
                   (4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*
                   (4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                     8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/
                    4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*(4*MC^2 + s) - 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*
              Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
               Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 16*MC^4 - 8*
                MB^2*s + s^2)) - (256*ED*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*
              Sqrt[\[Alpha]]*\[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - DiLog[
                 -((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (MB^2*(4*MB^2 - 4*MC^2 - s))), -4*MB^2 + 4*MC^2 + s - 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                 -((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (MB^2*(4*MB^2 - 4*MC^2 - s))), 4*MB^2 - 4*MC^2 - s + 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + ((4*MC^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                     (4*MC^2 - s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2), (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s) + 2*(4*MC^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*
                     (4*MC^2 - s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2)/(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                   ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + ((4*MC^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                     (4*MC^2 - s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                    2), 4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
               DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                     (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                   Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                 (-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 8*MB^2*
                       MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MB^2 + s)*
                     (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4)]), -4*MB^2 - 4*MC^2 + s + 2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li2], 
               LorentzIndex[Psi2], Momentum[p], Momentum[q]])/
             (3*Sqrt[MB*MC]*Sqrt[Pi]*(64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*
                (4*MC^2 + 3*s) - 4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
            (((64*I)/3)*ED*MB*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
              Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
               Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 
                 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                 2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 
                  4*MC^2 - 3*s)*Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/
                (32*MB^6 - 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + 
                   s) + 2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
               ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + Log[-(2*MB^2 - s + 
                      Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/(64*MB^6 - 
                 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                 4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/(Sqrt[MB*MC]*
              Sqrt[Pi])], 16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 
           8*MC^2*s + s^2 > 0]]])/(2*CW*s*SW*((-MZ^2 + s)^2 + 
       MZ^2*\[CapitalGamma]^2)) + 
    Re[PreContract[ConditionalExpression[
        ((-64*MB*MC^2*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                   8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - 16*MC^4 - 8*MB^2*s + s^2)*SW) - 
          (64*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*
            \[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
             DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MB^2*(4*MB^2 - 4*MC^2 - s))), -4*MB^2 + 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
                  2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 
               4*MB^2 - 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                   (4*MC^2 - s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
               (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s) + 2*(4*MC^2 + s)*
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                    s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*
                      MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 2*MC^2 - s/2 + 
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                    s))/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MC^4 - s^2/4 + 
                MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
          (((16*I)/3)*MB*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 3*s)*
               Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + ((4*I)*Sqrt[s*(-4*MB^2 + s)]*
               (I*Pi + Log[-(2*MB^2 - s + Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/
              (64*MB^6 - 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + 
                 s) + 4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/
           (CW*Sqrt[MB*MC]*Sqrt[Pi]*SW))*((-2*I)*(-1 + 4*SW^2)*
           Eps[LorentzIndex[li1], LorentzIndex[li2], Momentum[p1], 
            Momentum[p2]] + (1 + (-1 + 4*SW^2)^2)*
           (-(s*Pair[LorentzIndex[li1], LorentzIndex[li2]])/2 + 
            Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[li2], 
              Momentum[p1]] + Pair[LorentzIndex[li1], Momentum[p1]]*
             Pair[LorentzIndex[li2], Momentum[p2]]))*
         (-Pair[LorentzIndex[Psi1], LorentzIndex[Psi2]] + 
          (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[Psi2], 
             Momentum[p]])/(4*MC^2))*PreConjugate[
          (-64*MB*MC^2*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((-16*MB^4 + 
                    32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                 s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*
                  (4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                    8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*(4*MC^2 + s) - 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*
             Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
              Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 16*MC^4 - 
              8*MB^2*s + s^2)*SW) - (64*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*
             (1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - 
                      s)) - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - 
                    s))), -4*MB^2 + 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
                   2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 
                4*MB^2 - 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - 4*MC^2 - s)*
                  (4*MC^2 - s) + 2*(4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                  ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                (-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 8*MB^2*
                      MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                    (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li2], LorentzIndex[
               Psi2], Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
             (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
              4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
           (((16*I)/3)*MB*Rb*Rc*(1 + 4*ED*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
              Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 
                32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 
                 3*s)*Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 
                32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
              ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + Log[-(2*MB^2 - s + 
                     Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/(64*MB^6 - 
                48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
                4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/(CW*Sqrt[MB*MC]*
             Sqrt[Pi]*SW)], 16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 
          8*MC^2*s + s^2 > 0]]]/(16*CW^2*SW^2*((-MZ^2 + s)^2 + 
       MZ^2*\[CapitalGamma]^2)) + 
    Re[PreContract[((-256*ED*MB*MC^2*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
           (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2), 
              4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*
                MB^2*s - 2*MC^2*s + s^2/4] - 
            DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(
                -2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
              2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*
                MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
            DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(
                -2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
              -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*
                MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
            DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 
                  16*MC^4 + 8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2), -4*MB^4 + 4*MC^4 - s^2/4 + 2*MB^2*
                (4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - 
                  s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(
                (-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/
                4 - 2*MB^2*(4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*
                s - 2*MC^2*s + s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 
                  16*MC^4 + 8*MB^2*s - s^2)/4 + ((4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                  8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*
                (4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
            Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 16*MC^4 - 
            8*MB^2*s + s^2)) - (256*ED*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*
           Sqrt[\[Alpha]]*\[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
            DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (MB^2*(4*MB^2 - 4*MC^2 - s))), -4*MB^2 + 4*MC^2 + s - 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
                 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 
              4*MB^2 - 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*
                MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
            DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                   s))/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
              (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s) + 2*(4*MC^2 + s)*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] - DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/
                 4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(
                ((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2), 2*MC^2 - s/2 + Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] + DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                   s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
              4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*
                s - 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/
                 2 - s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                 s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                 s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                 s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
            Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
           (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
            4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
         (((64*I)/3)*ED*MB*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
           Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
            Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 
              32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
              2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
            (I*(4*MB^2 - 4*MC^2 - 3*s)*Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/
             (32*MB^6 - 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
              2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
            ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + Log[-(2*MB^2 - s + 
                   Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/(64*MB^6 - 
              48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
              4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/(Sqrt[MB*MC]*Sqrt[Pi]))*
        (-(s*Pair[LorentzIndex[li1], LorentzIndex[li2]])/2 + 
         Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[li2], 
           Momentum[p1]] + Pair[LorentzIndex[li1], Momentum[p1]]*
          Pair[LorentzIndex[li2], Momentum[p2]])*
        (-Pair[LorentzIndex[Psi1], LorentzIndex[Psi2]] + 
         (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[Psi2], 
            Momentum[p]])/(4*MC^2))*PreConjugate[
         (-256*ED*MB*MC^2*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(8*MB^2*MC^2), -4*MC^2 + Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2), 4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 4*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 + 4*MC^4 - 
                s^2/4 + 2*MB^2*(4*MC^2 + s) + ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
                   8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + 
                s^2/4 - 2*MB^2*(4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4])*Eps[LorentzIndex[li2], LorentzIndex[Psi2], 
             Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - 16*MC^4 - 8*MB^2*s + s^2)) - 
          (256*ED*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - 
                     s)) - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - 
                   s))), -4*MB^2 + 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 2*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MB^2*(4*MB^2 - 4*MC^2 - s))), 4*MB^2 - 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - 
                    s))/4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - 4*MC^2 - s)*
                 (4*MC^2 - s) + 2*(4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                   (4*MC^2 - s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
               2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
                 ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*
                   (4*MC^2 - s))/4 + ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
               4*MC^4 - s^2/4 + MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
                   (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li2], LorentzIndex[Psi2], 
             Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
          (((64*I)/3)*ED*MB*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
             Momentum[q]]*((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 3*s)*
               Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 32*MB^4*
                (4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*
                (80*MC^4 + 24*MC^2*s + 5*s^2)) + ((4*I)*Sqrt[s*(-4*MB^2 + s)]*
               (I*Pi + Log[-(2*MB^2 - s + Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/
              (64*MB^6 - 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + 
                 s) + 4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2))))/
           (Sqrt[MB*MC]*Sqrt[Pi])]]]/s^2))/(8*s)}
