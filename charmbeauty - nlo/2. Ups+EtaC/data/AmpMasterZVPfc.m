(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 (I*CA*CF*e*(4*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((2 + 8*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[p, D]]) - (2*I)*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k]*((2*I)*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + (-2 + D)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    4*(3 - D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (-((-4 + D)*(4*MB^2 + 4*MC^2 - s)*(1 + 4*ED*SW^2)*
        Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
         Momentum[q, D]]) + (-2 + D)*(4*MB^2 - 4*MC^2 - s)*
       ((1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
          Momentum[p, D], Momentum[q, D]] - I*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    2*(-3 + D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - 3*s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) - 
    (4*I)*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((-8*I)*(s + 4*ED*s*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      (-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        4*s*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    8*(-3 + D)*MB^2*MC^4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 2*(4*MB^2 - 4*MC^2 + s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (-2 + D)*MC^2*IntF[-(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     (2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 2*(4*MB^2 - 4*MC^2 + s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])))*SMP["g_s"]^4)/
  (CW*(-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 ((I/2)*CA*CF*e*(4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k]*
     ((2 + 8*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[p, D]]) + 4*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((2 + 8*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] - 
      I*(-2 + D)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    8*(-3 + D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (-2*(4*MB^2 - 4*(-3 + D)*MC^2 - s)*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*(4*MB^2 - 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[p, D]]) + 2*(-3 + D)*MB^2*MC^2*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(2*(4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - 
        (-10 + D)*s)*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(4*MB^2 - 4*MC^2 - 3*s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) - 
    (4*I)*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-8*I)*(s + 4*ED*s*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + (-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 4*s*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    8*(-3 + D)*MB^2*MC^4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
       (1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        2*(4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (-2 + D)*MC^2*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     (-2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 2*(4*MB^2 - 4*MC^2 + s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])))*SMP["g_s"]^4)/
  (CW*(-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 0, 0, 
 (I*CA*CF*e*(4*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((2 + 8*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[p, D]]) + (2*I)*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k]*((-2*I)*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + (-2 + D)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    4*(3 - D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (-((-4 + D)*(4*MB^2 + 4*MC^2 - s)*(1 + 4*ED*SW^2)*
        Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
         Momentum[q, D]]) + (-2 + D)*(4*MB^2 - 4*MC^2 - s)*
       ((1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
          Momentum[p, D], Momentum[q, D]] + I*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    2*(-3 + D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - 3*s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (4*I)*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((8*I)*(s + 4*ED*s*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      (-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        4*s*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    8*(-3 + D)*MB^2*MC^4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 2*(4*MB^2 - 4*MC^2 + s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (-2 + D)*MC^2*IntF[-(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     (2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 2*(4*MB^2 - 4*MC^2 + s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])))*SMP["g_s"]^4)/
  (CW*(-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 ((I/2)*CA*CF*e*(4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k]*
     ((2 + 8*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[p, D]]) + 4*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((2 + 8*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      I*(-2 + D)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    8*(-3 + D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (-2*(4*MB^2 - 4*(-3 + D)*MC^2 - s)*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*(4*MB^2 - 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[p, D]]) + 2*(-3 + D)*MB^2*MC^2*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(2*(4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - 
        (-10 + D)*s)*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] - 
      I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(4*MB^2 - 4*MC^2 - 3*s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (4*I)*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((8*I)*(s + 4*ED*s*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + (-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 4*s*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]]*Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (-2 + D)*MC^2*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     (-2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 2*(4*MB^2 - 4*MC^2 + s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    8*(-3 + D)*MB^2*MC^4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
       (1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        2*(4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])))*SMP["g_s"]^4)/
  (CW*(-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 0, 0, 
 0, 0, 0, 0, 0, (-2*CA*CF*e*MB*((8*I)*(-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (-4*MB^2 + 4*MC^2 + s)*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
      LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] + 
    (4*I)*(-2 + D)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[p, D]])^(-1), k] - 
    (128*I)*(-3 + D)*MB^2*MC^4*(4*MB^2 - 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    (64*I)*(-3 + D)*MB^2*MC^4*(4*MB^2 - 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] - (32*I)*(-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[q, D]])*(4*MB^2 - 4*MC^2 - s - 
         2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    2*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[-(-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
    (-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      4*MC^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    2*(-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + 4*MC^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    4*(-3 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((32*MB^6 - 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
        2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      (2*(16*MB^4 - (-2 + D)*(-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*MB^4 - (4*MC^2 - s)*(4*(-4 + 3*D)*MC^2 + D*s) + 
          4*MB^2*(4*(-3 + D)*MC^2 - (1 + D)*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
    2*(-3 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((32*MB^6 - 32*MB^4*(4*MC^2 + s) - 
        (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (2*(16*MB^4 - (-2 + D)*(-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*MB^4 - (4*MC^2 - s)*(4*(-4 + 3*D)*MC^2 + D*s) + 
          4*MB^2*(4*(-3 + D)*MC^2 - (1 + D)*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    4*(-3 + D)*MB^2*MC^2*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      (4*MB^2 + 4*MC^2 - s)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(-1 + D)*
         (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) - 
    4*(-3 + D)*MB^2*MC^2*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      (4*MB^2 + 4*MC^2 - s)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(-1 + D)*
         (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (3 - D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*s*((4*I)*s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         (1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
          Momentum[p, D], Momentum[q, D]] - 
        (256*MB^8 - 256*MB^6*(4*MC^2 + s) + (-4*MC^2 + s)^2*(16*MC^4 + s^2) + 
          96*MB^4*(16*MC^4 + 4*MC^2*s + s^2) - 16*MB^2*(64*MC^6 + s^3))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        (-2*(-2*s*(16*MB^4 - 8*MB^2*s + (-4*MC^2 + s)^2) + 
            D*(64*MB^6 + s*(-4*MC^2 + s)^2 - 16*MB^4*(8*MC^2 + s) + 
              MB^2*(64*MC^4 - 4*s^2)))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] - (4*MB^2 + 4*MC^2 - s)*(-16*MC^2*s + 
            D*(-4*MB^2 + 4*MC^2 + s)^2)*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
      (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
       (s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
            8*MB^2*(-4*(-2 + D)*MC^2 + D*s))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (16*(-2 + D)*MB^4 + 8*MB^2*(-4*(-2 + D)*MC^2 + 
              s) + (4*MC^2 - s)*(4*(-2 + D)*MC^2 + D*s))*
           Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) - 
    2*(3 - D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((16*MB^4 + 16*MC^4 + 24*MC^2*s + s^2 - 8*MB^2*(4*MC^2 + s))*
       (s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
            8*MB^2*(-4*(-2 + D)*MC^2 + D*s))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (16*(-2 + D)*MB^4 + 8*MB^2*(-4*(-2 + D)*MC^2 + 
              s) + (4*MC^2 - s)*(4*(-2 + D)*MC^2 + D*s))*
           Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
      2*s*((4*I)*s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         (1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
          Momentum[p, D], Momentum[q, D]] + 
        (256*MB^8 - 256*MB^6*(4*MC^2 + s) + (-4*MC^2 + s)^2*
           (16*MC^4 + 16*MC^2*s + s^2) + 32*MB^4*(48*MC^4 + 20*MC^2*s + 
            3*s^2) - 16*MB^2*(64*MC^6 + 32*MC^4*s + 8*MC^2*s^2 + s^3))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        (2*(-2*(128*MC^6 - 48*MC^4*s + s^3 + 16*MB^4*(8*MC^2 + s) - 
              8*MB^2*(32*MC^4 + s^2)) + D*(64*MB^6 + 128*MC^6 - 16*MB^4*s - 
              48*MC^4*s + s^3 - 4*MB^2*(48*MC^4 - 16*MC^2*s + s^2)))*
           Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          (-16*MC^2*(32*MB^4 + 32*MC^4 - 4*MC^2*s - s^2 - 4*MB^2*(16*MC^2 + 
                s)) + D*(64*MB^6 + 320*MC^6 + 48*MB^4*(4*MC^2 - s) + 
              16*MC^4*s - 20*MC^2*s^2 - s^3 + MB^2*(-576*MC^4 + 32*MC^2*s + 
                12*s^2)))*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])))*SMP["g_s"]^4)/
  (CW*(-3 + D)*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)^2*s*(-4*MB^2 + 8*MB*MC - 4*MC^2 + s)^2*
   SW), (-2*CA*CF*e*MB*((8*I)*(-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (-4*MB^2 + 4*MC^2 + s)*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
      LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] + 
    (4*I)*(-2 + D)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[p, D]])^(-1), k] - 
    (128*I)*(-3 + D)*MB^2*MC^4*(4*MB^2 - 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    (64*I)*(-3 + D)*MB^2*MC^4*(4*MB^2 - 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] - (32*I)*(-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[q, D]])*(4*MB^2 - 4*MC^2 - s - 
         2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    2*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[-(-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
    (-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      4*MC^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    2*(-3 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + 4*MC^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    4*(-3 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((32*MB^6 - 32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
        2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      (2*(16*MB^4 - (-2 + D)*(-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*MB^4 - (4*MC^2 - s)*(4*(-4 + 3*D)*MC^2 + D*s) + 
          4*MB^2*(4*(-3 + D)*MC^2 - (1 + D)*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
    2*(-3 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((32*MB^6 - 32*MB^4*(4*MC^2 + s) - 
        (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (2*(16*MB^4 - (-2 + D)*(-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*MB^4 - (4*MC^2 - s)*(4*(-4 + 3*D)*MC^2 + D*s) + 
          4*MB^2*(4*(-3 + D)*MC^2 - (1 + D)*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
    4*(-3 + D)*MB^2*MC^2*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      (4*MB^2 + 4*MC^2 - s)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(-1 + D)*
         (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) - 
    4*(-3 + D)*MB^2*MC^2*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      (4*MB^2 + 4*MC^2 - s)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(-1 + D)*
         (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) + 
    (3 - D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*s*((4*I)*s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         (1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
          Momentum[p, D], Momentum[q, D]] - 
        (256*MB^8 - 256*MB^6*(4*MC^2 + s) + (-4*MC^2 + s)^2*(16*MC^4 + s^2) + 
          96*MB^4*(16*MC^4 + 4*MC^2*s + s^2) - 16*MB^2*(64*MC^6 + s^3))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        (-2*(-2*s*(16*MB^4 - 8*MB^2*s + (-4*MC^2 + s)^2) + 
            D*(64*MB^6 + s*(-4*MC^2 + s)^2 - 16*MB^4*(8*MC^2 + s) + 
              MB^2*(64*MC^4 - 4*s^2)))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] - (4*MB^2 + 4*MC^2 - s)*(-16*MC^2*s + 
            D*(-4*MB^2 + 4*MC^2 + s)^2)*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) + 
      (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
       (s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
            8*MB^2*(-4*(-2 + D)*MC^2 + D*s))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (16*(-2 + D)*MB^4 + 8*MB^2*(-4*(-2 + D)*MC^2 + 
              s) + (4*MC^2 - s)*(4*(-2 + D)*MC^2 + D*s))*
           Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])) - 
    2*(3 - D)*MC^2*(4*MB^2 + 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((16*MB^4 + 16*MC^4 + 24*MC^2*s + s^2 - 8*MB^2*(4*MC^2 + s))*
       (s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        ((16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
            8*MB^2*(-4*(-2 + D)*MC^2 + D*s))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (16*(-2 + D)*MB^4 + 8*MB^2*(-4*(-2 + D)*MC^2 + 
              s) + (4*MC^2 - s)*(4*(-2 + D)*MC^2 + D*s))*
           Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]]) - 
      2*s*((4*I)*s*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         (1 + 4*ED*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
          Momentum[p, D], Momentum[q, D]] + 
        (256*MB^8 - 256*MB^6*(4*MC^2 + s) + (-4*MC^2 + s)^2*
           (16*MC^4 + 16*MC^2*s + s^2) + 32*MB^4*(48*MC^4 + 20*MC^2*s + 
            3*s^2) - 16*MB^2*(64*MC^6 + 32*MC^4*s + 8*MC^2*s^2 + s^3))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        (2*(-2*(128*MC^6 - 48*MC^4*s + s^3 + 16*MB^4*(8*MC^2 + s) - 
              8*MB^2*(32*MC^4 + s^2)) + D*(64*MB^6 + 128*MC^6 - 16*MB^4*s - 
              48*MC^4*s + s^3 - 4*MB^2*(48*MC^4 - 16*MC^2*s + s^2)))*
           Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          (-16*MC^2*(32*MB^4 + 32*MC^4 - 4*MC^2*s - s^2 - 4*MB^2*(16*MC^2 + 
                s)) + D*(64*MB^6 + 320*MC^6 + 48*MB^4*(4*MC^2 - s) + 
              16*MC^4*s - 20*MC^2*s^2 - s^3 + MB^2*(-576*MC^4 + 32*MC^2*s + 
                12*s^2)))*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
         Pair[LorentzIndex[Psi1, D], Momentum[p, D]])))*SMP["g_s"]^4)/
  (CW*(-3 + D)*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)^2*s*(-4*MB^2 + 8*MB*MC - 4*MC^2 + s)^2*
   SW), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
