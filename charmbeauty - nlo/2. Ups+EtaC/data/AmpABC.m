(* Created with the Wolfram Language : www.wolfram.com *)
((-8*I)*CA*CF*e*ED*LTensor[LeviCivitaE, li1, p, Psi1, q]*
   (-((-2 + D)*MC^2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*PVA[0, MB]) + 
    4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*PVA[0, MC] + 
    8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*(-3 + D)*MC^2 + s)*
     PVB[0, 0, 4*MC^2, 0, 0] + 2*(-3 + D)*MB^2*MC^2*
     (4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*
     PVB[0, 0, -MB^2 + 2*MC^2 + s/2, 0, MB] - 16*(-3 + D)*MB^2*MC^2*s*
     PVB[0, 0, s/4, MC, MB] + 4*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     PVC[0, 0, 0, MC^2, s/4, -MB^2 + 2*MC^2 + s/2, 0, MC, MB] + 
    8*(-3 + D)*MB^2*MC^4*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     PVC[0, 0, 0, 4*MC^2, MB^2, -MB^2 + 2*MC^2 + s/2, 0, 0, MB])*
   SMP["g_s"]^4)/((-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)) + 
 ((8*I)*CA*CF*e*ED*LTensor[LeviCivitaE, li1, p, Psi1, q]*
   ((-2 + D)*MC^2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*PVA[0, MB] - 
    4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*PVA[0, MC] - 
    8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*(-3 + D)*MC^2 + s)*
     PVB[0, 0, 4*MC^2, 0, 0] - 2*(-3 + D)*MB^2*MC^2*
     (4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*
     PVB[0, 0, -MB^2 + 2*MC^2 + s/2, MB, 0] + 16*(-3 + D)*MB^2*MC^2*s*
     PVB[0, 0, s/4, MB, MC] - 8*(-3 + D)*MB^2*MC^4*
     (4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*PVC[0, 0, 0, 4*MC^2, 
      -MB^2 + 2*MC^2 + s/2, MB^2, 0, 0, MB] - 4*(-3 + D)*MB^2*MC^2*
     (-4*MB^2 + 4*MC^2 + s)^2*PVC[0, 0, 0, s/4, MC^2, -MB^2 + 2*MC^2 + s/2, 
      MB, MC, 0])*SMP["g_s"]^4)/((-3 + D)*(-2 + D)*MB*MC^2*
   (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)) - 
 ((32*I)*CA*CF*e*ED*MB*LTensor[LeviCivitaE, li1, p, Psi1, q]*
   ((-2*(-2 + D)*PVA[0, MC])/((-3 + D)*MC^2) + 4*PVB[0, 0, 4*MC^2, 0, 0] + 
    (4*s*PVB[0, 0, s/4, MB, MC])/(4*MB^2 - 4*MC^2 - s) + 
    (4*s*PVB[0, 0, s/4, MC, MB])/(4*MB^2 - 4*MC^2 - s) - 
    (8*s*PVB[0, 0, s, MB, MB])/(4*MB^2 - 4*MC^2 - s) - 
    (4*MB^2 - 4*MC^2 - s)*PVC[0, 0, 0, MC^2, s/4, -MB^2 + 2*MC^2 + s/2, 0, 
      MC, MB] + (32*MB^2*MC^2*PVC[0, 0, 0, 4*MC^2, MB^2, 
       -MB^2 + 2*MC^2 + s/2, 0, 0, MB])/(4*MB^2 + 4*MC^2 - s) + 
    (32*MB^2*MC^2*PVC[0, 0, 0, 4*MC^2, -MB^2 + 2*MC^2 + s/2, MB^2, 0, 0, MB])/
     (4*MB^2 + 4*MC^2 - s) + (4*MB^2*(4*MB^2 - 4*MC^2 - s)*
      PVC[0, 0, 0, -MB^2 + 2*MC^2 + s/2, s, MB^2, 0, MB, MB])/
     (4*MB^2 + 4*MC^2 - s) - (4*MB^2 - 4*MC^2 - s)*PVC[0, 0, 0, s/4, MC^2, 
      -MB^2 + 2*MC^2 + s/2, MB, MC, 0] + 
    (4*MB^2*(4*MB^2 - 4*MC^2 - s)*PVC[0, 0, 0, s, MB^2, -MB^2 + 2*MC^2 + s/2, 
       MB, MB, 0])/(4*MB^2 + 4*MC^2 - s))*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s))
