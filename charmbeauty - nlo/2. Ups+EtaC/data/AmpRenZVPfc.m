(* Created with the Wolfram Language : www.wolfram.com *)
{ConditionalExpression[(CA*CF*e*MB*MC^2*Rb*Rc*(1 + 4*ED*SW^2)*
     (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
          s^2/4]) - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
             4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2), 
        -4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
           s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
         s^2/4] - DiLog[(8*MB^2*MC^2 - 2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
             4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2), 
        4*MC^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
           s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
         s^2/4] - DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 
          4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4])/(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 
          4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4]), 2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
           4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
      DiLog[(-2*MC^2*(4*MB^2 + 4*MC^2 - s) + 
          4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4])/(-2*MC^2*(4*MB^2 + 4*MC^2 - s) - 
          4*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4]), -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
           4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
      DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
          ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
              2*MC^2*s + s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
            8*MB^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 - s)*
            Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
              s^2/4])/2), -4*MB^4 + 4*MC^4 - s^2/4 + 2*MB^2*(4*MC^2 + s) + 
         ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
         2*MB^2*s - 2*MC^2*s + s^2/4] - 
      DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
          ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
              2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
         ((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/4 + 
          ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
              2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 
         2*MB^2*(4*MC^2 + s) + ((-4*MB^2 + 4*MC^2 + s)*
           Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
          2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
         s^2/4] + DiLog[((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 8*MB^2*s - s^2)/
           4 + ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
              2*MC^2*s + s^2/4])/2)/((-16*MB^4 + 32*MB^2*MC^2 + 16*MC^4 + 
            8*MB^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 + s)*
            Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
              s^2/4])/2), 4*MB^4 - 4*MC^4 + s^2/4 - 2*MB^2*(4*MC^2 + s) - 
         ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
         2*MB^2*s - 2*MC^2*s + s^2/4])*Eps[li1, p, Psi1, q]*SMP["g_s"]^4)/
    (2*CW*Sqrt[MB*MC]*Pi^3*(16*MB^4 - 16*MC^4 - 8*MB^2*s + s^2)*SUNN*SW) + 
   (CA*CF*e*MB^3*Rb*Rc*(4*MB^2 - 4*MC^2 - s)*(1 + 4*ED*SW^2)*
     (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
          s^2/4]) - DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
           2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
              s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), -4*MB^2 + 4*MC^2 + s - 
         2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
      DiLog[-((-(MB^2*(4*MB^2 - 4*MC^2 - s)) - 
           2*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
              s^2/4])/(MB^2*(4*MB^2 - 4*MC^2 - s))), 4*MB^2 - 4*MC^2 - s + 
         2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
      DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
          ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
              2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
          ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
              2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
        (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s) + 2*(4*MC^2 + s)*
          Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
      DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
          ((4*MB^2 - 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
              2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
         (((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
          ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
              2*MB^2*s - 2*MC^2*s + s^2/4])/2), 2*MC^2 - s/2 + 
         Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
      DiLog[(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
          ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
              2*MC^2*s + s^2/4])/2)/(((4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s))/4 + 
          ((-4*MB^2 + 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
              2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MC^4 - s^2/4 + 
         MB^2*(-4*MC^2 + s) - ((4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
             4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
      DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
          s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
          Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
         2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
      DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
             4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
         (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MB^2 + s)*
            (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4)]), 
        4*MB^2 + 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
            2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
         4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
      DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
          s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
          Sqrt[s*(-4*MB^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
         2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
       Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
      DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
             4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
         (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MB^2 + s)*
            (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4)]), 
        -4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
            2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
         4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*Eps[li1, p, Psi1, q]*
     SMP["g_s"]^4)/(2*CW*Sqrt[MB*MC]*Pi^3*(64*MB^6 + (4*MC^2 - s)^3 - 
      16*MB^4*(4*MC^2 + 3*s) - 4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SUNN*
     SW) + ((I/8)*CA*CF*e*MB*Rb*Rc*(1 + 4*ED*SW^2)*Eps[li1, p, Psi1, q]*
     ((Pi*(-4*MB^2 + 4*MC^2 + 3*s))/(32*MB^6 - 32*MB^4*(4*MC^2 + s) - 
        (-4*MC^2 + s)^2*(4*MC^2 + s) + 2*MB^2*(80*MC^4 + 24*MC^2*s + 
          5*s^2)) + (I*(4*MB^2 - 4*MC^2 - 3*s)*
        Log[(-2*MB^2)/(4*MB^2 - 4*MC^2 - s)])/(32*MB^6 - 
        32*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
        2*MB^2*(80*MC^4 + 24*MC^2*s + 5*s^2)) + 
      ((4*I)*Sqrt[s*(-4*MB^2 + s)]*(I*Pi + 
         Log[-(2*MB^2 - s + Sqrt[s*(-4*MB^2 + s)])/(2*MB^2)]))/
       (64*MB^6 - 48*MB^4*(4*MC^2 + s) - (-4*MC^2 + s)^2*(4*MC^2 + s) + 
        4*MB^2*(48*MC^4 + 8*MC^2*s + 3*s^2)))*SMP["g_s"]^4)/
    (CW*Sqrt[MB*MC]*Pi^3*SUNN*SW), 16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 
    8*MB^2*s - 8*MC^2*s + s^2 > 0], 0}
