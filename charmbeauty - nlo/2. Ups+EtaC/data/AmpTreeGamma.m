(* Created with the Wolfram Language : www.wolfram.com *)
(e^3*ED*EU^2*Rb*Rc*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
   Momentum[p], Momentum[q]])/(Sqrt[MB*MC]*Pi*(4*MB^3 - 4*MB*MC^2 + MB*s)*
  SUNN)
