(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 ((4*I)*CA*CF*e*ED*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*(-3 + D)*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] + 
    4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] + 4*(-3 + D)*MB^2*MC^2*
     (4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    16*(-3 + D)*MB^2*MC^4*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    32*(-3 + D)*MB^2*MC^2*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    2*(-2 + D)*MC^2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[-(-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k])*SMP["g_s"]^4)/
  ((-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 
 ((-4*I)*CA*CF*e*ED*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (-8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*(-3 + D)*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] - 
    4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] + (-2 + D)*MC^2*
     (4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k] + 16*(-3 + D)*MB^2*MC^2*s*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    2*(-3 + D)*MB^2*MC^2*(4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] - 8*(-3 + D)*MB^2*MC^4*
     (4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] - 4*(-3 + D)*MB^2*MC^2*
     (-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/((-3 + D)*(-2 + D)*MB*MC^2*
   (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 0, 0, 
 ((4*I)*CA*CF*e*ED*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*(-3 + D)*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] + 
    4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] + 4*(-3 + D)*MB^2*MC^2*
     (4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    16*(-3 + D)*MB^2*MC^4*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    32*(-3 + D)*MB^2*MC^2*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    2*(-2 + D)*MC^2*(4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[-(-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] + 
         4*Pair[Momentum[k, D], Momentum[p, D]] + 
         2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k])*SMP["g_s"]^4)/
  ((-3 + D)*(-2 + D)*MB*MC^2*(4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 
 ((-4*I)*CA*CF*e*ED*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (-8*(-3 + D)*MB^2*MC^2*(-4*MB^2 + 4*(-3 + D)*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] - 
    4*(-2 + D)*MB^2*(4*MB^2 - 4*MC^2 - s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] + (-2 + D)*MC^2*
     (4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k] + 16*(-3 + D)*MB^2*MC^2*s*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    2*(-3 + D)*MB^2*MC^2*(4*(-6 + D)*MB^2 + (56 - 12*D)*MC^2 - (-10 + D)*s)*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] - 8*(-3 + D)*MB^2*MC^4*
     (4*(-6 + D)*MB^2 + (-2 + D)*(4*MC^2 - s))*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] - 4*(-3 + D)*MB^2*MC^2*
     (-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/((-3 + D)*(-2 + D)*MB*MC^2*
   (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 0, 0, 0, 0, 0, 0, 0, 
 ((16*I)*CA*CF*e*ED*MB*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] - 
    (2*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k])/((-3 + D)*MC^2) + 
    (64*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) + 
    (8*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 - 4*MC^2 - s) - 2*(4*MB^2 - 4*MC^2 - s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (4*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k])/(4*MB^2 - 4*MC^2 - s) + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
          2*Pair[Momentum[k, D], Momentum[p, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 + 4*MC^2 - s) - 
    (4*MB^2 - 4*MC^2 - s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MC^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] + (8*MB^2*(4*MB^2 - 4*MC^2 - s)*
      IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 + 4*MC^2 - s) + 
    (8*MB^2*(4*MB^2 - 4*MC^2 - s)*
      IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 + 4*MC^2 - s) + 
    (16*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
          Pair[Momentum[k, D], Momentum[q, D]])*(4*MB^2 - 4*MC^2 - s - 
          2*Pair[Momentum[k, D], Momentum[k, D]] + 
          4*Pair[Momentum[k, D], Momentum[p, D]] + 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 - 4*MC^2 - s))*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 
 ((16*I)*CA*CF*e*ED*MB*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k] - 
    (2*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k])/((-3 + D)*MC^2) + 
    (64*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) + 
    (8*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 - 4*MC^2 - s) - 2*(4*MB^2 - 4*MC^2 - s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (4*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k])/(4*MB^2 - 4*MC^2 - s) + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
          2*Pair[Momentum[k, D], Momentum[p, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 + 4*MC^2 - s) - 
    (4*MB^2 - 4*MC^2 - s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MC^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] + (8*MB^2*(4*MB^2 - 4*MC^2 - s)*
      IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 + 4*MC^2 - s) + 
    (8*MB^2*(4*MB^2 - 4*MC^2 - s)*
      IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 + 4*MC^2 - s) + 
    (16*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
          Pair[Momentum[k, D], Momentum[q, D]])*(4*MB^2 - 4*MC^2 - s - 
          2*Pair[Momentum[k, D], Momentum[k, D]] + 
          4*Pair[Momentum[k, D], Momentum[p, D]] + 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 - 4*MC^2 - s))*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
