(* Created with the Wolfram Language : www.wolfram.com *)
{MetricTensor[li2, li3]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*
  $LineB[(-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*e*EU*DiracMatrix[li1]) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p/2 - q, MC]*
  PropagatorDenominator[q, 0]*$LineB[(-I)*Gstrong*DiracMatrix[li3]*
    SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . ((-I)*e*EU*DiracMatrix[li1])], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p + q/2, MB]*
  $LineB[((-I)*e*ED*DiracMatrix[li1]) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[p + q/2]) . ((-I)*e*ED*DiracMatrix[li1])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]}
