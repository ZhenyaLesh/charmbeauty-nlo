(* Created with the Wolfram Language : www.wolfram.com *)
{MetricTensor[li2, li3]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*$LineB[(-I)*e*ED*DiracMatrix[li3]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*e*EU*DiracMatrix[li2])], MetricTensor[li2, li3]*
  PropagatorDenominator[q, MZ]*PropagatorDenominator[p/2 + q, MC]*
  $LineB[((-I/4)*e*(DiracMatrix[li3] + 4*ED*SW^2*DiracMatrix[li3] - 
      DiracMatrix[li3] . GA[5]))/(CW*SW)]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li2] + DiracMatrix[li2] . 
        GA[5]))/(CW*SW))], MetricTensor[li2, li3]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[q, 0]*
  $LineB[(-I)*e*ED*DiracMatrix[li3]]*
  $LineC[((-I)*e*EU*DiracMatrix[li2]) . (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], MetricTensor[li2, li3]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[q, MZ]*
  $LineB[((-I/4)*e*(DiracMatrix[li3] + 4*ED*SW^2*DiracMatrix[li3] - 
      DiracMatrix[li3] . GA[5]))/(CW*SW)]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li2] + 
       DiracMatrix[li2] . GA[5]))/(CW*SW)) . (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], MetricTensor[li2, li3]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*e*ED*DiracMatrix[li3])]*$LineC[(-I)*e*EU*DiracMatrix[li2]], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, MZ]*
  PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    (((-I/4)*e*(DiracMatrix[li3] + 4*ED*SW^2*DiracMatrix[li3] - 
       DiracMatrix[li3] . GA[5]))/(CW*SW))]*
  $LineC[((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li2] + 
      DiracMatrix[li2] . GA[5]))/(CW*SW)], MetricTensor[li2, li3]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*e*ED*DiracMatrix[li3]) . (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*e*EU*DiracMatrix[li2]], MetricTensor[li2, li3]*
  PropagatorDenominator[-p, MZ]*PropagatorDenominator[-p - q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li3] + 4*ED*SW^2*DiracMatrix[li3] - 
       DiracMatrix[li3] . GA[5]))/(CW*SW)) . (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li2] + 
      DiracMatrix[li2] . GA[5]))/(CW*SW)]}
