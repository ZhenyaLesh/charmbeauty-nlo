(* Created with the Wolfram Language : www.wolfram.com *)
{I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p/2, MC], PropagatorDenominator[k + p/2 + q, 
    MC]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-q, 0]*PropagatorDenominator[p/2 + q, MC]*
  $LineB[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k - p/2 - q]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MC + DiracSlash[-k - p/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k + p/2, 0], PropagatorDenominator[k + p/2 + q, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[-k - p/2 + q, li7]*MetricTensor[li3, li5] + 
   FourVector[-k - p/2 - 2*q, li5]*MetricTensor[li3, li7] + 
   FourVector[2*k + p + q, li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[p/2 + q, MC]*SUNF[Index[Gluon, 6], Index[Gluon, 7], 
   Index[Gluon, 8]]*$LineB[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[k]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k - q/2, MB], PropagatorDenominator[k + q/2, MB]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k + q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[k - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - q/2, 0], PropagatorDenominator[k + q/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[-k - (3*q)/2, li7]*MetricTensor[li3, li5] + 
   FourVector[-k + (3*q)/2, li5]*MetricTensor[li3, li7] + 
   FourVector[2*k, li3]*MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNF[Index[Gluon, 6], Index[Gluon, 7], Index[Gluon, 8]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li6]*
     SUNT[Index[Gluon, 8]])]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p/2, MC], PropagatorDenominator[k - q, 0], 
   PropagatorDenominator[k - q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[p/2 + q, MC]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[-k + q/2]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k - p/2]) . ((-I)*Gstrong*DiracMatrix[li2]*
     SUNT[Index[Gluon, 6]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p/2, MC], PropagatorDenominator[k - q, 0], 
   PropagatorDenominator[k - q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[p/2 + q, MC]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k - q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]])]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k - p/2]) . ((-I)*Gstrong*DiracMatrix[li2]*
     SUNT[Index[Gluon, 6]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p/2, MC], PropagatorDenominator[k + p/2 + q, 
    MC]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  $LineB[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[k + p/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MC + DiracSlash[k + p/2 + q]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*Gstrong*FeynAmpDenominator[
   PropagatorDenominator[k, MC], PropagatorDenominator[k + p/2, 0], 
   PropagatorDenominator[k + p/2 + q, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*(FourVector[-k - p/2 + q, li7]*
    MetricTensor[li3, li5] + FourVector[-k - p/2 - 2*q, li5]*
    MetricTensor[li3, li7] + FourVector[2*k + p + q, li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  SUNF[Index[Gluon, 6], Index[Gluon, 7], Index[Gluon, 8]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li6]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k - q/2, MB], PropagatorDenominator[k + q/2, MB]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[q, 0]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k + q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[k - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*Gstrong*FeynAmpDenominator[
   PropagatorDenominator[k, MB], PropagatorDenominator[k - q/2, 0], 
   PropagatorDenominator[k + q/2, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*(FourVector[-k - (3*q)/2, li7]*
    MetricTensor[li3, li5] + FourVector[-k + (3*q)/2, li5]*
    MetricTensor[li3, li7] + FourVector[2*k, li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PropagatorDenominator[-p/2 - q, MC]*
  PropagatorDenominator[q, 0]*SUNF[Index[Gluon, 6], Index[Gluon, 7], 
   Index[Gluon, 8]]*$LineB[((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-k]) . 
    ((-I)*Gstrong*DiracMatrix[li6]*SUNT[Index[Gluon, 8]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p/2, MC], PropagatorDenominator[k - q, 0], 
   PropagatorDenominator[k - q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p/2 - q, MC]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[-k + q/2]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[k + p/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p/2, MC], PropagatorDenominator[k - q, 0], 
   PropagatorDenominator[k - q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p/2 - q, MC]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k - q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[k + p/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + q/2, MB], PropagatorDenominator[k + p + q/2, 
    MB]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-k - p - q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[-k - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k + q/2, 0], PropagatorDenominator[k + p + q/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[-k + p - q/2, li7]*MetricTensor[li3, li5] + 
   FourVector[-k - 2*p - q/2, li5]*MetricTensor[li3, li7] + 
   FourVector[p + 2*(k + q/2), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p + q/2, MB]*SUNF[Index[Gluon, 6], Index[Gluon, 7], 
   Index[Gluon, 8]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[k]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + q/2, MB], PropagatorDenominator[k + p + q/2, 
    MB]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k + q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[k + p + q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k + q/2, 0], PropagatorDenominator[k + p + q/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[-k + p - q/2, li7]*MetricTensor[li3, li5] + 
   FourVector[-k - 2*p - q/2, li5]*MetricTensor[li3, li7] + 
   FourVector[p + 2*(k + q/2), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-p - q/2, MB]*SUNF[Index[Gluon, 6], Index[Gluon, 7], 
   Index[Gluon, 8]]*$LineB[((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-k]) . 
    ((-I)*Gstrong*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MU + DiracSlash[k - p - q], I*Gstrong*DiracMatrix[li5], 
   MU + DiracSlash[k - p], I*Gstrong*DiracMatrix[li3], MU + DiracSlash[k], 
   ((I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/
    (CW*SW)]*FeynAmpDenominator[PropagatorDenominator[k, MU], 
   PropagatorDenominator[k - p, MU], PropagatorDenominator[k - p - q, MU]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MC + DiracSlash[k - p - q], 
   I*Gstrong*DiracMatrix[li5], MC + DiracSlash[k - p], 
   I*Gstrong*DiracMatrix[li3], MC + DiracSlash[k], 
   ((I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/
    (CW*SW)]*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p, MC], PropagatorDenominator[k - p - q, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MT + DiracSlash[k - p - q], 
   I*Gstrong*DiracMatrix[li5], MT + DiracSlash[k - p], 
   I*Gstrong*DiracMatrix[li3], MT + DiracSlash[k], 
   ((I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/
    (CW*SW)]*FeynAmpDenominator[PropagatorDenominator[k, MT], 
   PropagatorDenominator[k - p, MT], PropagatorDenominator[k - p - q, MT]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MU + DiracSlash[k - p - q], 
   (-I)*Gstrong*DiracMatrix[li5], MU + DiracSlash[k - p], 
   (-I)*Gstrong*DiracMatrix[li3], MU + DiracSlash[k], 
   ((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . GA[5]))/
    (CW*SW)]*FeynAmpDenominator[PropagatorDenominator[k, MU], 
   PropagatorDenominator[k - p, MU], PropagatorDenominator[k - p - q, MU]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MC + DiracSlash[k - p - q], 
   (-I)*Gstrong*DiracMatrix[li5], MC + DiracSlash[k - p], 
   (-I)*Gstrong*DiracMatrix[li3], MC + DiracSlash[k], 
   ((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . GA[5]))/
    (CW*SW)]*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p, MC], PropagatorDenominator[k - p - q, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MT + DiracSlash[k - p - q], 
   (-I)*Gstrong*DiracMatrix[li5], MT + DiracSlash[k - p], 
   (-I)*Gstrong*DiracMatrix[li3], MT + DiracSlash[k], 
   ((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . GA[5]))/
    (CW*SW)]*FeynAmpDenominator[PropagatorDenominator[k, MT], 
   PropagatorDenominator[k - p, MT], PropagatorDenominator[k - p - q, MT]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MD + DiracSlash[k - p - q], 
   I*Gstrong*DiracMatrix[li5], MD + DiracSlash[k - p], 
   I*Gstrong*DiracMatrix[li3], MD + DiracSlash[k], 
   ((I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] + 
      DiracMatrix[li1] . GA[5]))/(CW*SW)]*FeynAmpDenominator[
   PropagatorDenominator[k, MD], PropagatorDenominator[k - p, MD], 
   PropagatorDenominator[k - p - q, MD]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MS + DiracSlash[k - p - q], 
   I*Gstrong*DiracMatrix[li5], MS + DiracSlash[k - p], 
   I*Gstrong*DiracMatrix[li3], MS + DiracSlash[k], 
   ((I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] + 
      DiracMatrix[li1] . GA[5]))/(CW*SW)]*FeynAmpDenominator[
   PropagatorDenominator[k, MS], PropagatorDenominator[k - p, MS], 
   PropagatorDenominator[k - p - q, MS]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MB + DiracSlash[k - p - q], 
   I*Gstrong*DiracMatrix[li5], MB + DiracSlash[k - p], 
   I*Gstrong*DiracMatrix[li3], MB + DiracSlash[k], 
   ((I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] + 
      DiracMatrix[li1] . GA[5]))/(CW*SW)]*FeynAmpDenominator[
   PropagatorDenominator[k, MB], PropagatorDenominator[k - p, MB], 
   PropagatorDenominator[k - p - q, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MD + DiracSlash[k - p - q], 
   (-I)*Gstrong*DiracMatrix[li5], MD + DiracSlash[k - p], 
   (-I)*Gstrong*DiracMatrix[li3], MD + DiracSlash[k], 
   ((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
      DiracMatrix[li1] . GA[5]))/(CW*SW)]*FeynAmpDenominator[
   PropagatorDenominator[k, MD], PropagatorDenominator[k - p, MD], 
   PropagatorDenominator[k - p - q, MD]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MS + DiracSlash[k - p - q], 
   (-I)*Gstrong*DiracMatrix[li5], MS + DiracSlash[k - p], 
   (-I)*Gstrong*DiracMatrix[li3], MS + DiracSlash[k], 
   ((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
      DiracMatrix[li1] . GA[5]))/(CW*SW)]*FeynAmpDenominator[
   PropagatorDenominator[k, MS], PropagatorDenominator[k - p, MS], 
   PropagatorDenominator[k - p - q, MS]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], (-I)*DiracTrace[MB + DiracSlash[k - p - q], 
   (-I)*Gstrong*DiracMatrix[li5], MB + DiracSlash[k - p], 
   (-I)*Gstrong*DiracMatrix[li3], MB + DiracSlash[k], 
   ((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
      DiracMatrix[li1] . GA[5]))/(CW*SW)]*FeynAmpDenominator[
   PropagatorDenominator[k, MB], PropagatorDenominator[k - p, MB], 
   PropagatorDenominator[k - p - q, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*$LineC[(-I)*Gstrong*DiracMatrix[li2]*
    SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q, MB], PropagatorDenominator[k - q/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[p + q/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-k + p + q]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-k]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q, MB], PropagatorDenominator[k - q/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p + q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k]) . (((-I/4)*e*(DiracMatrix[li1] + 
       4*ED*SW^2*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/(CW*SW)) . 
    (MB + DiracSlash[k - p - q]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q, MB], PropagatorDenominator[k - q/2, 0], 
   PropagatorDenominator[k - p - q/2, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*(FourVector[k + p - q/2, li7]*
    MetricTensor[li3, li5] + FourVector[k - 2*p - q/2, li5]*
    MetricTensor[li3, li7] + FourVector[-2*k + p + q, li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PropagatorDenominator[-p, 0]*SUNF[Index[Gluon, 6], Index[Gluon, 7], 
   Index[Gluon, 8]]*$LineB[((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[k]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[k - p - q]) . 
    ((-I)*Gstrong*DiracMatrix[li6]*SUNT[Index[Gluon, 8]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - q, MB], PropagatorDenominator[k - p - q, MB], 
   PropagatorDenominator[k - q/2, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k]) . (((-I/4)*e*(DiracMatrix[li1] + 
       4*ED*SW^2*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/(CW*SW)) . 
    (MB + DiracSlash[k - p - q]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[k - q]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - q, MB], PropagatorDenominator[k - p - q, MB], 
   PropagatorDenominator[k - q/2, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-k + q]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[-k + p + q]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-k]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k - p/2, MC], PropagatorDenominator[k + p/2, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[p, 0]*
  PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[k + p/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MC + DiracSlash[k - p/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k + p/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[-k - (3*p)/2, li7]*MetricTensor[li3, li5] + 
   FourVector[-k + (3*p)/2, li5]*MetricTensor[li3, li7] + 
   FourVector[2*k, li3]*MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNF[Index[Gluon, 6], Index[Gluon, 7], Index[Gluon, 8]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li6]*
     SUNT[Index[Gluon, 8]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k + p/2, 0], 
   PropagatorDenominator[k + p/2 + q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-k - p/2 - q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k + p/2, 0], 
   PropagatorDenominator[k - p/2 - q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[k - p/2 - q/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k - p/2, MC], PropagatorDenominator[k + p/2, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[p, 0]*
  PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[k + p/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MC + DiracSlash[k - p/2]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])], 
 (-I)*Gstrong*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k + p/2, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[-k - (3*p)/2, li7]*MetricTensor[li3, li5] + 
   FourVector[-k + (3*p)/2, li5]*MetricTensor[li3, li7] + 
   FourVector[2*k, li3]*MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNF[Index[Gluon, 6], Index[Gluon, 7], Index[Gluon, 8]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li6]*
     SUNT[Index[Gluon, 8]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k + p/2, 0], 
   PropagatorDenominator[k + p/2 + q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[k + p/2 + q/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k + p/2, 0], 
   PropagatorDenominator[k - p/2 - q/2, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-k + p/2 + q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k - p - q, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[q, 0]*
  $LineB[(-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-k + p + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW)) . (MC + DiracSlash[-k]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k - p - q, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*
  $LineB[(-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[k]) . (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[k - p - q]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-p/2 - q]) . ((-I)*Gstrong*DiracMatrix[li2]*
     SUNT[Index[Gluon, 6]])], (-I)*Gstrong*FeynAmpDenominator[
   PropagatorDenominator[k, MC], PropagatorDenominator[k - p/2, 0], 
   PropagatorDenominator[k - p - q, MC], PropagatorDenominator[k - p/2 - q, 
    0]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  (FourVector[k - p/2 + q, li7]*MetricTensor[li3, li5] + 
   FourVector[k - p/2 - 2*q, li5]*MetricTensor[li3, li7] + 
   FourVector[-2*k + p + q, li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PropagatorDenominator[-q, 0]*
  SUNF[Index[Gluon, 6], Index[Gluon, 7], Index[Gluon, 8]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[k]) . (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[k - p - q]) . 
    ((-I)*Gstrong*DiracMatrix[li6]*SUNT[Index[Gluon, 8]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p, MC], PropagatorDenominator[k - p/2, 0], 
   PropagatorDenominator[k - p - q, MC]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  $LineB[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[k]) . (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[k - p - q]) . 
    ((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[k - p]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p, MC], PropagatorDenominator[k - p/2, 0], 
   PropagatorDenominator[k - p - q, MC]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  $LineB[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k + p]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]]) . (MC + DiracSlash[-k + p + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW)) . (MC + DiracSlash[-k]) . 
    ((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k - p - q, MC], 
   PropagatorDenominator[k - p/2 - q, 0], PropagatorDenominator[
    k - p/2 - q/2, MB]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[-k + p/2 + q/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[k]) . (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[k - p - q]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2, 0], PropagatorDenominator[k - p - q, MC], 
   PropagatorDenominator[k - p/2 - q, 0], PropagatorDenominator[
    k - p/2 - q/2, MB]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  $LineB[((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k - p/2 - q/2]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[k]) . (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[k - p - q]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q, MB], PropagatorDenominator[k - q/2, 0], 
   PropagatorDenominator[k - p - q/2, 0], PropagatorDenominator[
    k - p/2 - q/2, MC]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[k]) . (((-I/4)*e*(DiracMatrix[li1] + 
       4*ED*SW^2*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/(CW*SW)) . 
    (MB + DiracSlash[k - p - q]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[-k + p/2 + q/2]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q, MB], PropagatorDenominator[k - q/2, 0], 
   PropagatorDenominator[k - p - q/2, 0], PropagatorDenominator[
    k - p/2 - q/2, MC]]*MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  $LineB[((-I)*Gstrong*DiracMatrix[li5]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[k]) . (((-I/4)*e*(DiracMatrix[li1] + 
       4*ED*SW^2*DiracMatrix[li1] - DiracMatrix[li1] . GA[5]))/(CW*SW)) . 
    (MB + DiracSlash[k - p - q]) . ((-I)*Gstrong*DiracMatrix[li3]*
     SUNT[Index[Gluon, 6]])]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[k - p/2 - q/2]) . ((-I)*Gstrong*DiracMatrix[li4]*
     SUNT[Index[Gluon, 7]])], (-I)*DiracTrace[MU + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MU + DiracSlash[-k + q], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MU], PropagatorDenominator[k - q, MU]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*DiracTrace[MC + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MC + DiracSlash[-k + q], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - q, MC]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*DiracTrace[MT + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MT + DiracSlash[-k + q], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MT], 
   PropagatorDenominator[k - q, MT]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*DiracTrace[MD + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MD + DiracSlash[-k + q], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MD], 
   PropagatorDenominator[k - q, MD]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*DiracTrace[MS + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MS + DiracSlash[-k + q], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MS], 
   PropagatorDenominator[k - q, MS]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*DiracTrace[MB + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MB + DiracSlash[-k + q], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - q, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*Gstrong^2*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k - q, 0]]*FourVector[k, li5]*FourVector[k - q, li3]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*PropagatorDenominator[p/2 + q, MC]*
  SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 7]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 6]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (I/2)*Gstrong^2*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k - q, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[-k - q, li8]*MetricTensor[li3, li6] + 
   FourVector[-k + 2*q, li6]*MetricTensor[li3, li8] + 
   FourVector[2*k - q, li3]*MetricTensor[li6, li8])*
  (FourVector[k + q, li9]*MetricTensor[li5, li7] + 
   FourVector[k - 2*q, li7]*MetricTensor[li5, li9] + 
   FourVector[-2*k + q, li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PropagatorDenominator[-q, 0]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*SUNF[Index[Gluon, 6], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2 - q, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC, 2]*
  $LineB[(-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 (-I)*DiracTrace[MU + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MU + DiracSlash[-k + q], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MU], 
   PropagatorDenominator[k - q, MU]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p/2 - q, MC]*
  PropagatorDenominator[-q, 0]*PropagatorDenominator[q, 0]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*DiracTrace[MC + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MC + DiracSlash[-k + q], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MC], PropagatorDenominator[k - q, MC]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*DiracTrace[MT + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MT + DiracSlash[-k + q], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MT], PropagatorDenominator[k - q, MT]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*DiracTrace[MD + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MD + DiracSlash[-k + q], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MD], PropagatorDenominator[k - q, MD]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*DiracTrace[MS + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MS + DiracSlash[-k + q], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MS], PropagatorDenominator[k - q, MS]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*DiracTrace[MB + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MB + DiracSlash[-k + q], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MB], PropagatorDenominator[k - q, MB]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*SUNTrace[SUNT[Index[Gluon, 7]] . 
    SUNT[Index[Gluon, 6]]]*$LineB[(-I)*Gstrong*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*Gstrong^2*FeynAmpDenominator[
   PropagatorDenominator[k, 0], PropagatorDenominator[k - q, 0]]*
  FourVector[k, li5]*FourVector[k - q, li3]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p/2 - q, MC]*
  PropagatorDenominator[-q, 0]*PropagatorDenominator[q, 0]*
  SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 7]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 6]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (I/2)*Gstrong^2*FeynAmpDenominator[
   PropagatorDenominator[k, 0], PropagatorDenominator[k - q, 0]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[-k - q, li8]*MetricTensor[li3, li6] + 
   FourVector[-k + 2*q, li6]*MetricTensor[li3, li8] + 
   FourVector[2*k - q, li3]*MetricTensor[li6, li8])*
  (FourVector[k + q, li9]*MetricTensor[li5, li7] + 
   FourVector[k - 2*q, li7]*MetricTensor[li5, li9] + 
   FourVector[-2*k + q, li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[-q, 0]*
  PropagatorDenominator[q, 0]*SUNF[Index[Gluon, 6], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]]*
  $LineB[(-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], I*FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k - p/2 - q, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[-p/2 - q, MC, 2]*
  $LineB[(-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[k]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], (-I)*DiracTrace[MU + DiracSlash[-k], 
   (-I)*Gstrong*DiracMatrix[li5], MU + DiracSlash[-k - p], 
   (-I)*Gstrong*DiracMatrix[li3]]*FeynAmpDenominator[
   PropagatorDenominator[k, MU], PropagatorDenominator[k + p, MU]]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MC + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MC + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k + p, MC]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MT + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MT + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MT], 
   PropagatorDenominator[k + p, MT]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MD + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MD + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MD], 
   PropagatorDenominator[k + p, MD]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MS + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MS + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MS], 
   PropagatorDenominator[k + p, MS]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MB + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MB + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k + p, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*Gstrong^2*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p, 0]]*FourVector[k, li5]*FourVector[k + p, li3]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[p + q/2, MB]*
  SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 7]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 6]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (I/2)*Gstrong^2*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[-k + p, li8]*MetricTensor[li3, li6] + 
   FourVector[-k - 2*p, li6]*MetricTensor[li3, li8] + 
   FourVector[2*k + p, li3]*MetricTensor[li6, li8])*
  (FourVector[k - p, li9]*MetricTensor[li5, li7] + 
   FourVector[k + 2*p, li7]*MetricTensor[li5, li9] + 
   FourVector[-2*k - p, li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[p, 0]*
  PropagatorDenominator[p + q/2, MB]*SUNF[Index[Gluon, 6], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q/2, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p + q/2, MB, 2]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-k]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MU + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MU + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MU], 
   PropagatorDenominator[k + p, MU]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MC + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MC + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MC], 
   PropagatorDenominator[k + p, MC]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MT + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MT + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MT], 
   PropagatorDenominator[k + p, MT]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MD + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MD + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MD], 
   PropagatorDenominator[k + p, MD]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MS + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MS + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MS], 
   PropagatorDenominator[k + p, MS]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*DiracTrace[MB + DiracSlash[-k], (-I)*Gstrong*DiracMatrix[li5], 
   MB + DiracSlash[-k - p], (-I)*Gstrong*DiracMatrix[li3]]*
  FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k + p, MB]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNTrace[SUNT[Index[Gluon, 7]] . SUNT[Index[Gluon, 6]]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (-I)*Gstrong^2*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p, 0]]*FourVector[k, li5]*FourVector[k + p, li3]*
  MetricTensor[li2, li3]*MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p, 0]*PropagatorDenominator[-p - q/2, MB]*
  SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 7]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 6]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 (I/2)*Gstrong^2*FeynAmpDenominator[PropagatorDenominator[k, 0], 
   PropagatorDenominator[k + p, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[-k + p, li8]*MetricTensor[li3, li6] + 
   FourVector[-k - 2*p, li6]*MetricTensor[li3, li8] + 
   FourVector[2*k + p, li3]*MetricTensor[li6, li8])*
  (FourVector[k - p, li9]*MetricTensor[li5, li7] + 
   FourVector[k + 2*p, li7]*MetricTensor[li5, li9] + 
   FourVector[-2*k - p, li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[p, 0]*
  PropagatorDenominator[-p - q/2, MB]*SUNF[Index[Gluon, 6], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]]*
  $LineB[((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 I*FeynAmpDenominator[PropagatorDenominator[k, MB], 
   PropagatorDenominator[k - p - q/2, 0]]*MetricTensor[li2, li3]*
  MetricTensor[li4, li5]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-p - q/2, MB, 2]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[p + q/2]) . ((-I)*Gstrong*DiracMatrix[li5]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[k]) . 
    ((-I)*Gstrong*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]}
