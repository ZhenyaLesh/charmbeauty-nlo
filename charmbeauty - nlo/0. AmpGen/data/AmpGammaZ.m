(* Created with the Wolfram Language : www.wolfram.com *)
{MetricTensor[li2, li3]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*$LineB[(-I)*e*ED*DiracMatrix[li3]]*
  $LineC[((-I)*e*EU*DiracMatrix[li1]) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*e*EU*DiracMatrix[li2])], MetricTensor[li2, li3]*
  PropagatorDenominator[q, MZ]*PropagatorDenominator[p/2 + q, MC]*
  $LineB[((-I/2)*e*ED*SW*DiracMatrix[li3])/CW + 
    ((I/2)*e*(-1/2 - ED*SW^2)*DiracMatrix[li3])/(CW*SW)]*
  $LineC[((-I)*e*EU*DiracMatrix[li1]) . (MC + DiracSlash[-p/2 - q]) . 
    (((-I/2)*e*EU*SW*DiracMatrix[li2])/CW + 
     ((I/2)*e*(1/2 - EU*SW^2)*DiracMatrix[li2])/(CW*SW))], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p/2 - q, MC]*
  PropagatorDenominator[q, 0]*$LineB[(-I)*e*ED*DiracMatrix[li3]]*
  $LineC[((-I)*e*EU*DiracMatrix[li2]) . (MC + DiracSlash[p/2 + q]) . 
    ((-I)*e*EU*DiracMatrix[li1])], MetricTensor[li2, li3]*
  PropagatorDenominator[-p/2 - q, MC]*PropagatorDenominator[q, MZ]*
  $LineB[((-I/2)*e*ED*SW*DiracMatrix[li3])/CW + 
    ((I/2)*e*(-1/2 - ED*SW^2)*DiracMatrix[li3])/(CW*SW)]*
  $LineC[(((-I/2)*e*EU*SW*DiracMatrix[li2])/CW + 
     ((I/2)*e*(1/2 - EU*SW^2)*DiracMatrix[li2])/(CW*SW)) . 
    (MC + DiracSlash[p/2 + q]) . ((-I)*e*EU*DiracMatrix[li1])], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[p + q/2, MB]*
  $LineB[((-I)*e*ED*DiracMatrix[li1]) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*e*ED*DiracMatrix[li3])]*$LineC[(-I)*e*EU*DiracMatrix[li2]], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, MZ]*
  PropagatorDenominator[p + q/2, MB]*
  $LineB[((-I)*e*ED*DiracMatrix[li1]) . (MB + DiracSlash[-p - q/2]) . 
    (((-I/2)*e*ED*SW*DiracMatrix[li3])/CW + 
     ((I/2)*e*(-1/2 - ED*SW^2)*DiracMatrix[li3])/(CW*SW))]*
  $LineC[((-I/2)*e*EU*SW*DiracMatrix[li2])/CW + 
    ((I/2)*e*(1/2 - EU*SW^2)*DiracMatrix[li2])/(CW*SW)], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*e*ED*DiracMatrix[li3]) . (MB + DiracSlash[p + q/2]) . 
    ((-I)*e*ED*DiracMatrix[li1])]*$LineC[(-I)*e*EU*DiracMatrix[li2]], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, MZ]*
  PropagatorDenominator[-p - q/2, MB]*
  $LineB[(((-I/2)*e*ED*SW*DiracMatrix[li3])/CW + 
     ((I/2)*e*(-1/2 - ED*SW^2)*DiracMatrix[li3])/(CW*SW)) . 
    (MB + DiracSlash[p + q/2]) . ((-I)*e*ED*DiracMatrix[li1])]*
  $LineC[((-I/2)*e*EU*SW*DiracMatrix[li2])/CW + 
    ((I/2)*e*(1/2 - EU*SW^2)*DiracMatrix[li2])/(CW*SW)]}
