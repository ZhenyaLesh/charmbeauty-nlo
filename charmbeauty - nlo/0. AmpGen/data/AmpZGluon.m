(* Created with the Wolfram Language : www.wolfram.com *)
{MetricTensor[li2, li3]*PropagatorDenominator[q, 0]*
  PropagatorDenominator[p/2 + q, MC]*
  $LineB[(-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]]*
  $LineC[(((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MC + DiracSlash[-p/2 - q]) . 
    ((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]])], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p/2 - q, MC]*
  PropagatorDenominator[q, 0]*$LineB[(-I)*Gstrong*DiracMatrix[li3]*
    SUNT[Index[Gluon, 6]]]*
  $LineC[((-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]) . 
    (MC + DiracSlash[p/2 + q]) . 
    (((-I/4)*e*((-1 + 4*EU*SW^2)*DiracMatrix[li1] + DiracMatrix[li1] . 
        GA[5]))/(CW*SW))], MetricTensor[li2, li3]*
  PropagatorDenominator[-p, 0]*PropagatorDenominator[p + q/2, MB]*
  $LineB[(((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW)) . (MB + DiracSlash[-p - q/2]) . 
    ((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]])]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]], 
 MetricTensor[li2, li3]*PropagatorDenominator[-p, 0]*
  PropagatorDenominator[-p - q/2, MB]*
  $LineB[((-I)*Gstrong*DiracMatrix[li3]*SUNT[Index[Gluon, 6]]) . 
    (MB + DiracSlash[p + q/2]) . 
    (((-I/4)*e*(DiracMatrix[li1] + 4*ED*SW^2*DiracMatrix[li1] - 
       DiracMatrix[li1] . GA[5]))/(CW*SW))]*
  $LineC[(-I)*Gstrong*DiracMatrix[li2]*SUNT[Index[Gluon, 6]]]}
