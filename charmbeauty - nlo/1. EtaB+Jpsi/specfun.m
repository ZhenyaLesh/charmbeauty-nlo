
As[\[Mu]_] := Block[{\[Beta]0 = 11/3 CA - 4/3 Tf Nf, \[Beta]1 = 34/3 CA^2 - 4 CF Tf Nf - 20/3 CA Tf Nf,
 L = Log[\[Mu]^2/\[CapitalLambda]QCD^2], Nf = 5, Tf = 1/2, CA = 3, CF = 4/3}, 4 \[Pi] (1/(\[Beta]0 L) - (\[Beta]1 Log[L])/(\[Beta]0^3 L^2))];

A0eps[m_] := (m^2*(6 + Pi^2 + 6*Log[\[Mu]^2/m^2] + 3*Log[\[Mu]^2/m^2]^2))/6;

B0eps[s_, m1_, m2_] := ((2*Pi^2*s)/3 + (m1^2 - m2^2 + s)*(Log[\[Mu]^2/m1^2]^2
			+ 2*Log[\[Mu]^2/m1^2]*Derivative[0, 1, 0, 0][Hypergeometric2F1][1, 0, 3/2, (0. + 1.*^-9*I) + (m1^2 - m2^2 + s)^2/(4*m1^2*s)] + 
        Derivative[0, 2, 0, 0][Hypergeometric2F1][1, 0, 3/2, (0. + 1.*^-9*I) + (m1^2 - m2^2 + s)^2/(4*m1^2*s)]) - (m1^2 - m2^2 - s)*(Log[\[Mu]^2/m2^2]^2 + 
        2*Log[\[Mu]^2/m2^2]*Derivative[0, 1, 0, 0][Hypergeometric2F1][1, 0, 3/2, (0. + 1.*^-9*I) + (-m1^2 + m2^2 + s)^2/(4*m2^2*s)] +
	Derivative[0, 2, 0, 0][Hypergeometric2F1][1, 0, 3/2, (0. + 1.*^-9*I) + (-m1^2 + m2^2 + s)^2/(4*m2^2*s)]))/(4*s);

NDiLog[x_, eps_] := Module[{val},
   If[eps < 0, val = PolyLog[2, x], 
    val = -PolyLog[2, x/(x - 1)] - 0.5*Log[1/(1 - x)]^2];
   If[eps == 0, Clear[val]; 
    Print[Text[Style["DILOG IS UNDEFINED", FontColor -> Red]]]];
   Return[val] ;
   ];
DiLog[x_, eps_] /; NumberQ[x] && NumberQ[eps] := NDiLog[x, eps];

FactorOut[0, __] = 0;
FactorOut[0.+0.*I, __] = 0;
FactorOut[1] = 1;
FactorOut[Longest[c_] exp_, p_] /; FreeQ[c, p] := c FactorOut[exp];
