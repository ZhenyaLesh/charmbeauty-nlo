(* Created with the Wolfram Language : www.wolfram.com *)
{(-2*CA*CF*e*EU*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*
    (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
         s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 
          2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), 4*MB^2 - 4*MC^2 + s - 
        2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
     DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 
          2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
             s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*MC^2 - s + 
        2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
     DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
         ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4])/2)/(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
         ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
             2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
       -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*
         Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
     DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
         ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
             2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
        (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
         ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
             2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
        2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
     DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
         ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4])/2)/(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
         ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
             2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
       (4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*
         Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
     DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
         s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
        (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MC^2 + s)*
           (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4)]), 
       4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
           2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
        2*MB^2*s - 2*MC^2*s + s^2/4] + 
     DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
         s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
        (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MC^2 + s)*
           (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4)]), 
       4*MB^2 + 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
           2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
        2*MB^2*s - 2*MC^2*s + s^2/4] - 
     DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
         s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
        (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MC^2 + s)*
           (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4)]), 
       -4*MB^2 - 4*MC^2 + s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
           2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
        2*MB^2*s - 2*MC^2*s + s^2/4] + 
     DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
         s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
        (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MC^2 + s)*
           (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4)]), 
       -4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
           2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
        2*MB^2*s - 2*MC^2*s + s^2/4])*Eps[li1, p, Psi1, q]*SMP["g_s"]^4)/
   (Sqrt[MB*MC]*Pi^3*(64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
     4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SUNN) + 
  ((I/2)*CA*CF*e*EU*MC*Rb*Rc*Eps[li1, p, Psi1, q]*
    ((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 
       16*MB^4*(10*MC^2 + s) + 4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - 
     (I*(4*MB^2 - 4*MC^2 + 3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/
      (64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
       4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - 
     ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(I*Pi + 
        Log[-(2*MC^2 - s + Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/
      (-64*MB^6 + (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 
       4*MB^2*(-48*MC^4 + 8*MC^2*s + s^2)))*SMP["g_s"]^4)/
   (Sqrt[MB*MC]*Pi^3*SUNN) + (2*CA*CF*e*EU*MB^2*MC*Rb*Rc*Eps[li1, p, Psi1, q]*
    (-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
         s^2/4]) - DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 
         4*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
            s^2/4])/(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 
         4*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
            s^2/4]), 2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
          4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
     DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 
         4*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
            s^2/4])/(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 
         4*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
            s^2/4]), -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
          4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
     DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
         ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4])/2)/((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 
           8*MC^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 - s)*
           Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
          2), 4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 - 
        ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
            2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
        2*MB^2*s - 2*MC^2*s + s^2/4] - 
     DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
         ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
             2*MC^2*s + s^2/4])/2)/((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 
           8*MC^2*s - s^2)/4 + ((4*MB^2 - 4*MC^2 + s)*
           Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
          2), -4*MB^4 - 8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + 
        ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
            2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
        2*MB^2*s - 2*MC^2*s + s^2/4] + 
     DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
         ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
             2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
        ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
         ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
             2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
        (-4*MC^2 + s)^2/4 + ((4*MB^2 - 4*MC^2 + s)*
          Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
         2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
        s^2/4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
            2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
      (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
     PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
           2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
          2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
        2*MC^2*s + s^2/4] + 
     PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
            2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2)]/
      Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])*
    SMP["g_s"]^4)/(Sqrt[MB*MC]*Pi^3*(16*MB^4 - (-4*MC^2 + s)^2)*SUNN), 0, 0}
