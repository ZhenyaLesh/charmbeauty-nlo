(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, ((I/2)*CA*CF*e*(4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] - 
      I*(-2 + D)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k]*
     ((2 - 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + 8*(-3 + D)*MB^2*MC^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-3 + D)*MB^2 - 4*MC^2 + s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + 8*(-3 + D)*MB^4*MC^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    (-2 + D)*MB^2*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
     (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    4*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(-8*s*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 4*s*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    2*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (-2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        2*(4*MB^2 - 4*MC^2 + 3*s)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])))*SMP["g_s"]^4)/
  (CW*(2 - D)*(3 - D)*MB^2*MC*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW), 
 ((I/2)*CA*CF*e*(4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] - 
      I*(-2 + D)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k]*
     ((2 - 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + 8*(-3 + D)*MB^2*MC^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-3 + D)*MB^2 - 4*MC^2 + s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + 8*(-3 + D)*MB^4*MC^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    (-2 + D)*MB^2*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
     (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    4*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(-8*s*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 4*s*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    2*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (-2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        2*(4*MB^2 - 4*MC^2 + 3*s)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])))*SMP["g_s"]^4)/
  (CW*(2 - D)*(3 - D)*MB^2*MC*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW), 0, 0, 0, 0, 
 ((I/2)*CA*CF*e*(4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k]*
     ((2 - 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + 4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      I*(-2 + D)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    8*(-3 + D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-3 + D)*MB^2 - 4*MC^2 + s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + (-2 + D)*MB^2*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k]*
     (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    8*(-3 + D)*MB^4*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    4*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(-8*s*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 4*s*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    2*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (-2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        2*(4*MB^2 - 4*MC^2 + 3*s)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])))*SMP["g_s"]^4)/
  (CW*(2 - D)*(3 - D)*MB^2*MC*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW), 
 ((I/2)*CA*CF*e*(4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k]*
     ((2 - 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + 4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      I*(-2 + D)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    8*(-3 + D)*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-3 + D)*MB^2 - 4*MC^2 + s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]]) + (-2 + D)*MB^2*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k]*
     (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    8*(-3 + D)*MB^4*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    4*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(-8*s*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 4*s*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    2*(-3 + D)*MB^2*MC^2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (-2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        2*(4*MB^2 - 4*MC^2 + 3*s)*Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]])))*SMP["g_s"]^4)/
  (CW*(2 - D)*(3 - D)*MB^2*MC*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW), 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, (-2*CA*CF*e*MC*((32*I)*(-4 + D)*(-3 + D)*MB^2*
     (4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
      8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
      LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (8*I)*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)^2*s*(4*MB^2 - 4*MC^2 + s)*
     (-4*MB^2 + 8*MB*MC - 4*MC^2 + s)^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (64*I)*(-4 + D)*(-3 + D)*MB^4*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (128*I)*(-4 + D)*(-3 + D)*MB^4*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (4*I)*(-4 + D)*(-2 + D)*(-4*MB^2 + 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[q, D]])^(-1), k] + 
    2*(-4 + D)*(-2 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(2 - D)*(-4 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*s*
     (64*MB^6 - (4*MC^2 - s)^3 - 16*MB^4*(12*MC^2 + s) + 
      4*MB^2*(48*MC^4 - 8*MC^2*s - s^2))*
     IntF[(4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
        2*Pair[Momentum[k, D], Momentum[p, D]] - 
        4*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
        4*MB^2*(32*MC^4 - 12*MC^2*s - s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((16*(-4 + 3*D)*MB^4 - 8*MB^2*(2*(-3 + D)*MC^2 + (-2 + D)*s) - 
          (4*MC^2 - s)*(4*MC^2 - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 2*(16*(-2 + D)*MB^4 - 16*MC^4 + 8*MC^2*s + 
          (-2 + D)*s^2 + 8*MB^2*(4*MC^2 - (-2 + D)*s))*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) - 
    2*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
        4*MB^2*(32*MC^4 - 12*MC^2*s - s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((16*(-4 + 3*D)*MB^4 - 8*MB^2*(2*(-3 + D)*MC^2 + (-2 + D)*s) - 
          (4*MC^2 - s)*(4*MC^2 - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 2*(16*(-2 + D)*MB^4 - 16*MC^4 + 8*MC^2*s + 
          (-2 + D)*s^2 + 8*MB^2*(4*MC^2 - (-2 + D)*s))*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) - 
    (-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - 
      s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    2*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(-4 + D)*(-3 + D)*MB^2*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((-4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + (4*MB^2 + 4*MC^2 - s)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) - 
    4*(-4 + D)*(-3 + D)*MB^2*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + (4*MB^2 + 4*MC^2 - s)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) - 
    2*(3 - D)*(-4 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
     IntF[1/((4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-8*I)*s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] - 
      (4*MB^2 - 4*MC^2 + s)*(s*(64*MB^6 - (4*MC^2 - s)^3 - 
          16*MB^4*(12*MC^2 + s) + 4*MB^2*(48*MC^4 - 8*MC^2*s - s^2))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        ((64*(-2 + D)*MB^6 - (-4*MC^2 + s)^2*(4*(-2 + D)*MC^2 - D*s) - 
            16*MB^4*(12*(-2 + D)*MC^2 + (-8 + 5*D)*s) + 4*(-2 + D)*MB^2*
             (48*MC^4 + 8*MC^2*s + 3*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (64*(-2 + D)*MB^6 - 16*(-2 + D)*MB^4*
             (12*MC^2 + 5*s) - (4*MC^2 - s)*(16*(-2 + D)*MC^4 - 8*D*MC^2*s - 
              3*(-2 + D)*s^2) + 4*MB^2*(48*(-2 + D)*MC^4 + 8*(-4 + D)*MC^
                2*s + 7*(-2 + D)*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    (3 - D)*(4 - D)*MB^2*(4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - 
      s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((-8*I)*s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + (4*MB^2 - 4*MC^2 + s)*
       (s*(64*MB^6 - (4*MC^2 - s)^3 - 16*MB^4*(12*MC^2 + s) + 
          4*MB^2*(48*MC^4 - 8*MC^2*s - s^2))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 
        ((64*(-2 + D)*MB^6 - (-4*MC^2 + s)^2*(4*(-2 + D)*MC^2 - D*s) - 
            16*MB^4*(12*(-2 + D)*MC^2 + (-8 + 5*D)*s) + 4*(-2 + D)*MB^2*
             (48*MC^4 + 8*MC^2*s + 3*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (64*(-2 + D)*MB^6 - 16*(-2 + D)*MB^4*
             (12*MC^2 + 5*s) - (4*MC^2 - s)*(16*(-2 + D)*MC^4 - 8*D*MC^2*s - 
              3*(-2 + D)*s^2) + 4*MB^2*(48*(-2 + D)*MC^4 + 8*(-4 + D)*MC^
                2*s + 7*(-2 + D)*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])))*
   SMP["g_s"]^4)/(CW*(-4 + D)*(-3 + D)*(-2 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)^3*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)^3*s*
   (4*MB^2 - 4*MC^2 + s)*SW), 
 (-2*CA*CF*e*MC*((32*I)*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s^2*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (8*I)*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)^2*s*(4*MB^2 - 4*MC^2 + s)*
     (-4*MB^2 + 8*MB*MC - 4*MC^2 + s)^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (64*I)*(-4 + D)*(-3 + D)*MB^4*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (128*I)*(-4 + D)*(-3 + D)*MB^4*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (4*I)*(-4 + D)*(-2 + D)*(-4*MB^2 + 4*MC^2 - s)*(4*MB^2 + 4*MC^2 - s)*s*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[q, D]])^(-1), k] + 
    2*(-4 + D)*(-2 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
        Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(2 - D)*(-4 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*s*
     (64*MB^6 - (4*MC^2 - s)^3 - 16*MB^4*(12*MC^2 + s) + 
      4*MB^2*(48*MC^4 - 8*MC^2*s - s^2))*
     IntF[(4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
        2*Pair[Momentum[k, D], Momentum[p, D]] - 
        4*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
        4*MB^2*(32*MC^4 - 12*MC^2*s - s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((16*(-4 + 3*D)*MB^4 - 8*MB^2*(2*(-3 + D)*MC^2 + (-2 + D)*s) - 
          (4*MC^2 - s)*(4*MC^2 - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 2*(16*(-2 + D)*MB^4 - 16*MC^4 + 8*MC^2*s + 
          (-2 + D)*s^2 + 8*MB^2*(4*MC^2 - (-2 + D)*s))*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) - 
    2*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
        4*MB^2*(32*MC^4 - 12*MC^2*s - s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((16*(-4 + 3*D)*MB^4 - 8*MB^2*(2*(-3 + D)*MC^2 + (-2 + D)*s) - 
          (4*MC^2 - s)*(4*MC^2 - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 2*(16*(-2 + D)*MB^4 - 16*MC^4 + 8*MC^2*s + 
          (-2 + D)*s^2 + 8*MB^2*(4*MC^2 - (-2 + D)*s))*
         Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) - 
    (-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - 
      s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    2*(-4 + D)*(-3 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
     (4*MB^2 - 4*MC^2 + s)^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] + 
      4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]) + 
    4*(-4 + D)*(-3 + D)*MB^2*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((-4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + (4*MB^2 + 4*MC^2 - s)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) - 
    4*(-4 + D)*(-3 + D)*MB^2*MC^2*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + (4*MB^2 + 4*MC^2 - s)*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) - 
    2*(3 - D)*(-4 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
     (16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
     IntF[1/((4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((-8*I)*s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]] - 
      (4*MB^2 - 4*MC^2 + s)*(s*(64*MB^6 - (4*MC^2 - s)^3 - 
          16*MB^4*(12*MC^2 + s) + 4*MB^2*(48*MC^4 - 8*MC^2*s - s^2))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] - 
        ((64*(-2 + D)*MB^6 - (-4*MC^2 + s)^2*(4*(-2 + D)*MC^2 - D*s) - 
            16*MB^4*(12*(-2 + D)*MC^2 + (-8 + 5*D)*s) + 4*(-2 + D)*MB^2*
             (48*MC^4 + 8*MC^2*s + 3*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (64*(-2 + D)*MB^6 - 16*(-2 + D)*MB^4*
             (12*MC^2 + 5*s) - (4*MC^2 - s)*(16*(-2 + D)*MC^4 - 8*D*MC^2*s - 
              3*(-2 + D)*s^2) + 4*MB^2*(48*(-2 + D)*MC^4 + 8*(-4 + D)*MC^
                2*s + 7*(-2 + D)*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])) + 
    (3 - D)*(4 - D)*MB^2*(4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - 
      s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((-8*I)*s^2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + (4*MB^2 - 4*MC^2 + s)*
       (s*(64*MB^6 - (4*MC^2 - s)^3 - 16*MB^4*(12*MC^2 + s) + 
          4*MB^2*(48*MC^4 - 8*MC^2*s - s^2))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] - 
        ((64*(-2 + D)*MB^6 - (-4*MC^2 + s)^2*(4*(-2 + D)*MC^2 - D*s) - 
            16*MB^4*(12*(-2 + D)*MC^2 + (-8 + 5*D)*s) + 4*(-2 + D)*MB^2*
             (48*MC^4 + 8*MC^2*s + 3*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + (64*(-2 + D)*MB^6 - 16*(-2 + D)*MB^4*
             (12*MC^2 + 5*s) - (4*MC^2 - s)*(16*(-2 + D)*MC^4 - 8*D*MC^2*s - 
              3*(-2 + D)*s^2) + 4*MB^2*(48*(-2 + D)*MC^4 + 8*(-4 + D)*MC^
                2*s + 7*(-2 + D)*s^2))*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])))*
   SMP["g_s"]^4)/(CW*(-4 + D)*(-3 + D)*(-2 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)^3*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)^3*s*
   (4*MB^2 - 4*MC^2 + s)*SW), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
