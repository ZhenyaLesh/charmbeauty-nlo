(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], 
    LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
   (-16*MB^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    32*MB^2*MC^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    16*MC^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MB^2*s*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MC^2*s*Pair[Momentum[k, D], Momentum[k, D]] - 
    s^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    32*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    8*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    40*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 12*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    24*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 4*D*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    6*s*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - D*s*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    8*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    24*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    2*s*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    D*s*Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], 
    LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
   (-16*MB^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    32*MB^2*MC^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    16*MC^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MB^2*s*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MC^2*s*Pair[Momentum[k, D], Momentum[k, D]] - 
    s^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    32*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    8*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    40*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 12*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    24*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 4*D*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    6*s*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - D*s*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    8*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    24*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    2*s*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    D*s*Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 
 ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (-16*MB^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    32*MB^2*MC^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    16*MC^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MB^2*s*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MC^2*s*Pair[Momentum[k, D], Momentum[k, D]] - 
    s^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    32*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    8*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    40*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 12*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    24*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 4*D*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    6*s*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - D*s*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    8*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    24*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    2*s*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    D*s*Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], 
    LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
   (-16*MB^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    32*MB^2*MC^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    16*MC^4*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MB^2*s*Pair[Momentum[k, D], Momentum[k, D]] + 
    8*MC^2*s*Pair[Momentum[k, D], Momentum[k, D]] - 
    s^2*Pair[Momentum[k, D], Momentum[k, D]] - 
    32*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    8*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    40*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 12*D*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    24*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 4*D*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    6*s*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - D*s*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    8*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MB^2*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    24*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    4*D*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    2*s*Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    D*s*Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   (16*MB^4*Pair[Momentum[k, D], Momentum[k, D]] - 
    32*MB^2*MC^2*Pair[Momentum[k, D], Momentum[k, D]] + 
    16*MC^4*Pair[Momentum[k, D], Momentum[k, D]] - 
    8*MB^2*s*Pair[Momentum[k, D], Momentum[k, D]] - 
    8*MC^2*s*Pair[Momentum[k, D], Momentum[k, D]] + 
    s^2*Pair[Momentum[k, D], Momentum[k, D]] + 
    16*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    16*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    4*s*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2)*
   SMP["g_s"]^4)/((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]] - 
    4*Pair[Momentum[k, D], Momentum[q, D]])*
   (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], 
    LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
   (16*MB^4*Pair[Momentum[k, D], Momentum[k, D]] - 
    32*MB^2*MC^2*Pair[Momentum[k, D], Momentum[k, D]] + 
    16*MC^4*Pair[Momentum[k, D], Momentum[k, D]] - 
    8*MB^2*s*Pair[Momentum[k, D], Momentum[k, D]] - 
    8*MC^2*s*Pair[Momentum[k, D], Momentum[k, D]] + 
    s^2*Pair[Momentum[k, D], Momentum[k, D]] + 
    16*MB^2*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    16*MB^2*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] - 
    4*s*Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*Pair[Momentum[k, D], Momentum[q, D]]^2)*
   SMP["g_s"]^4)/((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]] - 
    4*Pair[Momentum[k, D], Momentum[q, D]])*
   (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
