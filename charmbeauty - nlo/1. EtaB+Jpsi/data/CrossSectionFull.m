(* Created with the Wolfram Language : www.wolfram.com *)
{(Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*(PreContract[(2*ED*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*MZ^2*SW^2 + 
         MC^2*(1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
        \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
         Momentum[p], Momentum[q]]*
        (-(s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Gamma]1]])/2 + 
         Pair[LorentzIndex[\[Gamma]], Momentum[p2]]*
          Pair[LorentzIndex[\[Gamma]1], Momentum[p1]] + 
         Pair[LorentzIndex[\[Gamma]], Momentum[p1]]*
          Pair[LorentzIndex[\[Gamma]1], Momentum[p2]])*
        (-Pair[LorentzIndex[\[Psi]], LorentzIndex[\[Psi]1]] + 
         (Pair[LorentzIndex[\[Psi]], Momentum[p]]*Pair[LorentzIndex[\[Psi]1], 
            Momentum[p]])/(4*MC^2))*PreConjugate[
         (2*ED*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*MZ^2*SW^2 + 
            MC^2*(1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
           \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]1], 
            Momentum[p], Momentum[q]])/(3*CW^2*MC*Sqrt[MB*MC]*(2*MC - MZ)*
           (2*MC + MZ)*(-4*MB^2 + 4*MC^2 + s)*SW^2)])/(3*CW^2*MC*Sqrt[MB*MC]*
        (2*MC - MZ)*(2*MC + MZ)*(-4*MB^2 + 4*MC^2 + s)*SW^2)]/s^2 + 
    PreContract[-(MB*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*SW^2*(1 + 4*ED*SW^2) - 
          (MC^2*(-1 + 4*EU*SW^2)*(16*MC^4 + 32*MB^4*(1 + 4*ED*SW^2 + 8*ED^2*
                SW^4) - MZ^2*s*(3 + 8*ED*SW^2 + 16*ED^2*SW^4) + 
             4*MC^2*(s + (MZ + 4*ED*MZ*SW^2)^2) - 4*MB^2*((MZ + 4*ED*MZ*SW^2)^
                2 - 2*s*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) + 4*MC^2*
                (3 + 8*ED*SW^2 + 16*ED^2*SW^4))))/((2*MB - MZ)*(2*MB + MZ)*
            (-2*MC + MZ)*(2*MC + MZ)*(4*MB^2 - 4*MC^2 + s)))*\[Alpha]^(3/2)*
         Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
          Momentum[q]]*((-2*I)*(-1 + 4*SW^2)*Eps[LorentzIndex[\[Gamma]], 
            LorentzIndex[\[Gamma]1], Momentum[p1], Momentum[p2]] + 
          (1 + (-1 + 4*SW^2)^2)*(-(s*Pair[LorentzIndex[\[Gamma]], 
                LorentzIndex[\[Gamma]1]])/2 + Pair[LorentzIndex[\[Gamma]], 
              Momentum[p2]]*Pair[LorentzIndex[\[Gamma]1], Momentum[p1]] + 
            Pair[LorentzIndex[\[Gamma]], Momentum[p1]]*
             Pair[LorentzIndex[\[Gamma]1], Momentum[p2]]))*
         (-Pair[LorentzIndex[\[Psi]], LorentzIndex[\[Psi]1]] + 
          (Pair[LorentzIndex[\[Psi]], Momentum[p]]*
            Pair[LorentzIndex[\[Psi]1], Momentum[p]])/(4*MC^2))*
         PreConjugate[-(MB*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*SW^2*(1 + 
                4*ED*SW^2) - (MC^2*(-1 + 4*EU*SW^2)*(16*MC^4 + 32*MB^4*
                  (1 + 4*ED*SW^2 + 8*ED^2*SW^4) - MZ^2*s*(3 + 8*ED*SW^2 + 
                   16*ED^2*SW^4) + 4*MC^2*(s + (MZ + 4*ED*MZ*SW^2)^2) - 
                 4*MB^2*((MZ + 4*ED*MZ*SW^2)^2 - 2*s*(1 + 4*ED*SW^2 + 
                     8*ED^2*SW^4) + 4*MC^2*(3 + 8*ED*SW^2 + 16*ED^2*SW^4))))/(
                (2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
                (4*MB^2 - 4*MC^2 + s)))*\[Alpha]^(3/2)*
             Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]1], Momentum[p], 
              Momentum[q]])/(6*CW^3*(MB*MC)^(3/2)*(-4*MB^2 + 4*MC^2 + s)*
            SW^3)])/(6*CW^3*(MB*MC)^(3/2)*(-4*MB^2 + 4*MC^2 + s)*SW^3)]/
     (16*CW^2*SW^2*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    ((-MZ^2 + s)*Re[PreContract[-(MB*Sqrt[Pi]*Rb*Rc*
           (4*CW^2*ED*EU*SW^2*(1 + 4*ED*SW^2) - (MC^2*(-1 + 4*EU*SW^2)*
              (16*MC^4 + 32*MB^4*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) - MZ^2*s*
                (3 + 8*ED*SW^2 + 16*ED^2*SW^4) + 4*MC^2*(s + 
                 (MZ + 4*ED*MZ*SW^2)^2) - 4*MB^2*((MZ + 4*ED*MZ*SW^2)^2 - 
                 2*s*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) + 4*MC^2*(3 + 8*ED*SW^2 + 
                   16*ED^2*SW^4))))/((2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*
              (2*MC + MZ)*(4*MB^2 - 4*MC^2 + s)))*\[Alpha]^(3/2)*
           Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
            Momentum[q]]*((-I)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[
               \[Gamma]1], Momentum[p1], Momentum[p2]] + (-1 + 4*SW^2)*
             (-(s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Gamma]1]])/2 + 
              Pair[LorentzIndex[\[Gamma]], Momentum[p2]]*Pair[LorentzIndex[
                 \[Gamma]1], Momentum[p1]] + Pair[LorentzIndex[\[Gamma]], 
                Momentum[p1]]*Pair[LorentzIndex[\[Gamma]1], Momentum[p2]]))*
           (-Pair[LorentzIndex[\[Psi]], LorentzIndex[\[Psi]1]] + 
            (Pair[LorentzIndex[\[Psi]], Momentum[p]]*Pair[LorentzIndex[
                \[Psi]1], Momentum[p]])/(4*MC^2))*PreConjugate[
            (2*ED*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*MZ^2*SW^2 + MC^2*
                (1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
              \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]1], LorentzIndex[
                \[Psi]1], Momentum[p], Momentum[q]])/(3*CW^2*MC*Sqrt[MB*MC]*
              (2*MC - MZ)*(2*MC + MZ)*(-4*MB^2 + 4*MC^2 + s)*SW^2)])/
         (6*CW^3*(MB*MC)^(3/2)*(-4*MB^2 + 4*MC^2 + s)*SW^3)]])/
     (2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2))))/(8*s), 
 (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*(((-MZ^2 + s)*Re[PreContract[ConditionalExpression[
         ((-I)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[li1], Momentum[p1], 
             Momentum[p2]] + (-1 + 4*SW^2)*(-(s*Pair[LorentzIndex[li1], 
                 LorentzIndex[\[Gamma]]])/2 + Pair[LorentzIndex[li1], 
               Momentum[p2]]*Pair[LorentzIndex[\[Gamma]], Momentum[p1]] + 
             Pair[LorentzIndex[li1], Momentum[p1]]*Pair[LorentzIndex[
                \[Gamma]], Momentum[p2]]))*
          (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
           (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
              Momentum[p]])/(4*MC^2))*((64*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*
             (-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - 
                      s)) - 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - 
                    s))), 4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 
                   2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), 
                -4*MB^2 + 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                      s))/4 + ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                      s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[
               Psi1], Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
             (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
              4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
           (((16*I)/3)*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
                (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 
                 3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - 
                (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*
                Sqrt[s*(-4*MC^2 + s)]*(I*Pi + Log[-(2*MC^2 - s + Sqrt[
                      s*(-4*MC^2 + s)])/(2*MC^2)]))/(-64*MB^6 + (4*MC^2 - s)^
                 3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*(-48*MC^4 + 8*MC^2*s + 
                  s^2))))/(CW*Sqrt[MB*MC]*Sqrt[Pi]*SW) - 
           (64*MB^2*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]) - DiLog[
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                 8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4]])^2/(2*Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
              PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2)]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]))/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 
              (-4*MC^2 + s)^2)*SW))*PreConjugate[(2*ED*Sqrt[Pi]*Rb*Rc*
             (4*CW^2*ED*EU*MZ^2*SW^2 + MC^2*(1 - 4*EU*SW^2 - 4*ED*SW^2*
                 (-1 + 4*CW^2*EU + 4*EU*SW^2)))*\[Alpha]^(3/2)*
             Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
              Momentum[q]])/(3*CW^2*MC*Sqrt[MB*MC]*(2*MC - MZ)*(2*MC + MZ)*
             (-4*MB^2 + 4*MC^2 + s)*SW^2)], 16*MB^4 - 32*MB^2*MC^2 + 
           16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2 > 0]]])/
     (2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    Re[PreContract[ConditionalExpression[
        ((-2*I)*(-1 + 4*SW^2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[li1], 
            Momentum[p1], Momentum[p2]] + (1 + (-1 + 4*SW^2)^2)*
           (-(s*Pair[LorentzIndex[li1], LorentzIndex[\[Gamma]]])/2 + 
            Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[
               \[Gamma]], Momentum[p1]] + Pair[LorentzIndex[li1], 
              Momentum[p1]]*Pair[LorentzIndex[\[Gamma]], Momentum[p2]]))*
         (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
          (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
             Momentum[p]])/(4*MC^2))*((64*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*
            (-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - 
                     s)) - 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - 
                   s))), 4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -((4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                 (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
          (((16*I)/3)*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
               (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 3*s)*Log[
                (2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - (2*MC^2 - s)*
                (-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(
                I*Pi + Log[-(2*MC^2 - s + Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/
              (-64*MB^6 + (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*
                (-48*MC^4 + 8*MC^2*s + s^2))))/(CW*Sqrt[MB*MC]*Sqrt[Pi]*SW) - 
          (64*MB^2*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 
                   8*MC^2*s - s^2)/4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
              (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]) - PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2)]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4]))/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - (-4*MC^2 + s)^2)*SW))*PreConjugate[
          -(MB*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*SW^2*(1 + 4*ED*SW^2) - 
              (MC^2*(-1 + 4*EU*SW^2)*(16*MC^4 + 32*MB^4*(1 + 4*ED*SW^2 + 
                   8*ED^2*SW^4) - MZ^2*s*(3 + 8*ED*SW^2 + 16*ED^2*SW^4) + 
                 4*MC^2*(s + (MZ + 4*ED*MZ*SW^2)^2) - 4*MB^2*
                  ((MZ + 4*ED*MZ*SW^2)^2 - 2*s*(1 + 4*ED*SW^2 + 8*ED^2*
                      SW^4) + 4*MC^2*(3 + 8*ED*SW^2 + 16*ED^2*SW^4))))/(
                (2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
                (4*MB^2 - 4*MC^2 + s)))*\[Alpha]^(3/2)*
             Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
              Momentum[q]])/(6*CW^3*(MB*MC)^(3/2)*(-4*MB^2 + 4*MC^2 + s)*
            SW^3)], 16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + 
          s^2 > 0]]]/(8*CW^2*SW^2*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    (2*Re[PreContract[(-(s*Pair[LorentzIndex[li1], LorentzIndex[\[Gamma]]])/
           2 + Pair[LorentzIndex[li1], Momentum[p2]]*
           Pair[LorentzIndex[\[Gamma]], Momentum[p1]] + 
          Pair[LorentzIndex[li1], Momentum[p1]]*Pair[LorentzIndex[\[Gamma]], 
            Momentum[p2]])*(-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
          (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
             Momentum[p]])/(4*MC^2))*((256*EU*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - 
             s)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - 
                     s)) - 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - 
                   s))), 4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -((4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                 (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
          (((64*I)/3)*EU*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
               (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 3*s)*Log[
                (2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - (2*MC^2 - s)*
                (-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(
                I*Pi + Log[-(2*MC^2 - s + Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/
              (-64*MB^6 + (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*
                (-48*MC^4 + 8*MC^2*s + s^2))))/(Sqrt[MB*MC]*Sqrt[Pi]) - 
          (256*EU*MB^2*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 
                   8*MC^2*s - s^2)/4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
              (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]) - PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2)]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4]))/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - (-4*MC^2 + s)^2)))*PreConjugate[
          (2*ED*Sqrt[Pi]*Rb*Rc*(4*CW^2*ED*EU*MZ^2*SW^2 + 
             MC^2*(1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
            \[Alpha]^(3/2)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
             Momentum[p], Momentum[q]])/(3*CW^2*MC*Sqrt[MB*MC]*(2*MC - MZ)*
            (2*MC + MZ)*(-4*MB^2 + 4*MC^2 + s)*SW^2)]]])/s^2 + 
    ((-MZ^2 + s)*Re[PreContract[-(MB*Sqrt[Pi]*Rb*Rc*
           (4*CW^2*ED*EU*SW^2*(1 + 4*ED*SW^2) - (MC^2*(-1 + 4*EU*SW^2)*
              (16*MC^4 + 32*MB^4*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) - MZ^2*s*
                (3 + 8*ED*SW^2 + 16*ED^2*SW^4) + 4*MC^2*(s + 
                 (MZ + 4*ED*MZ*SW^2)^2) - 4*MB^2*((MZ + 4*ED*MZ*SW^2)^2 - 
                 2*s*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) + 4*MC^2*(3 + 8*ED*SW^2 + 
                   16*ED^2*SW^4))))/((2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*
              (2*MC + MZ)*(4*MB^2 - 4*MC^2 + s)))*\[Alpha]^(3/2)*
           Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
            Momentum[q]]*((-I)*Eps[LorentzIndex[\[Gamma]], LorentzIndex[li1], 
              Momentum[p1], Momentum[p2]] + (-1 + 4*SW^2)*
             (-(s*Pair[LorentzIndex[li1], LorentzIndex[\[Gamma]]])/2 + 
              Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[
                 \[Gamma]], Momentum[p1]] + Pair[LorentzIndex[li1], 
                Momentum[p1]]*Pair[LorentzIndex[\[Gamma]], Momentum[p2]]))*
           (-Pair[LorentzIndex[Psi1], LorentzIndex[\[Psi]]] + 
            (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[\[Psi]], 
               Momentum[p]])/(4*MC^2))*PreConjugate[
            (256*EU*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*Sqrt[\[Alpha]]*
               \[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]) - 
                DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[
                       4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                        s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), 4*MB^2 - 
                   4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
                DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[
                       4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                        s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 
                   4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
                DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                    ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                        2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                   (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                    ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                  -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4] - 
                DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                    ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                   (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                    ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                  4*MB^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
                DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                    ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                        2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                   (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                    ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                  (4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4] - 
                DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MC^2 + s)*
                      (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                    s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                    Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                       2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                   2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4] - 
                DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - s*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MC^2 + s)*
                      (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4)]), -4*MB^2 - 4*MC^2 + s + 2*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                    s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                    Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                       2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                   2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], 
                LorentzIndex[Psi1], Momentum[p], Momentum[q]])/
              (3*Sqrt[MB*MC]*Sqrt[Pi]*(64*MB^6 + (4*MC^2 - s)^3 - 
                16*MB^4*(4*MC^2 + 3*s) - 4*MB^2*(16*MC^4 + 8*MC^2*s - 
                  3*s^2))) - (((64*I)/3)*EU*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^
                2*Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
                Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
                  (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                  4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 
                   3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - 
                  (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                  4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - 
                ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(I*Pi + Log[-(2*MC^2 - s + 
                       Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/(-64*MB^6 + 
                  (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*(-48*MC^4 + 
                    8*MC^2*s + s^2))))/(Sqrt[MB*MC]*Sqrt[Pi]) - 
             (256*EU*MB^2*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*Eps[
                LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
                Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]) - 
                DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                  2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
                DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                  -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
                DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/
                     4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                        2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((16*MB^4 + 
                      32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                    ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                  4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
                DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/
                     4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                        2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((16*MB^4 + 
                      32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                    ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                  -4*MB^4 - 8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
                DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/
                     4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                   ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                    ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                        4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                  4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 + 
                   ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
                 Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
                 (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4]) - PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/(8*MB^2*MC^2)]/Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]))/
              (3*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - (-4*MC^2 + s)^2))])/
         (6*CW^3*(MB*MC)^(3/2)*(-4*MB^2 + 4*MC^2 + s)*SW^3)]])/
     (2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2))))/(8*s), 
 (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*(((-MZ^2 + s)*Re[PreContract[ConditionalExpression[
         ((-I)*Eps[LorentzIndex[li1], LorentzIndex[li2], Momentum[p1], 
             Momentum[p2]] + (-1 + 4*SW^2)*(-(s*Pair[LorentzIndex[li1], 
                 LorentzIndex[li2]])/2 + Pair[LorentzIndex[li1], Momentum[
                p2]]*Pair[LorentzIndex[li2], Momentum[p1]] + 
             Pair[LorentzIndex[li1], Momentum[p1]]*Pair[LorentzIndex[li2], 
               Momentum[p2]]))*(-Pair[LorentzIndex[Psi1], LorentzIndex[
              Psi2]] + (Pair[LorentzIndex[Psi1], Momentum[p]]*
             Pair[LorentzIndex[Psi2], Momentum[p]])/(4*MC^2))*
          ((64*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*(-1 + 4*EU*SW^2)*
             Sqrt[\[Alpha]]*\[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
              DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), 4*MB^2 - 
                 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 
                 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                      s))/4 + ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                      s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[
               Psi1], Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
             (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
              4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
           (((16*I)/3)*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
                (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 
                 3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - 
                (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*
                Sqrt[s*(-4*MC^2 + s)]*(I*Pi + Log[-(2*MC^2 - s + Sqrt[
                      s*(-4*MC^2 + s)])/(2*MC^2)]))/(-64*MB^6 + (4*MC^2 - s)^
                 3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*(-48*MC^4 + 8*MC^2*s + 
                  s^2))))/(CW*Sqrt[MB*MC]*Sqrt[Pi]*SW) - 
           (64*MB^2*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
              Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]) - DiLog[
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                 8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4]])^2/(2*Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
              PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2)]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]))/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 
              (-4*MC^2 + s)^2)*SW))*PreConjugate[
           (256*EU*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*Sqrt[\[Alpha]]*
              \[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]) - DiLog[
                 -((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (MC^2*(-4*MB^2 + 4*MC^2 - s))), 4*MB^2 - 4*MC^2 + s - 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                 -((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[4*MB^4 - 
                       8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                   (MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*MC^2 - s + 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + ((4*MB^2 + s)*
                     Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                       2*MC^2*s + s^2/4])/2)/(-((4*MB^2 - s)*(4*MB^2 - 
                       4*MC^2 + s))/4 + ((-4*MB^2 + 4*MC^2 - s)*Sqrt[
                      4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                       s^2/4])/2), -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
                  2*(4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
               DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                   ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                  (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                   ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 4*MB^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
               DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                   ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                       2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                  (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                   ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 (4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                   s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                   Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (((4*MB^2 + 4*MC^2 - s)*s)/2 + s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[s*(-4*MC^2 + s)*
                     (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4)]), 4*MB^2 + 4*MC^2 - s + 2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                   s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                   Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                  2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (((4*MB^2 + 4*MC^2 - s)*s)/2 + s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[s*(-4*MC^2 + s)*
                     (4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4)]), -4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4])*Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[
                p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*(64*MB^6 + 
               (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 4*MB^2*(16*MC^4 + 
                 8*MC^2*s - 3*s^2))) - (((64*I)/3)*EU*MC*Rb*Rc*Sqrt[\[Alpha]]*
              \[Alpha]s^2*Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[
                p], Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
                 (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                 4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 
                  3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - 
                 (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                 4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*
                 Sqrt[s*(-4*MC^2 + s)]*(I*Pi + Log[-(2*MC^2 - s + Sqrt[
                       s*(-4*MC^2 + s)])/(2*MC^2)]))/(-64*MB^6 + (4*MC^2 - s)^
                  3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*(-48*MC^4 + 8*MC^2*s + 
                   s^2))))/(Sqrt[MB*MC]*Sqrt[Pi]) - 
            (256*EU*MB^2*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*Eps[LorentzIndex[
                li2], LorentzIndex[Psi2], Momentum[p], Momentum[q]]*
              (-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4]) - DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - 
                     s) - 4*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/(-2*MB^2*(4*MB^2 + 
                     4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 2*MB^2 + 
                  2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                 -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
               DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                   ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                       2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((16*MB^4 + 
                     32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                   ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                   ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                       2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((16*MB^4 + 
                     32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                   ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 -4*MB^4 - 8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                    Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                      2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                   ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                  ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                   ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                       4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                 4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
               (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4]])^2/(2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
               PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                  (8*MB^2*MC^2)]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]))/(3*Sqrt[MB*MC]*Sqrt[Pi]*
              (16*MB^4 - (-4*MC^2 + s)^2))], 16*MB^4 - 32*MB^2*MC^2 + 
           16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2 > 0]]])/
     (2*CW*s*SW*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    Re[PreContract[ConditionalExpression[
        ((-2*I)*(-1 + 4*SW^2)*Eps[LorentzIndex[li1], LorentzIndex[li2], 
            Momentum[p1], Momentum[p2]] + (1 + (-1 + 4*SW^2)^2)*
           (-(s*Pair[LorentzIndex[li1], LorentzIndex[li2]])/2 + 
            Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[li2], 
              Momentum[p1]] + Pair[LorentzIndex[li1], Momentum[p1]]*
             Pair[LorentzIndex[li2], Momentum[p2]]))*
         (-Pair[LorentzIndex[Psi1], LorentzIndex[Psi2]] + 
          (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[Psi2], 
             Momentum[p]])/(4*MC^2))*((64*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*
            (-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - 
                     s)) - 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - 
                   s))), 4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -((4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                 (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
             Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
          (((16*I)/3)*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
               (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 3*s)*Log[
                (2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - (2*MC^2 - s)*
                (-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(
                I*Pi + Log[-(2*MC^2 - s + Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/
              (-64*MB^6 + (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*
                (-48*MC^4 + 8*MC^2*s + s^2))))/(CW*Sqrt[MB*MC]*Sqrt[Pi]*SW) - 
          (64*MB^2*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
             Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 
                   8*MC^2*s - s^2)/4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
              (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]) - PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2)]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4]))/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - (-4*MC^2 + s)^2)*SW))*PreConjugate[
          (64*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*(-1 + 4*EU*SW^2)*
             Sqrt[\[Alpha]]*\[Alpha]s^2*(Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
              DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), 4*MB^2 - 
                 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[
                     4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                      s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 
                 4*MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                      s))/4 + ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 
                      8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                      s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[
                4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                     2*MC^2*s + s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                  Sqrt[s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4])*Eps[LorentzIndex[li2], LorentzIndex[
               Psi2], Momentum[p], Momentum[q]])/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*
             (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
              4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*SW) - 
           (((16*I)/3)*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
              Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
                (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 
                 3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - 
                (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
                4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*
                Sqrt[s*(-4*MC^2 + s)]*(I*Pi + Log[-(2*MC^2 - s + Sqrt[
                      s*(-4*MC^2 + s)])/(2*MC^2)]))/(-64*MB^6 + (4*MC^2 - s)^
                 3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*(-48*MC^4 + 8*MC^2*s + 
                  s^2))))/(CW*Sqrt[MB*MC]*Sqrt[Pi]*SW) - 
           (64*MB^2*MC*Rb*Rc*(-1 + 4*EU*SW^2)*Sqrt[\[Alpha]]*\[Alpha]s^2*
             Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
              Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]) - DiLog[
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
                -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                   2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] - DiLog[
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                 8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                   Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                     s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4] + DiLog[
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                      2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                 ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                  ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                      4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
                4*MB^4 + 8*MB^2*MC^2 - (-4*MC^2 + s)^2/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
              (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4]])^2/(2*Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
              PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
              PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*MB^2*MC^2)]/
               Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]))/(3*CW*Sqrt[MB*MC]*Sqrt[Pi]*(16*MB^4 - 
              (-4*MC^2 + s)^2)*SW)], 16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 
          8*MB^2*s - 8*MC^2*s + s^2 > 0]]]/(16*CW^2*SW^2*
      ((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    Re[PreContract[(-(s*Pair[LorentzIndex[li1], LorentzIndex[li2]])/2 + 
         Pair[LorentzIndex[li1], Momentum[p2]]*Pair[LorentzIndex[li2], 
           Momentum[p1]] + Pair[LorentzIndex[li1], Momentum[p1]]*
          Pair[LorentzIndex[li2], Momentum[p2]])*
        (-Pair[LorentzIndex[Psi1], LorentzIndex[Psi2]] + 
         (Pair[LorentzIndex[Psi1], Momentum[p]]*Pair[LorentzIndex[Psi2], 
            Momentum[p]])/(4*MC^2))*
        ((256*EU*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*Sqrt[\[Alpha]]*\[Alpha]s^2*
           (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - 
                    s)) - 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - 
                  s))), 4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*
                MC^2 - s + 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                  2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*
                MB^2*s - 2*MC^2*s + s^2/4] + 
            DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4])/2)/(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                    s))/4 + ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2), 
              -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                    s))/4 + ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*
                     MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(
                -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                    s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(
                -((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                 s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                 s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                 s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                 s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                   2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 2*
                Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                  s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4])*Eps[LorentzIndex[li1], LorentzIndex[Psi1], 
            Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
           (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
            4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
         (((64*I)/3)*EU*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
           Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
            Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
              (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 
              4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - 
            (I*(4*MB^2 - 4*MC^2 + 3*s)*Log[(2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/
             (64*MB^6 - (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + 
                s) + 4*MB^2*(32*MC^4 - 12*MC^2*s - s^2)) - 
            ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(I*Pi + Log[-(2*MC^2 - s + 
                   Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/(-64*MB^6 + 
              (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*(-48*MC^4 + 
                8*MC^2*s + s^2))))/(Sqrt[MB*MC]*Sqrt[Pi]) - 
         (256*EU*MB^2*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
           Eps[LorentzIndex[li1], LorentzIndex[Psi1], Momentum[p], 
            Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4]) - 
            DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(
                -2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
              2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*
                MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
            DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(
                -2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
              -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*
                MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
            DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2)/((16*MB^4 + 32*MB^2*
                   MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + ((-4*MB^2 + 4*MC^2 - 
                   s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
               (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] - DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - 
                  s^2)/4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(
                (16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 8*MB^2*MC^2 + 
               (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] + DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - 
                  s^2)/4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/(
                (16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                    2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
               (-4*MC^2 + s)^2/4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
             (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4]) - PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                  4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 
                 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
             Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/
                4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/(8*
                MB^2*MC^2)]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*
                MC^2*s + s^2/4]))/(3*Sqrt[MB*MC]*Sqrt[Pi]*
           (16*MB^4 - (-4*MC^2 + s)^2)))*PreConjugate[
         (256*EU*MC^3*Rb*Rc*(-4*MB^2 + 4*MC^2 - s)*Sqrt[\[Alpha]]*\[Alpha]s^2*
            (Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                 2*MC^2*s + s^2/4]) - DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - 
                     s)) - 2*MC^2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/(MC^2*(-4*MB^2 + 4*MC^2 - 
                   s))), 4*MB^2 - 4*MC^2 + s - 2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 
                8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[-((-(MC^2*(-4*MB^2 + 4*MC^2 - s)) - 2*MC^2*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                 (MC^2*(-4*MB^2 + 4*MC^2 - s))), -4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -((4*MB^2 - s)*
                  (4*MB^2 - 4*MC^2 + s)) + 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + 
                     s))/4 + ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                     4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                (-((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s))/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), (4*MB^2 - s)*
                 (4*MB^2 - 4*MC^2 + s) - 2*(4*MB^2 + s)*Sqrt[4*MB^4 - 
                   8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 - Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), 4*MB^2 + 4*MC^2 - s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] - DiLog[(-(s*(-4*MB^2 - 4*MC^2 + s))/2 - 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s + 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4] + DiLog[(((4*MB^2 + 4*MC^2 - s)*s)/2 + 
                 s*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/(((4*MB^2 + 4*MC^2 - s)*s)/2 + Sqrt[
                  s*(-4*MC^2 + s)*(4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                    2*MC^2*s + s^2/4)]), -4*MB^2 - 4*MC^2 + s - 
                2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                   s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 
                2*MC^2*s + s^2/4])*Eps[LorentzIndex[li2], LorentzIndex[Psi2], 
             Momentum[p], Momentum[q]])/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
             4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))) - 
          (((64*I)/3)*EU*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
             Momentum[q]]*((Pi*(4*MB^2 - 4*MC^2 + 3*s))/(64*MB^6 - 
               (2*MC^2 - s)*(-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - (I*(4*MB^2 - 4*MC^2 + 3*s)*Log[
                (2*MC^2)/(4*MB^2 - 4*MC^2 + s)])/(64*MB^6 - (2*MC^2 - s)*
                (-4*MC^2 + s)^2 - 16*MB^4*(10*MC^2 + s) + 4*MB^2*
                (32*MC^4 - 12*MC^2*s - s^2)) - ((4*I)*Sqrt[s*(-4*MC^2 + s)]*(
                I*Pi + Log[-(2*MC^2 - s + Sqrt[s*(-4*MC^2 + s)])/(2*MC^2)]))/
              (-64*MB^6 + (4*MC^2 - s)^3 + 16*MB^4*(12*MC^2 + s) + 4*MB^2*
                (-48*MC^4 + 8*MC^2*s + s^2))))/(Sqrt[MB*MC]*Sqrt[Pi]) - 
          (256*EU*MB^2*MC*Rb*Rc*Sqrt[\[Alpha]]*\[Alpha]s^2*
            Eps[LorentzIndex[li2], LorentzIndex[Psi2], Momentum[p], 
             Momentum[q]]*(-Pi^2/(6*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                 2*MB^2*s - 2*MC^2*s + s^2/4]) - 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               2*MB^2 + 2*MC^2 - s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[(-2*MB^2*(4*MB^2 + 4*MC^2 - s) + 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (-2*MB^2*(4*MB^2 + 4*MC^2 - s) - 4*MB^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]), 
               -2*MB^2 - 2*MC^2 + s/2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                  2*MB^2*s - 2*MC^2*s + s^2/4]]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4] - 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 - ((4*MB^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 
                   8*MC^2*s - s^2)/4 + ((-4*MB^2 - s)*Sqrt[4*MB^4 - 
                     8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), -4*MB^4 - 
                8*MB^2*MC^2 + (-4*MC^2 + s)^2/4 + ((4*MB^2 + s)*
                  Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                    s^2/4])/2]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4] + 
             DiLog[((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2)/
                ((16*MB^4 + 32*MB^2*MC^2 - 16*MC^4 + 8*MC^2*s - s^2)/4 + 
                 ((-4*MB^2 + 4*MC^2 - s)*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                     2*MB^2*s - 2*MC^2*s + s^2/4])/2), 4*MB^4 + 8*MB^2*MC^2 - 
                (-4*MC^2 + s)^2/4 + ((4*MB^2 - 4*MC^2 + s)*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/2]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] - (I*Pi + Log[(4*MB^2)/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                    4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]])^2/
              (2*Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                 s^2/4]) - PolyLog[2, (4*MB^2 + Sqrt[4*MB^4 - 8*MB^2*MC^2 + 
                   4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/Sqrt[4*MB^4 - 
                  8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4]]/
              Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + 
                s^2/4] + PolyLog[2, (8*MB^2*MC^2 + 2*MC^2*Sqrt[4*MB^4 - 
                    8*MB^2*MC^2 + 4*MC^4 - 2*MB^2*s - 2*MC^2*s + s^2/4])/
                (8*MB^2*MC^2)]/Sqrt[4*MB^4 - 8*MB^2*MC^2 + 4*MC^4 - 
                2*MB^2*s - 2*MC^2*s + s^2/4]))/(3*Sqrt[MB*MC]*Sqrt[Pi]*
            (16*MB^4 - (-4*MC^2 + s)^2))]]]/s^2))/(8*s)}
