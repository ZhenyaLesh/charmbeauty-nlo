(* Created with the Wolfram Language : www.wolfram.com *)
((16*I)*CA*CF*e*EU*LTensor[LeviCivitaE, li1, p, Psi1, q]*
   (4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*PVA[0, MB] + 
    (-2 + D)*MB^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     PVA[0, MC] - 8*(-3 + D)*MB^2*MC^2*(4*(-3 + D)*MB^2 - 4*MC^2 + s)*
     PVB[0, 0, 4*MB^2, 0, 0] + 2*(-3 + D)*MB^2*MC^2*
     (4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
     PVB[0, 0, 2*MB^2 - MC^2 + s/2, MC, 0] + 16*(-3 + D)*MB^2*MC^2*s*
     PVB[0, 0, s/4, MB, MC] - 8*(-3 + D)*MB^4*MC^2*
     (4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     PVC[0, 0, 0, MC^2, 2*MB^2 - MC^2 + s/2, 4*MB^2, 0, MC, 0] - 
    4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*PVC[0, 0, 0, s/4, 
      2*MB^2 - MC^2 + s/2, MB^2, MB, MC, 0])*SMP["g_s"]^4)/
  ((2 - D)*(3 - D)*MB^2*MC*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
 ((32*I)*CA*CF*e*EU*MC*LTensor[LeviCivitaE, li1, p, Psi1, q]*
   ((-2*(-2 + D)*PVA[0, MB])/((-3 + D)*MB^2) + 4*PVB[0, 0, 4*MB^2, 0, 0] - 
    (8*s*PVB[0, 0, s/4, MB, MC])/(4*MB^2 - 4*MC^2 + s) + 
    (8*s*PVB[0, 0, s, MC, MC])/(4*MB^2 - 4*MC^2 + s) + 
    (32*MB^2*MC^2*PVC[0, 0, 0, MC^2, 2*MB^2 - MC^2 + s/2, 4*MB^2, 0, MC, 0])/
     (4*MB^2 + 4*MC^2 - s) - (4*MC^2*(4*MB^2 - 4*MC^2 + s)*
      PVC[0, 0, 0, MC^2, s, 2*MB^2 - MC^2 + s/2, 0, MC, MC])/
     (4*MB^2 + 4*MC^2 - s) + (32*MB^2*MC^2*PVC[0, 0, 0, 2*MB^2 - MC^2 + s/2, 
       MC^2, 4*MB^2, 0, MC, 0])/(4*MB^2 + 4*MC^2 - s) + 
    (4*MB^2 - 4*MC^2 + s)*PVC[0, 0, 0, 2*MB^2 - MC^2 + s/2, s/4, MB^2, 0, MC, 
      MB] + (4*MB^2 - 4*MC^2 + s)*PVC[0, 0, 0, s/4, 2*MB^2 - MC^2 + s/2, 
      MB^2, MB, MC, 0] - (4*MC^2*(4*MB^2 - 4*MC^2 + s)*
      PVC[0, 0, 0, s, MC^2, 2*MB^2 - MC^2 + s/2, MC, MC, 0])/
     (4*MB^2 + 4*MC^2 - s))*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s))
