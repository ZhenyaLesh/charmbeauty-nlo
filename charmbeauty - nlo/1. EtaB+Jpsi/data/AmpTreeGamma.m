(* Created with the Wolfram Language : www.wolfram.com *)
(e^3*ED^2*EU*Rb*Rc*Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], 
   Momentum[p], Momentum[q]])/(Sqrt[MB*MC]*Pi*(4*MB^2*MC - 4*MC^3 - MC*s)*
  SUNN)
