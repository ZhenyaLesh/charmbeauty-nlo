(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, ((2*I)*CA*CF*e*MC*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     ((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       (4*MB^2*Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[k, D]] - 16*(-4 + D)*MB^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    2*((4*(-10 + 3*D)*MB^2 + (-6 + D)*(4*MC^2 - s))*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  (CW*(-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
    s)*(4*MB^2 - 4*MC^2 + s)*SW*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 ((2*I)*CA*CF*e*MC*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     ((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] - I*(-2 + D)*
       (4*MB^2*Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[k, D]] - 16*(-4 + D)*MB^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    2*((4*(-10 + 3*D)*MB^2 + (-6 + D)*(4*MC^2 - s))*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  (CW*(-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
    s)*(4*MB^2 - 4*MC^2 + s)*SW*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 
 ((2*I)*CA*CF*e*MC*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     ((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       (4*MB^2*Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[k, D]] - 16*(-4 + D)*MB^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    2*((4*(-10 + 3*D)*MB^2 + (-6 + D)*(4*MC^2 - s))*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  (CW*(-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
    s)*(4*MB^2 - 4*MC^2 + s)*SW*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 ((2*I)*CA*CF*e*MC*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     ((-2 + 8*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + I*(-2 + D)*
       (4*MB^2*Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[k, D]] - 16*(-4 + D)*MB^2*(-1 + 4*EU*SW^2)*
     Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
      Momentum[q, D]]*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    2*((4*(-10 + 3*D)*MB^2 + (-6 + D)*(4*MC^2 - s))*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + I*(-2 + D)*(4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]]*Pair[LorentzIndex[Psi1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]] + 
    (-2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] - I*(-2 + D)*((16*MB^4 + (-4*MC^2 + s)^2 - 
          8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]]*
         Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  (CW*(-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
    s)*(4*MB^2 - 4*MC^2 + s)*SW*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 ((2*I)*CA*CF*e*MC*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     (-2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + (4*I)*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] - I*(-8*MB^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 - 
          8*MB^2*(-2*s + D*(4*MC^2 + s)))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] - (4*I)*(-2 + D)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[LorentzIndex[Psi1, D], Momentum[q, D]]*Pair[Momentum[k, D], 
      Momentum[k, D]]*Pair[Momentum[k, D], Momentum[p, D]] + 
    (16*I)*MB^2*((2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + 4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    (2*I)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (D*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        16*(-1 + D)*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 
    8*(-((64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
         4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*(-1 + 4*EU*SW^2)*
        Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
         Momentum[q, D]]) + (2*I)*MB^2*
       ((64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
          4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 
        ((-128*MB^2*MC^2 + D*(16*MB^4 + MB^2*(96*MC^2 - 8*s) + 
              (-4*MC^2 + s)^2))*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          16*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - (8*I)*(4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2*Pair[Momentum[k, D], 
      Momentum[q, D]] + 32*MC^2*
     (-((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
        Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
         Momentum[q, D]]) + (2*I)*MB^2*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    (8*I)*((64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
        4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((-128*MB^2*MC^2 + D*(16*MB^4 + MB^2*(96*MC^2 - 8*s) + 
            (-4*MC^2 + s)^2))*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        16*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    (32*I)*MC^2*((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^3)*SMP["g_s"]^4)/
  (CW*(-2 + D)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)^2*
   (-4*MB^2 + 8*MB*MC - 4*MC^2 + s)^2*SW*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]] - 
    4*Pair[Momentum[k, D], Momentum[q, D]])*
   (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 ((2*I)*CA*CF*e*MC*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     (-2*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
       Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
        Momentum[q, D]] + (4*I)*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] - I*(-8*MB^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 - 
          8*MB^2*(-2*s + D*(4*MC^2 + s)))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] - (4*I)*(-2 + D)*
     (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[LorentzIndex[Psi1, D], Momentum[q, D]]*Pair[Momentum[k, D], 
      Momentum[k, D]]*Pair[Momentum[k, D], Momentum[p, D]] + 
    (16*I)*MB^2*((2*I)*(16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       (-1 + 4*EU*SW^2)*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
        Momentum[p, D], Momentum[q, D]] + 4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    (2*I)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      (D*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        16*(-1 + D)*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
       Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 
    8*(-((64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
         4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*(-1 + 4*EU*SW^2)*
        Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
         Momentum[q, D]]) + (2*I)*MB^2*
       ((64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
          4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*Pair[LorentzIndex[li1, D], 
          LorentzIndex[Psi1, D]] + 
        ((-128*MB^2*MC^2 + D*(16*MB^4 + MB^2*(96*MC^2 - 8*s) + 
              (-4*MC^2 + s)^2))*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
          16*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - (8*I)*(4*MB^2*(16*MB^4 + (-4*MC^2 + s)^2 - 
        8*MB^2*(4*MC^2 + s))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + (8*(-1 + D)*MB^2*(4*MB^2 + 4*MC^2 - s)*
         Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        (16*(-2 + D)*MB^4 + (-2 + D)*(-4*MC^2 + s)^2 + 
          8*MB^2*(4*D*MC^2 + 2*s - D*s))*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2*Pair[Momentum[k, D], 
      Momentum[q, D]] + 32*MC^2*
     (-((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*(-1 + 4*EU*SW^2)*
        Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], Momentum[p, D], 
         Momentum[q, D]]) + (2*I)*MB^2*
       ((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
         Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
        2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
            Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
            Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]]))*
     Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    (8*I)*((64*MB^6 + (4*MC^2 - s)^3 - 16*MB^4*(4*MC^2 + 3*s) - 
        4*MB^2*(16*MC^4 + 8*MC^2*s - 3*s^2))*Pair[LorentzIndex[li1, D], 
        LorentzIndex[Psi1, D]] + 
      ((-128*MB^2*MC^2 + D*(16*MB^4 + MB^2*(96*MC^2 - 8*s) + 
            (-4*MC^2 + s)^2))*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
        16*(-1 + D)*MC^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]]^2 - 
    (32*I)*MC^2*((16*MB^4 + (-4*MC^2 + s)^2 - 8*MB^2*(4*MC^2 + s))*
       Pair[LorentzIndex[li1, D], LorentzIndex[Psi1, D]] + 
      2*(-1 + D)*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
          Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], 
          Momentum[q, D]])*Pair[LorentzIndex[Psi1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^3)*SMP["g_s"]^4)/
  (CW*(-2 + D)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)^2*
   (-4*MB^2 + 8*MB*MC - 4*MC^2 + s)^2*SW*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]] - 
    4*Pair[Momentum[k, D], Momentum[q, D]])*
   (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
