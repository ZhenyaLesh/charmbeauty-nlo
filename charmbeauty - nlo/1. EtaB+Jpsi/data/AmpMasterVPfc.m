(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, ((-4*I)*CA*CF*e*EU*Eps[LorentzIndex[li1, D], 
    LorentzIndex[Psi1, D], Momentum[p, D], Momentum[q, D]]*
   ((-2 + D)*MB^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] - 8*(-3 + D)*MB^2*MC^2*
     (4*(-3 + D)*MB^2 - 4*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    2*(-3 + D)*MB^2*MC^2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    8*(-3 + D)*MB^4*MC^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k] + 16*(-3 + D)*MB^2*MC^2*s*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/((2 - D)*(3 - D)*MB^2*MC*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)), 
 ((-4*I)*CA*CF*e*EU*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   ((-2 + D)*MB^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] - 8*(-3 + D)*MB^2*MC^2*
     (4*(-3 + D)*MB^2 - 4*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    2*(-3 + D)*MB^2*MC^2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    8*(-3 + D)*MB^4*MC^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k] + 16*(-3 + D)*MB^2*MC^2*s*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/((2 - D)*(3 - D)*MB^2*MC*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)), 0, 0, 0, 0, 
 ((-4*I)*CA*CF*e*EU*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   ((-2 + D)*MB^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] - 8*(-3 + D)*MB^2*MC^2*
     (4*(-3 + D)*MB^2 - 4*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    2*(-3 + D)*MB^2*MC^2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    8*(-3 + D)*MB^4*MC^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k] + 16*(-3 + D)*MB^2*MC^2*s*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/((2 - D)*(3 - D)*MB^2*MC*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)), 
 ((-4*I)*CA*CF*e*EU*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   ((-2 + D)*MB^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[p, D]])^(-1), k] - 8*(-3 + D)*MB^2*MC^2*
     (4*(-3 + D)*MB^2 - 4*MC^2 + s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    2*(-3 + D)*MB^2*MC^2*(4*(-14 + 3*D)*MB^2 - 4*(-6 + D)*MC^2 + (-10 + D)*s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    8*(-3 + D)*MB^4*MC^2*(4*(-2 + D)*MB^2 + 4*(-6 + D)*MC^2 - (-2 + D)*s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    4*(-2 + D)*MC^2*(4*MB^2 - 4*MC^2 + s)*
     IntF[(Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
         Momentum[q, D]])^(-1), k] + 16*(-3 + D)*MB^2*MC^2*s*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k] - 
    4*(-3 + D)*MB^2*MC^2*(4*MB^2 - 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/((2 - D)*(3 - D)*MB^2*MC*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   ((16*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
          Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 - 4*MC^2 + s) - (8*MC^2*(4*MB^2 - 4*MC^2 + s)*
      IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) + 
    4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) + 
    (64*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])*
         (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) - (8*MC^2*(4*MB^2 - 4*MC^2 + s)*
      IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])*
         (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) - 
    (2*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k])/((-3 + D)*MB^2) - 
    (4*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k])/(4*MB^2 - 4*MC^2 + s) - 
    (8*s*IntF[1/((4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], 
            Momentum[k, D]] - 2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 - 4*MC^2 + s) + 
    2*(4*MB^2 - 4*MC^2 + s)*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] + (4*MB^2 - 4*MC^2 + s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 
 ((-16*I)*CA*CF*e*EU*MC*Eps[LorentzIndex[li1, D], LorentzIndex[Psi1, D], 
    Momentum[p, D], Momentum[q, D]]*
   ((16*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
          Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 - 4*MC^2 + s) - (8*MC^2*(4*MB^2 - 4*MC^2 + s)*
      IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) + 
    4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k] + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) + 
    (64*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])*
         (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) - (8*MC^2*(4*MB^2 - 4*MC^2 + s)*
      IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])*
         (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k])/
     (4*MB^2 + 4*MC^2 - s) - 
    (2*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k])/((-3 + D)*MB^2) - 
    (4*s*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k])/(4*MB^2 - 4*MC^2 + s) - 
    (8*s*IntF[1/((4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], 
            Momentum[k, D]] - 2*Pair[Momentum[k, D], Momentum[p, D]] - 
          4*Pair[Momentum[k, D], Momentum[q, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k])/(4*MB^2 - 4*MC^2 + s) + 
    2*(4*MB^2 - 4*MC^2 + s)*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k] + (4*MB^2 - 4*MC^2 + s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k])*SMP["g_s"]^4)/
  ((-2 + D)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)), 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
