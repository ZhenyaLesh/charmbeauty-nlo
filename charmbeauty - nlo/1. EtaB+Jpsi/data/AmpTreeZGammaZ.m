(* Created with the Wolfram Language : www.wolfram.com *)
-(e^3*MB*Rb*Rc*(4*CW^2*ED*EU*SW^2*(1 + 4*ED*SW^2) - 
    (MC^2*(-1 + 4*EU*SW^2)*(16*MC^4 + 32*MB^4*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) - 
       MZ^2*s*(3 + 8*ED*SW^2 + 16*ED^2*SW^4) + 
       4*MC^2*(s + (MZ + 4*ED*MZ*SW^2)^2) - 4*MB^2*((MZ + 4*ED*MZ*SW^2)^2 - 
         2*s*(1 + 4*ED*SW^2 + 8*ED^2*SW^4) + 4*MC^2*(3 + 8*ED*SW^2 + 
           16*ED^2*SW^4))))/((2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
      (4*MB^2 - 4*MC^2 + s)))*Eps[LorentzIndex[\[Gamma]], 
    LorentzIndex[\[Psi]], Momentum[p], Momentum[q]])/
 (16*CW^3*(MB*MC)^(3/2)*Pi*(-4*MB^2 + 4*MC^2 + s)*SUNN*SW^3)
