(* Created with the Wolfram Language : www.wolfram.com *)
-(e^3*ED*EU*Rb*Rc*(1 + 4*ED*SW^2)*Eps[LorentzIndex[\[Gamma]], 
    LorentzIndex[\[Psi]], Momentum[p], Momentum[q]])/
 (4*CW*MC*Sqrt[MB*MC]*Pi*(-4*MB^2 + 4*MC^2 + s)*SUNN*SW)
