(* Created with the Wolfram Language : www.wolfram.com *)
(e^3*ED*Rb*Rc*(4*CW^2*ED*EU*MZ^2*SW^2 + 
   MC^2*(1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)))*
  Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]], Momentum[p], 
   Momentum[q]])/(4*CW^2*MC*Sqrt[MB*MC]*(2*MC - MZ)*(2*MC + MZ)*Pi*
  (-4*MB^2 + 4*MC^2 + s)*SUNN*SW^2)
