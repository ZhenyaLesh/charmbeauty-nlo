(* Created with the Wolfram Language : www.wolfram.com *)
{0, (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*((2*Re[PreContract[0]])/s^2 + Re[PreContract[0]]/
     (8*CW^2*SW^2*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    ((-MZ^2 + s)*Re[PreContract[0]])/(CW*s*SW*((-MZ^2 + s)^2 + 
       MZ^2*\[CapitalGamma]^2))))/(8*s), 
 (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*(Re[PreContract[0]]/s^2 + Re[PreContract[0]]/
     (16*CW^2*SW^2*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)) + 
    ((-MZ^2 + s)*Re[PreContract[0]])/(2*CW*s*SW*((-MZ^2 + s)^2 + 
       MZ^2*\[CapitalGamma]^2))))/(8*s)}
