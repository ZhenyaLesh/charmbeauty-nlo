(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, (CA*CF*e*(1 - 4*EU*SW^2)*
   (2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/((-3 + D)*MB^2) + 
    (4*MB^2 - 4*MC^2 + s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    (4*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 + s)) + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 + s) - 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 16*MC^2*s*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s) + 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 4*MC^2*(4*MB^2 - 4*MC^2 + 3*s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s))*
   SMP["g_s"]^4)/(2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 (CA*CF*e*(1 - 4*EU*SW^2)*
   (2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/((-3 + D)*MB^2) + 
    (4*MB^2 - 4*MC^2 + s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    (4*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 + s)) + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 + s) - 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 16*MC^2*s*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s) + 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 4*MC^2*(4*MB^2 - 4*MC^2 + 3*s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s))*
   SMP["g_s"]^4)/(2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 0, 0, 0, 0, 
 (CA*CF*e*(1 - 4*EU*SW^2)*
   (-2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/((-3 + D)*MB^2) - 
    (4*MB^2 - 4*MC^2 + s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    (4*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 + s)) - 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 + s) + 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 16*MC^2*s*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s) - 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 4*MC^2*(4*MB^2 - 4*MC^2 + 3*s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s))*
   SMP["g_s"]^4)/(2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 (CA*CF*e*(1 - 4*EU*SW^2)*
   (-2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/((-3 + D)*MB^2) - 
    (4*MB^2 - 4*MC^2 + s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    (4*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 + s)) - 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 + s) + 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 16*MC^2*s*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s) - 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - s)*(4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + 4*MC^2*(4*MB^2 - 4*MC^2 + 3*s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 + s))*
   SMP["g_s"]^4)/(2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 (CA*CF*e*(1 + 4*ED*SW^2)*
   ((-64*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 - s) - 
    (8*(-2 + D)*IntF[-(-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], 
            Momentum[k, D]] + 4*Pair[Momentum[k, D], Momentum[p, D]] + 
          2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 - s)) - 
    (4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      (4*MB^2*(4*MB^2 - 4*MC^2 - 3*s)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s) - 
    2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*MC^2) + 2*(4*MB^2 - 4*MC^2 - s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    (4*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      (16*MB^2*s*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 (CA*CF*e*(1 + 4*ED*SW^2)*
   ((4*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 - s)) - 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
          2*Pair[Momentum[k, D], Momentum[p, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k]*((4*MB^2 - 4*MC^2 + s)*
        Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s) - 
    (2*IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
          2*Pair[Momentum[k, D], Momentum[p, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k]*(4*MB^2*(4*MB^2 - 4*MC^2 - 3*s)*
        Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
        (4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 - s) - 
    2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*MC^2) + (4*MB^2 - 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) - 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k]*
      (16*MB^2*s*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 0, 0, (CA*CF*e*(1 + 4*ED*SW^2)*
   ((64*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
          2*Pair[Momentum[k, D], Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 - s) + 
    (8*(-2 + D)*IntF[-(-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], 
            Momentum[k, D]] + 4*Pair[Momentum[k, D], Momentum[p, D]] + 
          2*Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 - s)) + 
    (4*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      (4*MB^2*(4*MB^2 - 4*MC^2 - 3*s)*Pair[LorentzIndex[li1, D], 
         Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*(4*MC^2 - s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s) + 
    2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*MC^2) - 2*(4*MB^2 - 4*MC^2 - s)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    (4*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
          2*Pair[Momentum[k, D], Momentum[k, D]] - 
          4*Pair[Momentum[k, D], Momentum[p, D]] - 
          2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
      (16*MB^2*s*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 (CA*CF*e*(1 + 4*ED*SW^2)*
   ((-4*(-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])^(-1), k]*
      ((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*(4*MB^2 - 4*MC^2 - s)) + 
    (32*MB^2*MC^2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
         (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
          2*Pair[Momentum[k, D], Momentum[p, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k]*((4*MB^2 - 4*MC^2 + s)*
        Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
        Pair[LorentzIndex[li1, D], Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s) + 
    (2*IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
          2*Pair[Momentum[k, D], Momentum[p, D]])*
         (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
           Momentum[q, D]])), k]*(4*MB^2*(4*MB^2 - 4*MC^2 - 3*s)*
        Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
        (4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     (4*MB^2 - 4*MC^2 - s) + 
    2*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]])), k]*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    ((-2 + D)*IntF[(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[p, D]])^(-1), k]*
      (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]))/
     ((-3 + D)*MC^2) - (4*MB^2 - 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + 
    (2*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
           Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
          Pair[Momentum[k, D], Momentum[q, D]])), k]*
      (16*MB^2*s*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
       (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - s^2)*Pair[LorentzIndex[li1, D], 
         Momentum[q, D]]))/(4*MB^2 - 4*MC^2 - s))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*SW), 
 0, 0, 0, 0, 0, 
 (CA*CF*e*(4*s*(-1 + 4*EU*SW^2)*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((12*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      2*(4*MB^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    2*s*(1 - 4*EU*SW^2)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((12*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      2*(4*MB^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    2*s*(4*MB^2 - 4*MC^2 + s)*(1 - 4*EU*SW^2)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + s*(4*MB^2 - 4*MC^2 + s)*(1 - 4*EU*SW^2)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + (1 - 4*EU*SW^2)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((16*MB^4 + 16*MC^4 - s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 + 16*MC^4 + 3*s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    2*(-1 + 4*EU*SW^2)*IntF[1/((4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*
     ((16*MB^4 + 16*MC^4 - s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 + 16*MC^4 + 3*s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
   SW), 
 (CA*CF*e*(4*s*(-1 + 4*EU*SW^2)*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (4*MB^2 - 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((12*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      2*(4*MB^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    2*s*(1 - 4*EU*SW^2)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 + 
         Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((12*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      2*(4*MB^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    2*s*(4*MB^2 - 4*MC^2 + s)*(1 - 4*EU*SW^2)*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*(4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + s*(4*MB^2 - 4*MC^2 + s)*(1 - 4*EU*SW^2)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + (1 - 4*EU*SW^2)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] - 
         Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((16*MB^4 + 16*MC^4 - s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 + 16*MC^4 + 3*s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    2*(-1 + 4*EU*SW^2)*IntF[1/((4*MB^2 - 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         2*Pair[Momentum[k, D], Momentum[p, D]] - 
         4*Pair[Momentum[k, D], Momentum[q, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*
     ((16*MB^4 + 16*MC^4 - s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 + 16*MC^4 + 3*s^2 - 16*MB^2*(2*MC^2 + s))*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*
   SW), (CA*CF*e*(1 + 4*ED*SW^2)*(-2*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    4*(4*MB^2 - 4*MC^2 - s)*s*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((8*MC^2 - 2*s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (-4*MB^2 + 12*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    2*(4*MB^2 - 4*MC^2 - s)*s*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((8*MC^2 - 2*s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (-4*MB^2 + 12*MC^2 + s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + 2*(4*MB^2 - 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 16*MC^2*s + 3*s^2)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 16*MC^2*s - s^2)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    (4*MB^2 - 4*MC^2 - s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 
        16*MC^2*s + 3*s^2)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 16*MC^2*s - s^2)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*SW), 
 (CA*CF*e*(1 + 4*ED*SW^2)*(-2*s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    s*(-4*MB^2 + 4*MC^2 + s)^2*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
          Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]]) + 
    4*(4*MB^2 - 4*MC^2 - s)*s*IntF[1/(Pair[Momentum[k, D], Momentum[k, D]]*
        (-4*MB^2 + 4*MC^2 + s + 2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((8*MC^2 - 2*s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (-4*MB^2 + 12*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    2*(4*MB^2 - 4*MC^2 - s)*s*
     IntF[1/((4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
         2*Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((8*MC^2 - 2*s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (-4*MB^2 + 12*MC^2 + s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]]) + 2*(4*MB^2 - 4*MC^2 - s)*
     IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[p, D]])*(-4*MB^2 + 4*MC^2 + s + 
         2*Pair[Momentum[k, D], Momentum[k, D]] - 
         4*Pair[Momentum[k, D], Momentum[p, D]] - 
         2*Pair[Momentum[k, D], Momentum[q, D]])), k]*
     ((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 16*MC^2*s + 3*s^2)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 16*MC^2*s - s^2)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]) - 
    (4*MB^2 - 4*MC^2 - s)*IntF[1/((Pair[Momentum[k, D], Momentum[k, D]] + 
         Pair[Momentum[k, D], Momentum[p, D]])*
        (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
          Momentum[q, D]])), k]*((16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 
        16*MC^2*s + 3*s^2)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 16*MC^2*s - s^2)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]]))*SMP["g_s"]^4)/
  (2*CW*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*s*SW), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
