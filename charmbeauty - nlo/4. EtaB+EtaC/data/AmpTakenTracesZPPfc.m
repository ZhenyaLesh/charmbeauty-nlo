(* Created with the Wolfram Language : www.wolfram.com *)
{0, 0, 0, 0, (2*CA*CF*e*EU*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*((4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] - 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + 2*(4*MB^2 - 4*MC^2 + s)*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*((4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 (2*CA*CF*e*EU*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
      s)*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] - 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + 2*(4*MB^2 - 4*MC^2 + s)*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*((4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 
 (-2*CA*CF*e*EU*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
      s)*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] - 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + 2*(4*MB^2 - 4*MC^2 + s)*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*((4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 (-2*CA*CF*e*EU*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
      s)*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] - 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + 2*(4*MB^2 - 4*MC^2 + s)*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 16*MC^2*((4*MB^2 - 4*MC^2 + s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 - 4*MC^2 + s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, (-4*CA*CF*e*ED*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + (4*MB^2 - 4*MC^2 - s)*
     (4*MB^2 + 4*MC^2 - s)*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]] + 
    16*MB^2*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    8*MC^2*(-4*MB^2 + 4*MC^2 + s)*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]])*SMP["g_s"]^4)/((4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
    2*Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 - 4*MC^2 - s - 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    4*Pair[Momentum[k, D], Momentum[p, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])), 
 (2*CA*CF*e*ED*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
      s)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + 
    16*MB^2*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    2*(4*MB^2 - 4*MC^2 - s)*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]])*SMP["g_s"]^4)/
  ((4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
    2*Pair[Momentum[k, D], Momentum[p, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 
 (4*CA*CF*e*ED*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
      s)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + (4*MB^2 - 4*MC^2 - s)*
     (4*MB^2 + 4*MC^2 - s)*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]] + 
    16*MB^2*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]^2 - 
    8*MC^2*(-4*MB^2 + 4*MC^2 + s)*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]] + 2*(4*MB^2 - 4*MC^2 - s)*
     (8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]])*SMP["g_s"]^4)/((4*MB^2 - 4*MC^2 - s)*
   (4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
    2*Pair[Momentum[k, D], Momentum[p, D]])*(4*MB^2 - 4*MC^2 - s - 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    4*Pair[Momentum[k, D], Momentum[p, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])), 
 (-2*CA*CF*e*ED*((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - 
      s)*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]] + 
    16*MB^2*((4*MB^2 - 4*MC^2 + s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 - 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]^2 + 
    2*(4*MB^2 - 4*MC^2 - s)*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]])*SMP["g_s"]^4)/
  ((4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
   (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] + 
    2*Pair[Momentum[k, D], Momentum[p, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 0, 0, 0, 0, 0, 
 (-2*CA*CF*e*EU*((4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
      Momentum[q, D]]*Pair[Momentum[k, D], Momentum[k, D]] + 
    4*(4*MB^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (16*MB^4 - 8*MB^2*s + (-4*MC^2 + s)^2)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*Pair[Momentum[k, D], 
      Momentum[k, D]]*Pair[Momentum[k, D], Momentum[p, D]] - 
    16*MB^2*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2 + 2*(4*MB^2 + 4*MC^2 - s)*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - 16*MB^2*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 
    8*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2*Pair[Momentum[k, D], 
      Momentum[q, D]] + 8*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]] - 
    4*Pair[Momentum[k, D], Momentum[q, D]])*
   (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 (-2*CA*CF*e*EU*((4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
      Momentum[q, D]]*Pair[Momentum[k, D], Momentum[k, D]] + 
    4*(4*MB^2*(4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (16*MB^4 - 8*MB^2*s + (-4*MC^2 + s)^2)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*Pair[Momentum[k, D], 
      Momentum[k, D]]*Pair[Momentum[k, D], Momentum[p, D]] - 
    16*MB^2*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2 + 2*(4*MB^2 + 4*MC^2 - s)*
     ((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[k, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] - 16*MB^2*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 
    8*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2*Pair[Momentum[k, D], 
      Momentum[q, D]] + 8*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + 8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   Pair[Momentum[k, D], Momentum[k, D]]*
   (Pair[Momentum[k, D], Momentum[k, D]] + Pair[Momentum[k, D], 
     Momentum[p, D]])*(4*MB^2 - 4*MC^2 + s + 
    2*Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]] - 
    4*Pair[Momentum[k, D], Momentum[q, D]])*
   (4*MB^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[q, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[q, D]])), 
 (2*CA*CF*e*ED*((4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
      Momentum[p, D]]*Pair[Momentum[k, D], Momentum[k, D]] + 
    2*(4*MB^2 + 4*MC^2 - s)*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[k, D]]*
     Pair[Momentum[k, D], Momentum[p, D]] + 
    4*((16*MB^4 - 8*MB^2*s + (-4*MC^2 + s)^2)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + 4*MC^2*(4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*Pair[Momentum[k, D], 
      Momentum[k, D]]*Pair[Momentum[k, D], Momentum[q, D]] - 
    16*MC^2*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 
    8*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2*Pair[Momentum[k, D], 
      Momentum[q, D]] - 16*MC^2*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    8*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   Pair[Momentum[k, D], Momentum[k, D]]*
   (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] + 
    Pair[Momentum[k, D], Momentum[q, D]])*(4*MB^2 - 4*MC^2 - s - 
    2*Pair[Momentum[k, D], Momentum[k, D]] + 
    4*Pair[Momentum[k, D], Momentum[p, D]] + 
    2*Pair[Momentum[k, D], Momentum[q, D]])), 
 (2*CA*CF*e*ED*((4*MB^2 + 4*MC^2 - s)*(4*MB^2 - 8*MB*MC + 4*MC^2 - s)*
     (4*MB^2 + 8*MB*MC + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
      Momentum[p, D]]*Pair[Momentum[k, D], Momentum[k, D]] + 
    2*(4*MB^2 + 4*MC^2 - s)*(8*MB^2*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], 
        Momentum[q, D]])*Pair[Momentum[k, D], Momentum[k, D]]*
     Pair[Momentum[k, D], Momentum[p, D]] + 
    4*((16*MB^4 - 8*MB^2*s + (-4*MC^2 + s)^2)*Pair[LorentzIndex[li1, D], 
        Momentum[p, D]] + 4*MC^2*(4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[q, D]])*Pair[Momentum[k, D], 
      Momentum[k, D]]*Pair[Momentum[k, D], Momentum[q, D]] - 
    16*MC^2*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*Pair[Momentum[k, D], 
      Momentum[q, D]] + 
    8*(8*MB^2*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      (4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]^2*Pair[Momentum[k, D], 
      Momentum[q, D]] - 16*MC^2*((4*MB^2 + 4*MC^2 - s)*
       Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[q, D]]^2 + 
    8*((4*MB^2 + 4*MC^2 - s)*Pair[LorentzIndex[li1, D], Momentum[p, D]] + 
      8*MC^2*Pair[LorentzIndex[li1, D], Momentum[q, D]])*
     Pair[Momentum[k, D], Momentum[p, D]]*
     Pair[Momentum[k, D], Momentum[q, D]]^2)*SMP["g_s"]^4)/
  ((4*MB^2 - 8*MB*MC + 4*MC^2 - s)*(4*MB^2 + 8*MB*MC + 4*MC^2 - s)*
   Pair[Momentum[k, D], Momentum[k, D]]*
   (4*MC^2 + Pair[Momentum[k, D], Momentum[k, D]] - 
    2*Pair[Momentum[k, D], Momentum[p, D]])*
   (Pair[Momentum[k, D], Momentum[k, D]] - Pair[Momentum[k, D], 
     Momentum[p, D]])*(Pair[Momentum[k, D], Momentum[k, D]] + 
    Pair[Momentum[k, D], Momentum[q, D]])*(4*MB^2 - 4*MC^2 - s - 
    2*Pair[Momentum[k, D], Momentum[k, D]] + 
    4*Pair[Momentum[k, D], Momentum[p, D]] + 
    2*Pair[Momentum[k, D], Momentum[q, D]])), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
