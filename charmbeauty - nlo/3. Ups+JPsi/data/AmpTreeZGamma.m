(* Created with the Wolfram Language : www.wolfram.com *)
((-I/108)*e^3*Rb*Rc*((-6*I)*MB^2*(4*MB^2 - 4*MC^2 + s)*
    Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1], LorentzIndex[\[Psi]2], 
     Momentum[p]] - (6*I)*MC^2*(-4*MB^2 + 4*MC^2 + s)*
    Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1], LorentzIndex[\[Psi]2], 
     Momentum[q]] - 12*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], 
     LorentzIndex[\[Psi]2]]*Pair[LorentzIndex[\[Psi]1], Momentum[q]] + 
   12*MC^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] + 
   3*MC^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] + 
   32*MB^2*MC^2*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] - 
   32*MC^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] - 
   8*MC^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] + 
   12*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] - 
   12*MC^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] - 
   3*MC^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] - 
   32*MB^2*MC^2*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] + 
   32*MC^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] + 
   8*MC^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] - 
   12*MB^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   12*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] - 
   3*MB^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   16*MB^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] - 
   16*MB^2*MC^2*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   4*MB^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   12*MB^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] - 
   12*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] + 
   3*MB^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] - 
   16*MB^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] + 
   16*MB^2*MC^2*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] - 
   4*MB^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]]))/
 (CW*(MB*MC)^(3/2)*Pi*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SUNN*SW)
