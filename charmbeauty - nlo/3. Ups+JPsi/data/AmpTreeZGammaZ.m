(* Created with the Wolfram Language : www.wolfram.com *)
-(e^3*Rb*Rc*(MB^2*(4*CW^2*ED*EU*MZ^4*s*SW^2 + 16*MC^6*(1 + 4*ED*SW^2)*
       (-1 + 4*EU*SW^2) + 32*MB^4*(-2*CW^2*ED*EU*MZ^2*SW^2 + 
        MC^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2))) + 
      4*MC^4*(s*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) + 
        MZ^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2))) - 
      MC^2*MZ^2*(16*CW^2*ED*EU*MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 
          4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2))) - 
      4*MB^2*(4*CW^2*ED*EU*MZ^2*(-MZ^2 + s)*SW^2 + 
        4*MC^4*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2)) + 
        MC^2*(MZ^2*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 
          2*s*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2)))))*
     Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1], 
      LorentzIndex[\[Psi]2], Momentum[p]] + 
    MC^2*(4*CW^2*ED*EU*MZ^2*(-4*MC^2 + MZ^2)*(4*MC^2 + s)*SW^2 + 
      16*MB^6*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 
      4*MB^4*(-(s*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2)) + 
        MZ^2*(1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)) + 
        4*MC^2*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2))) + 
      MB^2*(32*MC^4*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 
            4*EU*SW^2)) - 4*MC^2*(MZ^2*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 
          2*s*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2))) - 
        MZ^2*(16*CW^2*ED*EU*MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 
            4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2)))))*
     Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1], 
      LorentzIndex[\[Psi]2], Momentum[q]] + 
    (2*I)*SW^2*(-(EU*MC^2*(4*MC^2 - MZ^2)*(-4*MB^2 + 4*MC^2 + s)*
        (CW^2*ED*MZ^2*(-1 + 4*EU*SW^2) - 
         2*MB^2*((1 + 4*ED*SW^2)*(-1 + 2*EU*SW^2) + 2*CW^2*ED*
            (-1 + 4*EU*SW^2)))*Pair[LorentzIndex[\[Gamma]], 
         LorentzIndex[\[Psi]2]]*Pair[LorentzIndex[\[Psi]1], Momentum[p]]) + 
      ED*MB^2*(4*MB^2 - MZ^2)*(4*MB^2 - 4*MC^2 + s)*
       (-(CW^2*EU*MZ^2*(1 + 4*ED*SW^2)) + 
        2*MC^2*((1 + 2*ED*SW^2)*(-1 + 4*EU*SW^2) + 
          2*CW^2*(EU + 4*ED*EU*SW^2)))*Pair[LorentzIndex[\[Gamma]], 
        LorentzIndex[\[Psi]1]]*Pair[LorentzIndex[\[Psi]2], Momentum[q]])))/
 (8*CW^3*(MB*MC)^(3/2)*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*Pi*
  (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SUNN*SW^3)
