(* Created with the Wolfram Language : www.wolfram.com *)
{(Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*PreContract[((-2*I)*(-1 + 4*SW^2)*Eps[LorentzIndex[\[Gamma]], 
        LorentzIndex[\[Gamma]1], Momentum[p1], Momentum[p2]] + 
      (1 + (-1 + 4*SW^2)^2)*
       (-(s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Gamma]1]])/2 + 
        Pair[LorentzIndex[\[Gamma]], Momentum[p2]]*
         Pair[LorentzIndex[\[Gamma]1], Momentum[p1]] + 
        Pair[LorentzIndex[\[Gamma]], Momentum[p1]]*
         Pair[LorentzIndex[\[Gamma]1], Momentum[p2]]))*
     (((8*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (4*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (4*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/(3*CW^3*(2*MB - MZ)*
          (2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*
          (4*MB^2 - 4*MC^2 + s)*SW^3) + (MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*
          \[Alpha]^(3/2))/(3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*
          (2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (4*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (32*ED*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (32*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (8*ED*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (8*EU*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*EU*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (ED*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MB^2*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (128*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (64*ED*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (16*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (32*ED*EU*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (16*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (CW^3*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)))*
       Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]11], Momentum[p], 
        Momentum[q]]*PreConjugate[-(Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2)*
           (-(MB^2*(4*CW^2*ED*EU*MZ^4*s*SW^2 + 16*MC^6*(1 + 4*ED*SW^2)*
                (-1 + 4*EU*SW^2) + 32*MB^4*(-2*CW^2*ED*EU*MZ^2*SW^2 + 
                 MC^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 
                     4*EU*SW^2))) + 4*MC^4*(s*(1 + 4*ED*SW^2)*(-1 + 
                   4*EU*SW^2) + MZ^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 
                     4*CW^2*EU + 4*EU*SW^2))) - MC^2*MZ^2*(16*CW^2*ED*EU*MZ^2*
                  SW^2 + s*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 
                     12*EU*SW^2))) - 4*MB^2*(4*CW^2*ED*EU*MZ^2*(-MZ^2 + s)*
                  SW^2 + 4*MC^4*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*
                      EU + 12*EU*SW^2)) + MC^2*(MZ^2*(1 + 4*ED*SW^2)*
                    (-1 + 4*EU*SW^2) - 2*s*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                      (-1 + 2*CW^2*EU + 4*EU*SW^2)))))*Eps[LorentzIndex[
                \[Gamma]1], LorentzIndex[\[Psi]11], Momentum[p], Momentum[
                q]]) + (2*I)*SW^2*(4*ED*MB^4*(4*MB^2 - MZ^2)*(4*MB^2 - 
                4*MC^2 + s)*(-(CW^2*EU*MZ^2*(1 + 4*ED*SW^2)) + 
                2*MC^2*((1 + 2*ED*SW^2)*(-1 + 4*EU*SW^2) + 2*CW^2*
                   (EU + 4*ED*EU*SW^2)))*Pair[LorentzIndex[\[Gamma]1], 
                LorentzIndex[\[Psi]11]] - EU*MC^2*(4*MC^2 - MZ^2)*(-4*MB^2 + 
                4*MC^2 + s)*(CW^2*ED*MZ^2*(-1 + 4*EU*SW^2) - 2*MB^2*
                 ((1 + 4*ED*SW^2)*(-1 + 2*EU*SW^2) + 2*CW^2*ED*(-1 + 
                    4*EU*SW^2)))*Pair[LorentzIndex[\[Gamma]1], Momentum[q]]*
               Pair[LorentzIndex[\[Psi]11], Momentum[p]])))/
         (3*CW^3*(MB*MC)^(3/2)*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*
          (2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3)] + 
      ((32*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (16*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (16*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (4*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (4*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (8*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (4*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (128*ED*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (128*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (256*ED*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*EU*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (256*ED*EU*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*ED*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*ED*EU*MB^4*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*EU*MB^2*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (32*ED*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (32*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (512*ED*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (256*ED*EU*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (256*ED*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (128*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)))*
       Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]11], 
        LorentzIndex[\[Psi]22], Momentum[p]]*PreConjugate[
        -(Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2)*(MB^2*(4*CW^2*ED*EU*MZ^4*s*SW^2 + 
              16*MC^6*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) + 32*MB^4*(
                -2*CW^2*ED*EU*MZ^2*SW^2 + MC^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                   (-1 + 2*CW^2*EU + 4*EU*SW^2))) + 4*MC^4*(s*(1 + 4*ED*SW^2)*
                 (-1 + 4*EU*SW^2) + MZ^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                   (-1 + 4*CW^2*EU + 4*EU*SW^2))) - MC^2*MZ^2*(16*CW^2*ED*EU*
                 MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 
                    12*EU*SW^2))) - 4*MB^2*(4*CW^2*ED*EU*MZ^2*(-MZ^2 + s)*
                 SW^2 + 4*MC^4*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 
                    12*EU*SW^2)) + MC^2*(MZ^2*(1 + 4*ED*SW^2)*(-1 + 
                    4*EU*SW^2) - 2*s*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 
                      2*CW^2*EU + 4*EU*SW^2)))))*Eps[LorentzIndex[\[Gamma]1], 
              LorentzIndex[\[Psi]11], LorentzIndex[\[Psi]22], Momentum[p]] + 
            MC^2*(4*CW^2*ED*EU*MZ^2*(-4*MC^2 + MZ^2)*(4*MC^2 + s)*SW^2 + 
              16*MB^6*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 4*MB^4*(
                -(s*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2)) + MZ^2*(1 - 4*EU*SW^2 - 
                  4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)) + 4*MC^2*
                 (-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*
                     SW^2))) + MB^2*(32*MC^4*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                   (-1 + 2*CW^2*EU + 4*EU*SW^2)) - 4*MC^2*
                 (MZ^2*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 2*s*(-1 + 
                    4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2))) - 
                MZ^2*(16*CW^2*ED*EU*MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 
                    4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2)))))*
             Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]11], 
              LorentzIndex[\[Psi]22], Momentum[q]] + (2*I)*SW^2*
             (-(EU*MC^2*(4*MC^2 - MZ^2)*(-4*MB^2 + 4*MC^2 + s)*
                (CW^2*ED*MZ^2*(-1 + 4*EU*SW^2) - 2*MB^2*((1 + 4*ED*SW^2)*
                    (-1 + 2*EU*SW^2) + 2*CW^2*ED*(-1 + 4*EU*SW^2)))*
                Pair[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]22]]*
                Pair[LorentzIndex[\[Psi]11], Momentum[p]]) + 
              ED*MB^2*(4*MB^2 - MZ^2)*(4*MB^2 - 4*MC^2 + s)*(
                -(CW^2*EU*MZ^2*(1 + 4*ED*SW^2)) + 2*MC^2*((1 + 2*ED*SW^2)*
                   (-1 + 4*EU*SW^2) + 2*CW^2*(EU + 4*ED*EU*SW^2)))*Pair[
                LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]11]]*Pair[
                LorentzIndex[\[Psi]22], Momentum[q]])))/(3*CW^3*(MB*MC)^(3/2)*
          (2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3)] + 
      ((16*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (16*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (32*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (4*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (4*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (4*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (8*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (64*ED*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*EU*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (256*ED*EU*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (128*ED*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (128*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (256*ED*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*ED*EU*MC^4*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*EU*MC^2*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (32*ED*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (32*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (256*ED*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (256*ED*EU*MB^2*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (512*ED*EU*MC^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (128*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)))*
       Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]11], 
        LorentzIndex[\[Psi]22], Momentum[q]]*PreConjugate[
        -(Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2)*(MB^2*(4*CW^2*ED*EU*MZ^4*s*SW^2 + 
              16*MC^6*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) + 32*MB^4*(
                -2*CW^2*ED*EU*MZ^2*SW^2 + MC^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                   (-1 + 2*CW^2*EU + 4*EU*SW^2))) + 4*MC^4*(s*(1 + 4*ED*SW^2)*
                 (-1 + 4*EU*SW^2) + MZ^2*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                   (-1 + 4*CW^2*EU + 4*EU*SW^2))) - MC^2*MZ^2*(16*CW^2*ED*EU*
                 MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 
                    12*EU*SW^2))) - 4*MB^2*(4*CW^2*ED*EU*MZ^2*(-MZ^2 + s)*
                 SW^2 + 4*MC^4*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 
                    12*EU*SW^2)) + MC^2*(MZ^2*(1 + 4*ED*SW^2)*(-1 + 
                    4*EU*SW^2) - 2*s*(-1 + 4*EU*SW^2 + 4*ED*SW^2*(-1 + 
                      2*CW^2*EU + 4*EU*SW^2)))))*Eps[LorentzIndex[\[Gamma]1], 
              LorentzIndex[\[Psi]11], LorentzIndex[\[Psi]22], Momentum[p]] + 
            MC^2*(4*CW^2*ED*EU*MZ^2*(-4*MC^2 + MZ^2)*(4*MC^2 + s)*SW^2 + 
              16*MB^6*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 4*MB^4*(
                -(s*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2)) + MZ^2*(1 - 4*EU*SW^2 - 
                  4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)) + 4*MC^2*
                 (-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*
                     SW^2))) + MB^2*(32*MC^4*(-1 + 4*EU*SW^2 + 4*ED*SW^2*
                   (-1 + 2*CW^2*EU + 4*EU*SW^2)) - 4*MC^2*
                 (MZ^2*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 2*s*(-1 + 
                    4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2))) - 
                MZ^2*(16*CW^2*ED*EU*MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 
                    4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2)))))*
             Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]11], 
              LorentzIndex[\[Psi]22], Momentum[q]] + (2*I)*SW^2*
             (-(EU*MC^2*(4*MC^2 - MZ^2)*(-4*MB^2 + 4*MC^2 + s)*
                (CW^2*ED*MZ^2*(-1 + 4*EU*SW^2) - 2*MB^2*((1 + 4*ED*SW^2)*
                    (-1 + 2*EU*SW^2) + 2*CW^2*ED*(-1 + 4*EU*SW^2)))*
                Pair[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]22]]*
                Pair[LorentzIndex[\[Psi]11], Momentum[p]]) + 
              ED*MB^2*(4*MB^2 - MZ^2)*(4*MB^2 - 4*MC^2 + s)*(
                -(CW^2*EU*MZ^2*(1 + 4*ED*SW^2)) + 2*MC^2*((1 + 2*ED*SW^2)*
                   (-1 + 4*EU*SW^2) + 2*CW^2*(EU + 4*ED*EU*SW^2)))*Pair[
                LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]11]]*Pair[
                LorentzIndex[\[Psi]22], Momentum[q]])))/(3*CW^3*(MB*MC)^(3/2)*
          (2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3)] + 
      ((-4*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (4*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (8*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) + 
        (2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/(3*CW^3*(2*MB - MZ)*
          (2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*
          (4*MB^2 - 4*MC^2 + s)*SW^3) + (MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*
          \[Alpha]^(3/2))/(3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*
          (2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (4*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3) - 
        (16*ED*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (32*ED*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (32*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (64*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (16*ED*EU*MC^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (8*ED*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (8*EU*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (16*ED*EU*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (4*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MB^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (ED*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) - 
        (ED*EU*Sqrt[MB*MC]*MZ^4*Sqrt[Pi]*Rb*Rc*s*\[Alpha]^(3/2))/
         (3*CW*MB^2*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW) + 
        (64*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (64*ED*EU*MB^4*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (128*ED*EU*MC^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (16*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (16*ED*EU*MB^2*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*SW*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (32*ED*EU*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) - 
        (16*ED*EU*MB^2*Sqrt[MB*MC]*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (3*CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)) + 
        (4*ED*EU*Sqrt[MB*MC]*MZ^2*Sqrt[Pi]*Rb*Rc*s*SW*\[Alpha]^(3/2))/
         (CW^3*MC^2*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*(2*MC + MZ)*
          (4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)))*
       Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]22], Momentum[p], 
        Momentum[q]]*PreConjugate[-(Sqrt[Pi]*Rb*Rc*\[Alpha]^(3/2)*
           (-(MC^2*(4*CW^2*ED*EU*MZ^2*(-4*MC^2 + MZ^2)*(4*MC^2 + s)*SW^2 + 16*
                MB^6*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 4*MB^4*
                (-(s*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2)) + MZ^2*
                  (1 - 4*EU*SW^2 - 4*ED*SW^2*(-1 + 4*CW^2*EU + 4*EU*SW^2)) + 
                 4*MC^2*(-3 + 12*EU*SW^2 + 4*ED*SW^2*(-3 + 4*CW^2*EU + 
                     12*EU*SW^2))) + MB^2*(32*MC^4*(-1 + 4*EU*SW^2 + 
                   4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2)) - 4*MC^2*
                  (MZ^2*(1 + 4*ED*SW^2)*(-1 + 4*EU*SW^2) - 2*s*(-1 + 
                     4*EU*SW^2 + 4*ED*SW^2*(-1 + 2*CW^2*EU + 4*EU*SW^2))) - 
                 MZ^2*(16*CW^2*ED*EU*MZ^2*SW^2 + s*(-3 + 12*EU*SW^2 + 
                     4*ED*SW^2*(-3 + 4*CW^2*EU + 12*EU*SW^2)))))*
              Eps[LorentzIndex[\[Gamma]1], LorentzIndex[\[Psi]22], Momentum[
                p], Momentum[q]]) + (2*I)*SW^2*(-4*EU*MC^4*(4*MC^2 - MZ^2)*(
                -4*MB^2 + 4*MC^2 + s)*(CW^2*ED*MZ^2*(-1 + 4*EU*SW^2) - 
                2*MB^2*((1 + 4*ED*SW^2)*(-1 + 2*EU*SW^2) + 2*CW^2*ED*
                   (-1 + 4*EU*SW^2)))*Pair[LorentzIndex[\[Gamma]1], 
                LorentzIndex[\[Psi]22]] + ED*MB^2*(4*MB^2 - MZ^2)*(4*MB^2 - 
                4*MC^2 + s)*(-(CW^2*EU*MZ^2*(1 + 4*ED*SW^2)) + 
                2*MC^2*((1 + 2*ED*SW^2)*(-1 + 4*EU*SW^2) + 2*CW^2*
                   (EU + 4*ED*EU*SW^2)))*Pair[LorentzIndex[\[Gamma]1], 
                Momentum[p]]*Pair[LorentzIndex[\[Psi]22], Momentum[q]])))/
         (3*CW^3*(MB*MC)^(3/2)*(2*MB - MZ)*(2*MB + MZ)*(-2*MC + MZ)*
          (2*MC + MZ)*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SW^3)])])/
  (128*CW^2*s*SW^2*((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)), 
 (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*Re[PreContract[0]])/(64*CW^2*s*SW^2*
   ((-MZ^2 + s)^2 + MZ^2*\[CapitalGamma]^2)), 
 (Sqrt[(16*MB^4 - 32*MB^2*MC^2 + 16*MC^4 - 8*MB^2*s - 8*MC^2*s + s^2)/s^2]*
   \[Alpha]*PreContract[0])/(128*CW^2*s*SW^2*((-MZ^2 + s)^2 + 
    MZ^2*\[CapitalGamma]^2))}
