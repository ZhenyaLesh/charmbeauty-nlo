(* Created with the Wolfram Language : www.wolfram.com *)
((-I/4)*e^3*ED*EU*Rs^2*((2*I)*MB^2*(4*MB^2 - 4*MC^2 + s)*
    Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1], LorentzIndex[\[Psi]2], 
     Momentum[p]] + (2*I)*MC^2*(-4*MB^2 + 4*MC^2 + s)*
    Eps[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1], LorentzIndex[\[Psi]2], 
     Momentum[q]] + 4*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], 
     LorentzIndex[\[Psi]2]]*Pair[LorentzIndex[\[Psi]1], Momentum[q]] - 
   4*MC^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] - 
   MC^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] - 16*EU*MB^2*MC^2*SW^2*
    Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] + 
   16*EU*MC^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] + 
   4*EU*MC^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[q]] - 
   4*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] + 
   4*MC^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] + 
   MC^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] + 
   16*EU*MB^2*MC^2*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] - 
   16*EU*MC^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] - 
   4*EU*MC^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]2]]*
    Pair[LorentzIndex[\[Psi]1], Momentum[p + q]] + 
   4*MB^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] - 
   4*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   MB^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   16*ED*MB^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] - 16*ED*MB^2*MC^2*SW^2*
    Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] + 
   4*ED*MB^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p]] - 
   4*MB^4*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] + 
   4*MB^2*MC^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] - 
   MB^2*s*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] - 
   16*ED*MB^4*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] + 
   16*ED*MB^2*MC^2*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]] - 
   4*ED*MB^2*s*SW^2*Pair[LorentzIndex[\[Gamma]], LorentzIndex[\[Psi]1]]*
    Pair[LorentzIndex[\[Psi]2], Momentum[p + q]]))/
 (CW*MB*MC*(MB + MC)*Pi*(4*MB^2 - 4*MC^2 - s)*(4*MB^2 - 4*MC^2 + s)*SUNN*SW)
